<?php

require_once '../vendor/autoload.php';
require_once '../App/Init.php';
require_once '../App/Connect.php';

use \App\Core\App;

$app = new App();
