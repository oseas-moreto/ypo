$('form .form-control[required]').change(function () {
  if ($(this).val()) {
    $(this).removeClass('is-invalid');
  }
});

function valida(frm) {
  var valor = $(frm).val().replace(/[^a-zA-Z-0-9]+/g, '');
  $(frm).val(valor);
}

function trocaicone(icone) {
  $('#mostraicone').removeClass();
  $('#mostraicone').addClass(icone);
}


function loadregistercombobox(t, id, url) {
  let dropdown = $(id);

  dropdown.empty();
  dropdown.removeAttr('disabled');

  dropdown.append('<option value="0" selected="true">Selecione</option>');
  dropdown.prop('selectedIndex', 0);

  $.getJSON(url + '/' + t.value, function (data) {
    if(data.length == 0){
      dropdown.attr('readonly', true);
      return false;
    }
    
    if(data.length > 0){
      dropdown.removeAttr('readonly');
    }
    
    $.each(data, function (key, entry) {
      dropdown.append($('<option></option>').attr('value', entry.id).text(entry.title));
    })
  });
}

// Evento Submit do formulário
function upload(form) {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);

  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: $('#urlpage').val() + "/uploads",
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      if (r.status == '1') {
        $('#imgload').val(r.image);
        $('#previewimg').attr('src', r.image);
      }
      //alertmsg(retorno.mensagem, retorno.class);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function uploadmulti(form, parameters, idreturn) {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);

  var formURL = $('#'+form).attr("action");

  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: formURL + "" + parameters,
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      /* if (r.status == '1') {
        $(idreturn).append('<span><img src="' + r.image + '" style="width: 100px"></span>');
      } else {
        alertmsg(r.mensagem, r.classe);
      } */
      //alertmsg(retorno.mensagem, retorno.class);
      window.location.replace(r.redirect);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function uploadgeneric(form, reference, formURL, input = '') {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);
  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: formURL+"?inputFile="+reference,
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      if (r.status == '1') {
        $('#' + reference).val(r.image);
        $('#preview' + reference).attr('src', r.image);
      }
      if(input != ''){
        $('#inputimagempreview').val(r.image);
      }
      //alertmsg(retorno.mensagem, retorno.class);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function uploadgenericmod(form, reference, formURL, input = '') {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);
  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: formURL+"?inputFile="+reference,
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      if(r.status == '0'){
        alert(r.mensagem);
        return false;
      }
      if (r.status == '1') {
        $('#' + reference).val(r.image);
        $('#preview' + reference).attr('src', r.image);
      }
      if(input != ''){
        let tipoimagem = $('input[name="tipoimagem"]:checked').val();
        switch (tipoimagem) {
          case 'l':
            $('input[name="logo"]').val(r.image);
            $('#previewlogo').attr('src', r.image);
          break;
          case 'b':
            $('input[name="banner"]').val(r.image);
            $('#previewbanner').attr('src', r.image);
          break;
          case 'c':
            $('input[name="imagem"]').val(r.image);
            $('#previewimagem').attr('src', r.image);
          break;
          default:
            var indice  = parseInt((Math.random() * 100), 10);
            var content = `<div style="margin-top: 10px" class="row">
                                <div class="col-lg-2">
                                    <button type="button" data-index="${indice}" class="btn dupimagem">+</button>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" onchange="$('#previewimg${indice}').attr('src', $(this).val())"  name="imagem[]" title="Imagem" value="${r.image}" class="form-control round" placeholder="Imagem">
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <img id="previewimg${indice}" src="${r.image}" class="img-fluid">
                                    <a href="#." data-index="${indice}" class="text-danger remove"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>`;
    
            $("#imagemContent").append(content);
          break;
        }
        $('#inputimagempreview').val(r.image);
      }
      //alertmsg(retorno.mensagem, retorno.class);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function deleteimage(image, t) {
  var r = confirm("Tem certeza que deseja deletar está imagem?");
  if (r == true) {
    $('#status_response').removeClass('d-none');
    $.ajax({
      type: "POST",
      url: $('#urlpage').val() + "/deleteimage",
      data: 'image=' + image,
      dataType: 'json',
      success: function (data) {
        var r = data.response;
        alertmsg(r.mensagem, r.classe);
        $(t).remove();
      }
    });
  } else {
    return false;
  }
}

function deletar(url, nome, redirect = '') {
  var r = confirm("Tem certeza que deseja deletar " + nome + "?");
  if (r == true) {
    var formURL = url;
    $('#status_response').removeClass('d-none');
    $.ajax({
      type: "GET",
      url: formURL,
      dataType: 'json',
      success: function (data) {
        if (redirect == '') {
          location.reload();
        } else {
          window.location.href = redirect;
        }
      }
    });
  } else {
    return false;
  }
}

function block(url, nome) {
  var r = confirm("Tem certeza que deseja bloquear " + nome + "?");
  if (r == true) {
    var formURL = url;
    $('#status_response').removeClass('d-none');
    $.ajax({
      type: "GET",
      url: formURL,
      dataType: 'json',
      success: function (data) {
        if (data == true) {
          location.reload();
        }

      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        $('#status_response').addClass('d-none');
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  } else {
    return false;
  }
}

function registerNew(form) {
  var formulario = document.getElementById(form);
  // Instância o FormData passando como parâmetro o formulário
  var postData = new FormData(formulario);
  var formURL = $("#"+form).attr("action");
  
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form#' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      processData: false,
      contentType: false,
      success: function (data) {
        var response = data['response'];
        $('#status_response').addClass('d-none');
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        $('#status_response').addClass('d-none');
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  }

}

function register(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      success: function (data) {
        var response = data['response'];
        $('#status_response').addClass('d-none');
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        $('#status_response').addClass('d-none');
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  }

}

function alertmsg(msg, classe) {
  $('#status_response').addClass('d-none');
  $('#response').html('<div class="alert-dismissible fade show p-2" role="alert">' + msg + '</div>');
  $('#response').removeClass('alert alert-danger');
  $('#response').removeClass('alert alert-success');
  $('#response').addClass('alert ' + classe);
  $("#response").slideDown();
  setTimeout(function () {
    $("#response").slideUp();
  }, 4000);
}

$(document).keypress(function (e) {
  if (e.which == 13) {
    return false
  }
});

function login(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      success: function (data) {
        var response = data['response'];
        $('#status_response').addClass('d-none');
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      }
    });
  }
}

function enviarsenha(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  $.ajax({
    type: "POST",
    url: formURL,
    data: postData,
    dataType: 'json',
    success: function (data) {
      var response = data['response'];
      $('#responsepass').html('<div class="alert-dismissible fade show p-2" role="alert">' + response['mensagem'] + '</div>');
      $('#responsepass').removeClass('alert alert-danger');
      $('#responsepass').removeClass('alert alert-success');
      $('#responsepass').addClass('alert ' + response['classe']);


      if (response['result'] == 'error') {
        $('.validate-input').each(function () {
          if ($(this).val() == '') {

            $(this).addClass('alert-validate');
          }
        });
        return false;
      } else {
        window.location.replace(response['redirect']);
      }
    }
  });
}

function somenteNumeros(num) {
  var er = /[^0-9.]/;
  er.lastIndex = 0;
  var campo = num;
  if (er.test(campo.value)) {
    campo.value = "";
  }
}

function readURL(input, preview) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $(preview).attr('src', e.target.result)
    };
    reader.readAsDataURL(input.files[0]);
  }
  else {
    var img = input.value;
    $(input).next().attr('src', img);
  }
}

function verificaMostraBotao(input, preview) {
  $(input).each(function (index) {
    if ($(input).eq(index).val() != "") {
      readURL(this, preview);
      $('.hide').show();
    }
  });
}
