<!DOCTYPE html>
<html lang="en">
<head>
		<?php include('includes/head.php');?>
</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>	
</header>
<!-- end Header -->
<main id="mainContent">
	<div class="">
		<!-- start section -->
		<section class="videoTeca-banner">
				<div class="banner-topo">
					<div>
						<span class="shape-down"></span>
						<h2>Videoteca</h2>
					</div>
				</div>
		</section>
		<section>
			<div class="container container-select-video">
				<div class="row">
					<div class="col-md-12 box-select-video">
						<select name="" id="">
							<option hide value=""> Classificar por:</option>
							<option value="">a</option>
							<option value="">b</option>
							<option value="">c</option>
						</select>
						<select name="" id="">
							<option hide value=""> Mostrar: </option>
							<option value="">a</option>
							<option value="">b</option>
							<option value="">c</option>
						</select>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		<!-- start section -->
		<section class="">
			<div class="container">
				<div class="list-videoblock">
					<div class="row">
						<div class="col-sm-6">
							<a href="cases_details.html" class="videoblock">
								<div class="videoblock__img">
									<img src="images/videoblock_img_01.jpg" alt="">
									<div class="videoblock__icon"></div>
								</div>
								<div class="videoblock__description">
									<h2 class="videoblock__title">
										DJI Announces Pricing And Availability of Multilink
									</h2>
								</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="cases_details.html" class="videoblock">
								<div class="videoblock__img">
									<img src="images/videoblock_img_02.jpg" alt="">
									<div class="videoblock__icon"></div>
								</div>
								<div class="videoblock__description">
									<h2 class="videoblock__title">
										DJI Announces Pricing And Availability of Multilink
									</h2>
								</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="cases_details.html" class="videoblock">
								<div class="videoblock__img">
									<img src="images/videoblock_img_03.jpg" alt="">
									<div class="videoblock__icon"></div>
								</div>
								<div class="videoblock__description">
									<h2 class="videoblock__title">
										Checklist for Travel - What to take and how to take it
									</h2>
								</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="cases_details.html" class="videoblock">
								<div class="videoblock__img">
									<img src="images/videoblock_img_04.jpg" alt="">
									<div class="videoblock__icon"></div>
								</div>
								<div class="videoblock__description">
									<h2 class="videoblock__title">
										Skiing the Mendenhall Glacier
									</h2>
								</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="cases_details.html" class="videoblock">
								<div class="videoblock__img">
									<img src="images/videoblock_img_05.jpg" alt="">
									<div class="videoblock__icon"></div>
								</div>
								<div class="videoblock__description">
									<h2 class="videoblock__title">
										5 Cold Weather Drone Tips!
									</h2>
								</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="cases_details.html" class="videoblock">
								<div class="videoblock__img">
									<img src="images/videoblock_img_06.jpg" alt="">
									<div class="videoblock__icon"></div>
								</div>
								<div class="videoblock__description">
									<h2 class="videoblock__title">
										DJI OSMO Pocket Unbox, Setup and Overview
									</h2>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="page-nav">
					<a href="#" class="page-nav__btn page-nav__left">
						<i class="btn__icon">
							<i class="fas fa-chevron-left"></i>
						</i>
						<span class="btn__text">ANT</span>
					</a>
					<ul class="page-nav__list">
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
					</ul>
					<a href="#" class="page-nav__btn page-nav__right">
						<span class="btn__text">PROX</span>
						<i class="btn__icon">
							<i class="fas fa-chevron-right"></i>
						</i>
					</a>
				</div>
			</div>
		</section>
		<!-- end section -->
	</div>
</main>
<footer id="footer" class="footer-bg01">
	<?php include('includes/footer.php') ?>
</footer>
</body>
</html>