// ACORDDION


    var acc = document.getElementsByClassName("accordion");
    var i;
    
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = 'unset';
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }

// INPUT 

function showInput(){
  var toggle = document.querySelector('.boxBusca');
  toggle.style.heigth = window.innerHeight - 60 + "px";
  if(toggle.style.top == "100px"){
      toggle.style.top = "-10000px";
  }else {
      toggle.style.top = "100px";
  }
}

  $(document).scroll(function () { 
    var posicaoScroll = $(document).scrollTop(); 
     if (posicaoScroll > 50) $('.boxBusca').animate({'top': "-10000px"}, 100);
  })

  function showMenu(){
    var toggle = document.querySelector('.menu-categoria');
    toggle.style.heigth = window.innerHeight - 60 + "px";
    if(toggle.style.right == "-10000px" && toggle.style.display == "none"){
      toggle.style.display = "block"
        toggle.style.right = "-4px";
    }else {
        toggle.style.display = "block"
        toggle.style.right = "-10000px";
    }
};

function closeMenu(){
  var toggle = document.querySelector('.menu-categoria');
  toggle.style.heigth = window.innerHeight - 60 + "px";
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }
};

function showUl(){
  var toggle = document.querySelector('.box-down-ul-classificados');
  toggle.style.heigth = window.innerHeight - 60 + "px";
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};


function showUlRetreats(){
  var toggle = document.querySelector('.box-down-ul-retreats');
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};
function showUlDicas(){
  var toggle = document.querySelector('.box-down-ul-dicas');
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};
function showUlDados(){
  var toggle = document.querySelector('.box-down-ul-dados');
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};

$( function() {
  $( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: $('#amountMax').attr('data-value'),
    values: [ $('#amountMin').attr('data-valor'), $('#amountMax').attr('data-valor') ],
    slide: function( event, ui ) {
      $("#amountMin" ).val( ui.values[ 0 ]);
      $("#amountMax").val( ui.values[ 1 ] )
    }
  });
  $( "#amountMin" ).val( $( "#slider-range" ).slider( "values", 0 ) );
  $( "#amountMax" ).val( $( "#slider-range" ).slider( "values", 1 ) );
} );

