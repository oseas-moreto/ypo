<div onclick="showMenu()" class="openMenuMobile">
    <button>
      Menu
      <i class="fas fa-bars"></i>
    </button>
</div>
<div class="menu-categoria categoria-principal menu-dica">
  <div class="box-menu-categoria box-menu-classificados">
    <div class="menuCategoria">
      <h2 class="menu-categoria-t1">
        Categorias
      </h2>
      <ul>
        <li>Veículos <span class="qtd">11</span></li>
        <li>Imóveis <span class="qtd">11</span></li>
        <li>Variados <span class="qtd">11</span></li>
        <ul class="list-inputs">
          <li class="t1-cidade"><span class="shape-rigth"></span>Finalidade</li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Quero alugar</label>
          </li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Quero comprar</label>
          </li>
        </ul>
        <ul class="input-valor">
          <li class="t1-cidade"><span class="shape-rigth"></span>Valor</li>
          <div class="main">
            <div id="slider-range"></div>
            <div class="valores">
              <input type="text" id="amountMin" readonly>
              <input type="text" id="amountMax" readonly>
            </div>
          </div>
        </ul>
        <ul class="list-inputs">
          <li class="t1-cidade"><span class="shape-rigth"></span>Cidade</li>
          <li>
            <input type="checkbox" id="check"> <label for="check">São Paulo - Capital</label>
          </li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Rio de Janeiro - RJ</label>
          </li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Belo Horizonte - MG</label>
          </li>
        </ul>
      </ul>
      <div class="box-btn">
        <button class="btnFiltrar">Filtrar</button> <button class="btnLimpar">Limpar Tudo</button>
      </div>
    </div>
</div>  
</div>    
<script src="js/script.js"></script>