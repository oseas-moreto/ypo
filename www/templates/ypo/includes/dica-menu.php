<div onclick="showMenu()" class="openMenuMobile">
    <button>
      Menu
      <i class="fas fa-bars"></i>
    </button>
</div>
<div class="menu-categoria categoria-principal menu-dica">
  <div class="box-menu-categoria box-menu-dica">
    <div class="menuCategoria">
      <h2 class="menu-categoria-t1">
        Categorias
      </h2>
      <ul>
        <li>Viagens e gastronomia</li>
        <li>Restadores de serviços  </li>
        <li>Saúde</li>
        <li>arte e cultura </li>
        <li>esportes e lazer </li>
        <li>Geral</li>
        <ul class="list-inputs">
          <li class="t1-cidade"><span class="shape-rigth"></span>Cidade</li>
          <li>
            <input type="checkbox" id="check"> <label for="check">São Paulo - Capital</label>
          </li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Rio de Janeiro - RJ</label>
          </li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Belo Horizonte - MG</label>
          </li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Brasília - DF</label>
          </li>
          <li>
            <input type="checkbox" id="check"> <label for="check">Campinas - SP</label>
          </li>
        </ul>
      </ul>
    </div>
</div>  
</div>    