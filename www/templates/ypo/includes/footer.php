<footer id="footer" class="no-indent container-footer">
	<div class="container ">
		<div class="row footer-redes">
			<div class="col-md-12 col-lg-5 container-footer-logo">
				<img src="images/Logo.jpg" alt="">
				<span>A comunidade de liderança global de <br> executivos extraordinários.</span>
			</div>
			<div class="col-md-12 col-lg-5 container-footer-rede-sociais">
				<span>
					<i class="fab fa-facebook-f"></i>
				</span>
				<span>
					<i class="fab fa-instagram"></i>
				</span>
				<span>
					<i class="fab fa-linkedin-in"></i>
				</span>
				<span>
					<i class="fab fa-twitter"></i>
				</span>
				<span>
					<i class="fab fa-youtube"></i>
				</span>
			</div>
		</div>
	</div>
<div class="footer-custom container-footer-end">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-12 col-lg-7">
				<div class="copyright">
					&copy; 2020, YPO. TODOS OS DIREITOS RESERVADOS.   |   POLÍTICA DE PRIVACIDADE DA YPO   |   TERMOS DE USO
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-4 ml-md-auto">
				<div class="footer-menu">
					<nav>
						<span>CRIAÇÃO/DESIGN  <span class="color-azul">AGÊNCIA ME</span>   |  DESENVOLVIMENTO  <span class="color-azul">DEEP OCEAN</span></span>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

</footer>

<a href="#" id="js-back-to-top" class="btn-scroll-top">
	<i class="fas fa-chevron-up"></i>
</a>
<?php include('includes/menu-mobile.php') ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/bundle.js"></script>
<script src="js/script.js"></script>
<script src="https://kit.fontawesome.com/ca4f1d2444.js" crossorigin="anonymous"></script>
<div class="pr-svg-sprite">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>