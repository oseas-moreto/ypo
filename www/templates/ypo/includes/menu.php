<div class="container-fluid">
    <div class="row  no-gutters ">
        <div class="col-md-3 side-col d-flex align-items-center text-nowrap">
            <!-- start navigation-toggle -->
				<a href="#" id="top-bar__navigation-toggler" class="hide-object-desktop">
					<span></span>
				</a>
				<!-- end navigation-toggle -->
				<!-- start logo -->
            <a href="index.php" class="top-bar__logo">
                <img src="images/logo.png" alt="">
            </a>
            <!-- end logo -->
        </div>
        <div class="col-md-6">
            <!-- start desktop menu -->

            <nav id="top-bar__navigation">

                <ul>
                    <div class="boxBusca-mobile show-mobile">
                        <div class="divBusca-mobile">
                            <input type="text" id="txtBusca" placeholder="Buscar..."/>
                            <button>
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <li onmouseover="showDestaque(this)">
                        <a href="/ypobrasil/parcerias.php"><span>PARCERIAS</span></a>
                    </li>
                    <li>
                        <a href="/ypobrasil/classificados.php"><span>classificados</span></a>
                    </li>
                    <li>
                        <a href="/ypobrasil/retreats.php"><span>RETREATS</span></a>
                    </li>
                    <li>
                        <a href="/ypobrasil/dica-indicacao.php"><span>DICAS</span></a>
                    </li>
                    <li>
                        <a href="/ypobrasil/videoTeca.php"><span>VideoTeca</span></a>
                    </li>
                </ul>
            </nav>
            <!-- end desktop menu -->
        </div>
        <div class="col-md-3 side-col" onclick="showInput()">
            <span class="box-search"><i class="fas fa-search"></i></span>
            <a href="/ypobrasil/cadastro.php" class="top-bar__custom-btn">
                <span>
                    <i class="far fa-user"></i>
                    Minha conta
                </span>
            </a>
            <!-- end custom btn -->
        </div>
        
    </div>
    <div class="boxBusca show-desktop">
        <div class="divBusca">
            <input type="text" id="txtBusca" placeholder="Buscar..."/>
            <button>
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
    
</div>