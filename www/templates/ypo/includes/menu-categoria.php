<div onclick="showMenu()" class="openMenuMobile">
      <button>
        Menu
        <i class="fas fa-bars"></i>
      </button>
    </div>
<div class="menu-categoria">
  <div class="menu-cadastro-categoria-t1">

  <span class="shape-menu-categoria">
  </span>
    <h2>
      Minha conta
    </h2>
  </div>
  <div class="box-menu-categoria">
    <div class="menuClassificados">
      <h2 class="menu-classificados-t1">
      <img src="/ypobrasil/images/categoria/class.jpg" alt="">
          Classificados
      </h2>
      <ul>
        <li class="ativo">Meus Anúncios <span class="qtd ativo">1</span></li>
        <li>Mensagens <span class="qtd">1</span></li>
        <li>Favoritos<span class="qtd">11</span></li>
        <li onclick="showUl()"><button>Criar Anúncio</button> <i class="fas fa-chevron-right"></i></li>
        <ul class="box-down-ul-classificados">
          <li>A</li>
          <li>B</li>
          <li>C</li>
        </ul>
      </ul>
    </div>
    <div class="menuRetreats">
      <h2 class="menu-retreats-t1">
      <img src="/ypobrasil/images/categoria/retreat.jpg" alt="">
        Retreats
      </h2>
      <ul>
        <li>Meus Retreats <span class="qtd">1</span></li>
        <li>Mensagens <span class="qtd">1</span></li>
        <li>Salvos <span class="qtd">1</span></li>
        <li onclick="showUlRetreats()"><button>Criar Retreats</button> <i class="fas fa-chevron-right"></i></li>
        <ul class="box-down-ul-retreats">
          <li>A</li>
          <li>B</li>
          <li>C</li>
        </ul>
      </ul>
    </div>
    <div class="menuDicas">
      <h2 class="menu-dicas-t1">
      <img src="/ypobrasil/images/categoria/dicas.jpg" alt="">
        Dicas
      </h2>
      <ul>
        <li>Minhas Dicas<span class="qtd">1</span></li>
        <li>Mensagens <span class="qtd">1</span></li>
        <li>Salvos<span class="qtd">1</span></li>
        <li onclick="showUlDicas()"><button>Criar Dicas</button> <i class="fas fa-chevron-right"></i></li>
        <ul class="box-down-ul-dicas">
          <li>A</li>
          <li>B</li>
          <li>C</li>
        </ul>
      </ul>
    </div>
    <div class="menuDados">
      <h2 class="dados-t1">
      <img src="/ypobrasil/images/categoria/dados.jpg" alt="">
        Meus dados
      </h2>
      <ul>
        <li onclick="showUlDados()"><button>Aleterar dados</button> <i class="fas fa-chevron-right"></i></li>
        <ul class="box-down-ul-dados">
          <li>A</li>
          <li>B</li>
          <li>C</li>
        </ul>
      </ul>
    </div>
  </div>
  <div class="menuClose">
      <button>
        Sair
      </button>
    </div>
</div>     