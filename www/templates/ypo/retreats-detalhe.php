<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>

<main id="mainContent" class="">
  <div class="container-retreats-detalhe">
    <div class="container box-retreats-detalhe">
      <div class="box-retreats-img">
        <img src="/ypobrasil/images/retreats/img.jpg" alt="">
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box-retreats-title">
            <h2>Lorem ipsum dolor sit amet, consectetur 
              adipiscing elit, sed do eiusmod tempor</h2>
          </div>
          <div class="col-md-12">
            <div class="box-retreats-text">
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="container-retreats-img-small">
            <div class="row box-retreats-img-small">
            <div class="col-md-4">
              <div class="box-retreats-small-img">
                    <img src="/ypobrasil/images/retreats/img-small.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4">
                <div class="box-retreats-small-img">
                  <img src="/ypobrasil/images/retreats/img-small.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4">
                <div class="box-retreats-small-img">
                  <img src="/ypobrasil/images/retreats/img-small.jpg" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="divisor">
        </div>
        <div class="col-md-12 container-retreats-info">
          <div class="row ">
            <div class="col-md-4 box-retreats-info">
              <span class="info1">Tel/WhatsApp</span>
              <span class="info2">(11) 9999-0000</span>
            </div>
            <div class="col-md-4 box-retreats-info">
              <span class="info1">E-mail</span>
              <span class="info2">johnsample@corporate.com.br</span>
            </div>
            <div class="col-md-4 box-retreats-info">
              <span class="info1">Site</span>
              <span class="info2">corporate.com.br/span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-retreats-detalhe-end container">
    <div class="box-comentario-retreats-detalhe">
        <div class="t1-comentario">
          <h2>Comentários</h2>
        </div>
      <div class="comentario-1">
          <div class="comentario">
            <div class="name-data">
              <span class="name">Rachel Green</span>
              <span class="data">March 19, 2019 / 09:20</span>
            </div>
            <div class="box-comentario">
              <p class="p-comentario">
                  Rivuline pearleye northern Stargazer dogfish, squarehead catfish firefish, snoek armorhead catfish, grunion lake chub barbeled dragonfish. 
              </p>
            </div>
            <div class="resposta">
                <span><button>Responder</button></span>
            </div>
          </div> 
      </div>
      <div class="comentario-2">
          <div class="comentario">
            <div class="name-data">
              <span class="name">Rachel Green</span>
              <span class="data">March 19, 2019 / 09:20</span>
            </div>
            <div class="box-comentario">
              <p class="p-comentario">
                  Rivuline pearleye northern Stargazer dogfish, squarehead catfish firefish, snoek armorhead catfish, grunion lake chub barbeled dragonfish. 
              </p>
            </div>
            <div class="resposta">
                <span><button>Responder</button></span>
            </div>
          </div> 
      </div>
      <div class="comentario-1">
          <div class="comentario">
            <div class="name-data">
              <span class="name">Rachel Green</span>
              <span class="data">March 19, 2019 / 09:20</span>
            </div>
            <div class="box-comentario">
              <p class="p-comentario">
                  Rivuline pearleye northern Stargazer dogfish, squarehead catfish firefish, snoek armorhead catfish, grunion lake chub barbeled dragonfish. 
              </p>
            </div>
            <div class="resposta">
                <span><button>Responder</button></span>
            </div>
          </div> 
      </div>
    </div>
    <div class="container container-retreats-detalhe-form">
    <div class="t1-comentario">
      <h2>Deixa seu comentário</h2>
    </div>
      <form action="">
        <div class="row">
          <div class="col-md-6 input-comentario">
            <input type="text" placeholder="Nome*">
          </div>
          <div class="col-md-6 input-comentario">
          <input type="text" placeholder="E-mail*">
          </div>
          <div class="col-md-12 textarea-comentario">
            <textarea name="" id="" placeholder="Comentário"></textarea>
          </div>
          <div class="col-md-12 button-comentario">
            <button>Enviar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>
<?php include('includes/footer.php') ?>
</body>
</html>