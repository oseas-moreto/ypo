<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>

<main id="mainContent" class="container-categoria container-classificados">
      <section class="dica-indicacao-banner">
				<div class="banner-topo">
					<div>
						<span class="shape-down"></span>
						<h2>Dicas e Indicações</h2>
					</div>
				</div>
		</section>
  <div class="container">
    <div class="row">
        <div class="col-md-4">
          <?php include('includes/classificados-menu.php') ?>
        </div>
        <div class="col-md-8">
            <div class="row">
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-01.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>venda</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-02.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>aluguel</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-01.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>venda</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-02.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>aluguel</span></div>
                  </div>
                </div>
              </div> 
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-01.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>venda</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-02.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>aluguel</span></div>
                  </div>
                </div>
              </div> 
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-01.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>venda</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="item-classificados">
                  <div class="img"><img src="/ypobrasil/images/classificados/img-02.jpg" alt=""></div>
                  <div class="padding">
                    <div class="title"><h2>Apto. Vila Olímpia 230m2 - 4 suites</h2></div>
                    <div class="valor"><span>R$ 0.000.000,00</span></div>
                    <div class="status"><span>aluguel</span></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 container-dica-indicacao-itens">
            <div class="row ">
              <div class="col-md-12">
                <div class="page-nav">
                  <a href="#" class="page-nav__btn page-nav__left">
                    <i class="btn__icon">
                      <i class="fas fa-chevron-left"></i>
                    </i>
                    <span class="btn__text">ANT</span>
                  </a>
                  <ul class="page-nav__list">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                  </ul>
                  <a href="#" class="page-nav__btn page-nav__right">
                    <span class="btn__text">PROX</span>
                    <i class="btn__icon">
                      <i class="fas fa-chevron-right"></i>
                    </i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    </div>
</main>
<?php include('includes/footer.php') ?>
</body>
</html>