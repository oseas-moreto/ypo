<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>

<main id="mainContent" class="container-categoria container-parcerias">
      <section class="parcerias-banner">
				<div class="banner-topo">
					<div>
						<span class="shape-down"></span>
						<h2>Parcerias</h2>
					</div>
				</div>
		</section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="p-categoria">
          O Programa de Parcerias do YPO Brasil está sempre em busca de trazer mais vantagens 
          para os seus membros. Por isso, criamos uma forma mais rápida e fácil para poderem 
          aproveitar todos os nossos benefícios. Experimentem!
        </p>
      </div>
        <div class="col-md-4">
          <?php include('includes/categoria-menu.php') ?>
        </div>
          <div class="col-md-8 container-categoria-itens">
            <div class="row ">
              <div class="col-md-4 categoria-item">
                  <img src="/ypobrasil/images/categoria/item-categoria.jpg" alt="">
              </div>
              <div class="col-md-4 categoria-item">
                  <img src="/ypobrasil/images/categoria/item-categoria.jpg" alt="">
              </div>
              <div class="col-md-4 categoria-item">
                  <img src="/ypobrasil/images/categoria/item-categoria.jpg" alt="">
              </div>
            </div>
          </div>
        </div>
        
    </div>
</main>
<?php include('includes/footer.php') ?>
</body>
</html>