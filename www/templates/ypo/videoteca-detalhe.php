<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>
<!-- end Header -->
<main id="mainContent">
	<div class="container-detalhe-videoTeca">
		<!-- start section -->
		<section class="section section__indent-13">
			<div class="container">
				<a href="http://www.dailymotion.com/video/xxgmlg#.UV71MasY3wE" class="videolink video-popup no-shadow">
					<div class="videolink__icon"></div>
					<img src="images/img_02.jpg" alt="">
				</a>
				<div class="row indent-top">
					<div class="col-lg-5 col-xl-4">
						<div class="box-table-01">
							<table>
								<tbody>
									<tr>
										<td>Cetegoria:</td>
										<td>Negócios</td>
									</tr>
									<tr>
										<td>Webinar:</td>
										<td>John Sample</td>
									</tr>
									<tr>
										<td>Data:</td>
										<td>20/03/2021</td>
									</tr>
									<tr>
										<td>URL:</td>
										<td><a href="www.JBD.com" target="_blank">johnsample.com.br</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="divider divider__lg d-block d-xl-none d-lg-none"></div>
					<div class="col-lg-6 box-texto-videoteca-detalhe">
						<div class="section-heading">
							<h4 class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
						</div>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
						</p>
					</div>
				</div>

			</div>
		</section>
		<!-- end section -->
	</div>
	<div class="container-detalhe-videoTeca-relacionados">
		<div class="relacionados-t1 container">
			<div class="t1">
				<span class="shape-right">
				</span>
				<h2>Vídeos relacionados</h2>
			</div>
			<div class="verTodos">
				<i class="fas fa-chevron-right"></i><a href="">Ver todos</a>
			</div>
		</div>
		<div class="list-videoblock container">
			<div class="row">
				<div class="col-sm-6">
					<a href="cases_details.html" class="videoblock">
						<div class="videoblock__img">
							<img src="images/videoblock_img_01.jpg" alt="">
							<div class="videoblock__icon"></div>
						</div>
						<div class="videoblock__description">
							<h2 class="videoblock__title">
								DJI Announces Pricing And Availability of Multilink
							</h2>
						</div>
					</a>
				</div>
				<div class="col-sm-6">
					<a href="cases_details.html" class="videoblock">
						<div class="videoblock__img">
							<img src="images/videoblock_img_02.jpg" alt="">
							<div class="videoblock__icon"></div>
						</div>
						<div class="videoblock__description">
							<h2 class="videoblock__title">
								DJI Announces Pricing And Availability of Multilink
							</h2>
						</div>
					</a>
				</div>
				<div class="col-sm-12">
					<div class="page-nav-img">
						<a href="#" class="page-nav-img__btn btn-prev box-icon">
							<span class="wrapper-img"></span>
							<i class="fas fa-chevron-left"></i>
							<span class="btn__text">Anterior</span>
						</a>
						<a href="#" class="page-nav-img__btn btn-next box-icon">
							<span class="wrapper-img"></span>
							<span class="btn__text">Próximo</span>
								<i class="fas fa-chevron-right"></i>
						</a>
					</div>
				</div>
			</div>
		
		</div>

	</div>
</main>
<?php include('includes/footer.php') ?>
</body>
</html>