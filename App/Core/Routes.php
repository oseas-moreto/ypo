<?php
namespace App\Core;

/**
 *
 */
class Routes {
    public function initroute($folder, $controller, $method, $params = []){
        $file = str_replace('.', '/', $folder);
        $class = str_replace('.', '\\', $folder);
        
        require_once '../App/Controllers/'.$file.'/'. ucfirst($controller) . '.php';
        $class = "App\\Controllers\\".$class."\\".ucfirst($controller);
        $contr = new $class;

        if(method_exists($contr, $method)){
            call_user_func_array(array($contr, $method), $params);
        }
    }
}


?>
