<?php

namespace App\Core;

use MVC\Di\Container;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Controller
 *
 * @author oseas
 */
class Models extends Model{
    public function __construct() {

    }

    public function getNextUserID($table, $id, $class){
        $statement = $class::whereRaw(''.$id.' = (select max(`'.$id.'`) from '.$table.')')->first();

        return !isset($statement->$id) ? 1 : $statement->$id+1;
    } 
}
