<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Core;

/**
 * Description of Action
 *
 * @author oseas
 */
class Action
{

    protected $view;
    protected $action;
    protected $folder;
    protected $template = 'Default';
    protected $page;
    protected $typeFile = 'phtml';

    public function __construct()
    {
        $this->view = new \stdClass();
    }

    public function render($action, $folder = 'Site', $page = 'layout', $layout = true)
    {
        $this->action = $action;
        $this->folder = $folder;
        $this->page   = $page;
        
        if($layout == true && file_exists('../App/Views/' . $folder . '/' . $page . '.' . $this->typeFile)) {
            $this->layout();
        } else if ($layout == false) {
            $this->content();
        }

    }

    public function redirect($url)
    {
        $url = self::tratarUrl($url);
        header("Location: /" . $url);
        exit();
    }

    public function layout()
    {
        $file = self::tratarUrl($this->page);
        include_once '../App/Views/' . $this->folder . '/' . $file . '.' . $this->typeFile;
    }

    public function content()
    {
        $url = str_replace('.', '/', $this->action);
        include_once '../App/Views/' . $this->folder . '/' . $url . '.' . $this->typeFile;
    }

    public function loadPage($page)
    {
        include_once '../App/Views/' . $this->folder . '/Includes/' . $page . '.' . $this->typeFile;
    }

    public function includepage($url)
    {
        $url = self::tratarUrl($url);
        include_once '../App/Views/' . $url . '.' . $this->typeFile;
    }

    public static function tratarUrl($url){
        return str_replace('.', '/', $url);
    }

}
