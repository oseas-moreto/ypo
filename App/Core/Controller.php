<?php

namespace App\Core;

use App\Classes\Funcoes\SessionControl;
use App\Core\Action;
use App\Classes\Funcoes\Generate;

/**
 * Description of Controller
 *
 * @author oseas
 */
class Controller extends Action
{

    public function rgb($hexdec)
    {
        $hexdec = str_replace('#', '', $hexdec);
        $split = str_split($hexdec, 2);
        $r = hexdec($split[0]);
        $g = hexdec($split[1]);
        $b = hexdec($split[2]);

        return $r . ", " . $g . ", " . $b;
    }

    public function print_pre($input)
    {
        echo '<pre>';
        print_r($input);
        exit();
    }

    public function antiInjection($var, $q = '')
    {
        //Verifica se o parâmetro é um array
        if (!is_array($var)) {
            //identifico o tipo da variável e trato a string
            switch (gettype($var)) {
                case 'double':
                case 'integer':
                    if ($var == null) {
                        $var = 0;
                    }
                    $return = $var;
                    break;
                case 'string':
                    /*Verifico quantas vírgulas tem na string.
                    Se for mais de uma trato como string normal,
                    caso contrário trato como String Numérica*/
                    $temp = (substr_count($var, ',') == 1) ? str_replace(',', '*;*', $var) : $var;
                    //aqui eu verifico se existe valor para não adicionar aspas desnecessariamente
                    if (!empty($temp)) {
                        if (is_numeric(str_replace('*;*', '.', $temp))) {
                            $temp = str_replace('*;*', '.', $temp);
                            $return = strstr($temp, '.') ? floatval($temp) : intval($temp);
                        } elseif (get_magic_quotes_gpc()) {
                            //aqui eu verifico o parametro q para o caso de ser necessário utilizar LIKE com %
                            $return = (empty($q)) ? '\'' . str_replace('*;*', ',', $temp) . '\'' : '\'%' . str_replace('*;*', ',', $temp) . '%\'';
                        } else {
                            //aqui eu verifico o parametro q para o caso de ser necessário utilizar LIKE com %
                            $return = (empty($q)) ? '\'' . addslashes(str_replace('*;*', ',', $temp)) . '\'' : '\'%' . addslashes(str_replace('*;*', ',', $temp)) . '%\'';
                        }
                    } else {
                        // $return = $temp;
                        return '\'\'';
                    }
                    break;
                case '':
                    return '\'\'';
                    break;
                case empty($temp):
                    return '\'\'';
                    break;
                case null:
                    return '\'\'';
                    break;
                default:
                    /*Abaixo eu coloquei uma msg de erro para poder tratar
                    antes de realizar a query caso seja enviado um valor
                    que nao condiz com nenhum dos tipos tratatos desta
                    função. Porém você pode usar o retorno como preferir*/
                    $return = '\'\'';
            }
            //Retorna o valor tipado
            return $return;
        } else {
            //Retorna os valores tipados de um array
            return array_map('antiInjection', $var);
        }
    }

    public function limits_characters($texto, $limite, $quebra = true)
    {
        $tamanho = strlen($texto);
        if ($tamanho <= $limite) { //Verifica se o tamanho do texto é menor ou igual ao limite
            $novo_texto = $texto;
        } else { // Se o tamanho do texto for maior que o limite
            if ($quebra == true) { // Verifica a opção de quebrar o texto
                $novo_texto = trim(substr($texto, 0, $limite)) . "...";
            } else { // Se não, corta $texto na última palavra antes do limite
                $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
                $novo_texto = trim(substr($texto, 0, $ultimo_espaco)) . "..."; // Corta o $texto até a posição localizada
            }
        }
        return $novo_texto; // Retorna o valor formatado
    }

    public function tagreplace($field)
    {
        $val = trim(strip_tags($field));
        $val = preg_replace('[\r\n]', '\n', $val);
        $val = preg_replace('[&quot;]', '"', $val);
        $val = preg_replace('[&amp;]', '&', $val);
        $val = preg_replace('/(\v|\s)+/', ' ', $val);
        $val = htmlspecialchars(trim(strip_tags($val)));

        return $val;
    }

    public function upload($file, $path)
    {
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');
        // A list of permitted file extensions
        $allowed = array('png', 'jpg', 'gif', 'jpeg', 'webp', 'pdf', 'doc', 'vcf', 'zip', 'rar', 'xlsx', 'csv');
        SessionControl::start_session();
        $response = [];
        $new_name = '';

        if (isset($file['file']) && $file['file']['error'] == 0) {
            $caminho = $_SERVER['DOCUMENT_ROOT'] . '/images/' . $path;
            $c = 'images';

            $extension = pathinfo($file['file']['name'], PATHINFO_EXTENSION);
            //echo $extension;
            
            if (!in_array(strtolower($extension), $allowed)) {
                $response['success'] = false;
                $response['message'] = 'As extensões permitidas são: ' . implode(',', $allowed);
                return $response;
            }

            if ($extension == 'pdf' || $extension == 'doc' || $extension == 'vcf' || $extension == 'zip' || $extension == 'rar' || $extension == 'xlsx' || $extension == 'csv') {
                $caminho = str_replace('images', 'arquivos', $caminho);
                $c = str_replace('images', 'arquivos', $c);
            }

            if (!is_dir($caminho)) {
                mkdir($caminho, 0777);
            }

            $name = pathinfo($file['file']['name']);
            $new_name = Generate::url_generate($name['filename']) .date('YmdHis'). '.' . $extension;

            if (move_uploaded_file($file['file']['tmp_name'], $caminho . '/' . $new_name)) {
                $response['success'] = true;
                $response['message'] = 'Upload feito com sucesso!';
                $response['image'] = '/' . $c . '/' . $path . '/' . $new_name;
            } else {
                $response['success'] = false;
                $response['message'] = 'Ocorreu algum erro ao subir a imagem!';
            }
        } else {
            $response['success'] = false;
            switch ($file['file']['error']) {
                case 1:
                    $response['message'] = 'Imagem muito grande';
                    break;
                default:
                    $response['message'] = 'As extensões permitidas são: ' . implode(',', $allowed);
                    break;
            }
        }

        return $response;
    }
}
