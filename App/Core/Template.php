<?php

namespace App\Core;

use App\Core\Action;
use App\Templates\Ypo\Modulos\TemplateLoad;
use stdClass;

class Template extends Action
{

    protected static $pagelink = '';
    protected static $pasta_default = '';

    public static function indexSistemaDefault(stdClass $parameters){
        $actionClass = new self();

        $actionClass->id_class      = $parameters->class;
        $actionClass->titulo_pagina = $parameters->title;
        self::$pagelink             = $parameters->pagelink;
        self::$pasta_default        = 'Ypo';

        $objs = $parameters->list;

        $breadcrumb['url'][0]          = '#';
        $breadcrumb['label'][0]        = $parameters->title;
        $breadcrumb['active'][0]       = 'active';
        $actionClass->view->breadcrumb = $breadcrumb;

        $actionClass->view->url    = '/' . self::$pagelink;
        $actionClass->view->titulo = $parameters->title;
        $actionClass->view->objs   = $objs;

        $parameters->siderbar = isset($parameters->siderbar) ? $parameters->siderbar : true;

        if (isset($_SESSION['message'])) {
            $actionClass->view->errormessage = isset($_SESSION['message']) ? $_SESSION['message'] : '';
            $actionClass->view->classe       = isset($_SESSION['classe']) ? $_SESSION['classe'] : '';
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $actionClass->view->info     = TemplateLoad::info();
        $actionClass->view->retorno  = $parameters->siderbar ? TemplateLoad::sidebar(): [];
        $actionClass->view->pagelink = self::$pagelink;
        if (isset($parameters->adds)) {
            $actionClass->view->adds = $parameters->adds;
        }
        
        $actionClass->render($parameters->view, self::$pasta_default, $parameters->page);
    }

    public static function formSistemaDefault(stdClass $parameters)
    {
        $actionClass = new self();

        $actionClass->id_class = $parameters->class;
        $actionClass->titulo_pagina = $parameters->title;
        self::$pagelink = $parameters->pagelink;
        self::$pasta_default = 'Ypo';

        $actionClass->view->url = '/' . self::$pagelink;
        $actionClass->view->action = '/' . self::$pagelink . ($parameters->id != '' ? '/update/' . $parameters->id : '/create');
        $actionClass->view->titulo = $parameters->title;
        $actionClass->view->label = $parameters->id != '' ? 'Editar' : 'Cadastrar';
        $actionClass->view->class_btn = $parameters->id != '' ? 'btn btn-warning' . $parameters->id : 'btn btn-success';
        $actionClass->view->obj = $parameters->obj;

        $breadcrumb['url'][0] = '/' . self::$pagelink;
        $breadcrumb['label'][0] = 'Lista de '.$parameters->title;
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = $parameters->id != '' ? 'Editar' : 'Cadastrar';
        $breadcrumb['active'][1] = 'active';
        $actionClass->view->breadcrumb = $breadcrumb;

        if (isset($_SESSION['message'])) {
            $actionClass->view->errormessage = isset($_SESSION['message']) ? $_SESSION['message'] : '';
            $actionClass->view->classe = isset($_SESSION['classe']) ? $_SESSION['classe'] : '';
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $actionClass->view->info = TemplateLoad::info();
        $actionClass->view->retorno = TemplateLoad::sidebar();
        $actionClass->view->pagelink = self::$pagelink;
        if (isset($parameters->adds)) {
            $actionClass->view->adds = $parameters->adds;
        }

        $actionClass->render($parameters->view, self::$pasta_default, $parameters->page);
    }

    public static function pageDefault(stdClass $parameters)
    {
        $actionClass = new self();
        self::$pagelink = $parameters->pagelink;
        self::$pasta_default = 'Ypo';

        if (isset($parameters->adds)) {
            $actionClass->view->adds = $parameters->adds;
        }

        $actionClass->view->info = TemplateLoad::info();
        $actionClass->view->pagelink = self::$pagelink;
        $actionClass->view->title = $parameters->title;
        $actionClass->view->class = isset($parameters->class) ? $parameters->class : '';
        $actionClass->view->label = isset($parameters->label) ? $parameters->label : '';
        $actionClass->render($parameters->view, self::$pasta_default, $parameters->page);

    }
}
