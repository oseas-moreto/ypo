<?php

namespace App\Routes;

use Slim\Slim;
use \App\Core\Routes;

\Slim\Slim::registerAutoloader();

class Web extends Routes
{

    protected $routes = [];
    protected $routescrud = [];

    public function __construct()
    {
        $app = new \Slim\Slim(array(
            'templates.path' => 'templates',
            'debug' => true
        ));

        // rotas index
        $this->routes = [
            '/',
            '/home',
            '/index',
            '/dashboard',
        ];

        //rotas 
            foreach ($this->routes as $route) {
                $app->get($route, function () use ($app) {
                    $this->initroute('Sistema', 'Home', 'index');
                });
            }

            $app->map('/perfil/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Sistema', 'Home', 'perfil', $data);
            })->via('GET', 'POST');


            $app->map('/home/compress', function () use ($app) {
                $this->initroute('Sistema', 'Home', 'compress');
            })->via('GET', 'POST');

            //rotas login
            $app->group('/login', function () use ($app) {

                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Login', 'index');
                });

                $app->post('/logar', function () use ($app) {
                    $request = $app->request->getBody();
                    parse_str($request, $get_array);

                    $data[] = $get_array;

                    $this->initroute('Sistema', 'Login', 'logar', $data);
                });

                $app->post('/retrievepassword', function () use ($app) {
                    $request = $app->request->getBody();
                    parse_str($request, $get_array);

                    $data[] = $get_array;

                    $this->initroute('Sistema', 'Login', 'retrievepassword', $data);
                });

                $app->get('/logout', function () use ($app) {
                    $this->initroute('Sistema', 'Login', 'logout');
                });
            });

            $app->get('/logout', function () use ($app) {
                $this->initroute('Sistema', 'Login', 'logout');
            });

            //rotas contato
            $app->group('/contacts', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Contact', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Contact', 'index');
                });

                $app->map('/view/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Contact', 'view', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Contact', 'destroy', $data);
                });
            });

            //rotas grupos
            $app->group('/gruposacesso', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Groups', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Groups', 'destroy', $data);
                });
            });

            //rotas menus
            $app->group('/menus', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Menus', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Menus', 'destroy', $data);
                });
            });

            //rotas menu-site
            $app->group('/menu-site', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'MenuSite', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'MenuSite', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'MenuSite', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'MenuSite', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'MenuSite', 'destroy', $data);
                });
            });

            //rotas users
            $app->group('/users', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'User', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/:id/uploads', function ($id) use ($app) {
                    $this->initroute('Sistema', 'Home', 'uploads');
                })->via('GET', 'POST');

                $app->map('/:id/update/:id2/', function ($id, $id2) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Home', 'perfil', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'User', 'destroy', $data);
                });

                $app->get('/block/:id/:s', function ($id, $s) use ($app) {
                    $data[] = $id;
                    $data[] = $s;
                    $this->initroute('Sistema', 'User', 'block', $data);
                });
            });

            //rotas settings
            $app->group('/settings', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Settings', 'index', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Settings', 'destroy', $data);
                });
            });

            //rotas settings
            $app->group('/social', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Social', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Social', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Social', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Social', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Social', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Social', 'destroy', $data);
                });
            });

            //rotas slides
            $app->group('/slides', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Slide', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Slide', 'destroy', $data);
                });
            });

            //rotas institucional
            $app->group('/institucional', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Institucional', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Institucional', 'destroy', $data);
                });
            });

            //rotas modules
            $app->group('/modules', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Module', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Module', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Module', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Module', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Module', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Module', 'destroy', $data);
                });
            });

            //rotas units
            $app->group('/units', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Unit', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Unit', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Unit', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Unit', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Unit', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Unit', 'destroy', $data);
                });
            });

            //rotas title
            $app->group('/translates', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Translate', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Translate', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Translate', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Translate', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Translate', 'destroy', $data);
                });
            });

            //rotas languages
            $app->group('/languages', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Languages', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Languages', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Languages', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Languages', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Languages', 'destroy', $data);
                });
            });

            //rotas seo
            $app->group('/seo', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Seo', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Seo', 'index');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Seo', 'index', $data);
                })->via('GET', 'POST');
            });

                
            $app->group('/categories', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Category', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Category', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Category', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Category', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Category', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Category', 'destroy', $data);
                });
            });

                
            $app->group('/testimonial', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Testimonial', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Testimonial', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Testimonial', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Testimonial', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Testimonial', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Testimonial', 'destroy', $data);
                });
            });

                
            $app->group('/clients', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Client', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Client', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Client', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Client', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Client', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Client', 'destroy', $data);
                });
            });

            //rotas products
            $app->group('/products', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Product', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Product', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Product', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Product', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Product', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Product', 'destroy', $data);
                });

                
                $app->group('/categories', function () use ($app) {
                    $app->map('/', function () use ($app) {
                        $this->initroute('Sistema', 'ProductCategory', 'index');
                    })->via('GET', 'POST');

                    $app->map('/index', function () use ($app) {
                        $this->initroute('Sistema', 'ProductCategory', 'index');
                    })->via('GET', 'POST');

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'ProductCategory', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ProductCategory', 'update', $data);
                    })->via('GET', 'POST');

                    $app->map('/uploads', function () use ($app) {
                        $this->initroute('Sistema', 'ProductCategory', 'uploads');
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ProductCategory', 'destroy', $data);
                    });
                });

                
                $app->group('/segments', function () use ($app) {
                    $app->map('/', function () use ($app) {
                        $this->initroute('Sistema', 'ProductSegment', 'index');
                    })->via('GET', 'POST');

                    $app->map('/index', function () use ($app) {
                        $this->initroute('Sistema', 'ProductSegment', 'index');
                    })->via('GET', 'POST');

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'ProductSegment', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ProductSegment', 'update', $data);
                    })->via('GET', 'POST');

                    $app->map('/uploads', function () use ($app) {
                        $this->initroute('Sistema', 'ProductSegment', 'uploads');
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ProductSegment', 'destroy', $data);
                    });
                });

                
                $app->group('/services', function () use ($app) {
                    $app->map('/', function () use ($app) {
                        $this->initroute('Sistema', 'ProductService', 'index');
                    })->via('GET', 'POST');

                    $app->map('/index', function () use ($app) {
                        $this->initroute('Sistema', 'ProductService', 'index');
                    })->via('GET', 'POST');

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'ProductService', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ProductService', 'update', $data);
                    })->via('GET', 'POST');

                    $app->map('/uploads', function () use ($app) {
                        $this->initroute('Sistema', 'ProductService', 'uploads');
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ProductService', 'destroy', $data);
                    });
                });
            });

            //rotas type
            $app->group('/faq', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Faq', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Faq', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Faq', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Faq', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Faq', 'destroy', $data);
                });
            });
            //rotas type
            $app->group('/departments', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Department', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Department', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Department', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Department', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Department', 'destroy', $data);
                });
            });
            //rotas type
            $app->group('/office', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Office', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Office', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Office', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Office', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Office', 'destroy', $data);
                });
            });
            //rotas type
            $app->group('/teams', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Teams', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Teams', 'destroy', $data);
                });
            });
            //rotas type
            $app->group('/newsletters', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Newsletter', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Newsletter', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Newsletter', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Newsletter', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Newsletter', 'destroy', $data);
                });
            });

            //rotas blog
            $app->group('/blogs', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Blog', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Blog', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Blog', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Blog', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Blog', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Blog', 'destroy', $data);
                });

                $app->group('/authors', function () use ($app) {
                    $app->get('/', function () use ($app) {
                        $this->initroute('Sistema', 'BlogAuthor', 'index');
                    });

                    $app->get('/index', function () use ($app) {
                        $this->initroute('Sistema', 'BlogAuthor', 'index');
                    });

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'BlogAuthor', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'BlogAuthor', 'update', $data);
                    })->via('GET', 'POST');

                    $app->map('/uploads', function () use ($app) {
                        $this->initroute('Sistema', 'BlogAuthor', 'uploads');
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'BlogAuthor', 'destroy', $data);
                    });
                });

                //rotas type
                $app->group('/types', function () use ($app) {
                    $app->get('/', function () use ($app) {
                        $this->initroute('Sistema', 'BlogType', 'index');
                    });

                    $app->get('/index', function () use ($app) {
                        $this->initroute('Sistema', 'BlogType', 'index');
                    });

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'BlogType', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'BlogType', 'update', $data);
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'BlogType', 'destroy', $data);
                    });
                });
            });

        $app->run();
    }
}
