<?php

namespace App\Routes;

use Slim\Slim;
use \App\Core\Routes;

\Slim\Slim::registerAutoloader();

class Ypo extends Routes
{

    protected $routes = [];
    protected $routescrud = [];

    public function __construct()
    {
        $app = new \Slim\Slim(array(
            'templates.path' => 'templates',
            'debug' => true
        ));

        // rotas index
        $this->routes = [
            '/',
            '/home',
            '/index',
            '/principal'
        ];

        //ROTAS ADMINISTRATIVAS
        //rotas login
        $app->group('/login', function () use ($app) {

            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Login', 'index');
            });

            $app->post('/logar', function () use ($app) {
                $request = $app->request->getBody();
                parse_str($request, $get_array);

                $data[] = $get_array;

                $this->initroute('Ypo.Administrativo', 'Login', 'logar', $data);
            });

            $app->post('/retrievepassword', function () use ($app) {
                $request = $app->request->getBody();
                parse_str($request, $get_array);

                $data[] = $get_array;

                $this->initroute('Ypo.Administrativo', 'Login', 'retrievepassword', $data);
            });

            $app->get('/logout', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Login', 'logout');
            });
        });

        $app->get('/logout', function () use ($app) {
            $this->initroute('Ypo.Administrativo', 'Login', 'logout');
        });

        $app->map('/dashboard', function () use ($app) {
            $this->initroute('Ypo.Administrativo', 'Dashboard', 'index');
        })->via('GET', 'POST');

        //rotas grupos
        $app->group('/gruposacesso', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Groups', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Groups', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Groups', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Groups', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Groups', 'destroy', $data);
            });
        });

        //rotas menus
        $app->group('/menus', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Menus', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Menus', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Menus', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Menus', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Menus', 'destroy', $data);
            });
        });

        //rotas users
        $app->group('/users', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'User', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'User', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'User', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'User', 'update', $data);
            })->via('GET', 'POST');

            $app->map('/:id/uploads', function ($id) use ($app) {
                $this->initroute('Ypo.Administrativo', 'Home', 'uploads');
            })->via('GET', 'POST');

            $app->map('/:id/update/:id2/', function ($id, $id2) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Home', 'perfil', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'User', 'destroy', $data);
            });

            $app->get('/block/:id/:s', function ($id, $s) use ($app) {
                $data[] = $id;
                $data[] = $s;
                $this->initroute('Ypo.Administrativo', 'User', 'block', $data);
            });
        });

        //rotas users
        $app->group('/cadastro-spouse', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Spouse', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Spouse', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Spouse', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Spouse', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Spouse', 'destroy', $data);
            });
        });

        //rotas users
        $app->group('/cadastro-diretoria', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Diretoria', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Diretoria', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Diretoria', 'create');
            })->via('GET', 'POST');

            $app->map('/atualizarOrdemCapitulo', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Diretoria', 'atualizarOrdemCapitulo');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Diretoria', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Diretoria', 'destroy', $data);
            });
        });

        //rotas seo
        $app->group('/seo', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Seo', 'index');
            })->via('GET', 'POST');

            $app->map('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Seo', 'index');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Seo', 'index', $data);
            })->via('GET', 'POST');
        });

        //rotas settings
        $app->group('/settings', function () use ($app) {
            $app->map('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Settings', 'index');
            })->via('GET', 'POST');

            $app->map('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Settings', 'index');
            })->via('GET', 'POST');

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Settings', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Settings', 'index', $data);
            })->via('GET', 'POST');

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Settings', 'uploads');
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Settings', 'destroy', $data);
            });
        });

        //rotas slides
        $app->group('/slides', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Slide', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Slide', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Slide', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Slide', 'update', $data);
            })->via('GET', 'POST');

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Slide', 'uploads');
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Slide', 'destroy', $data);
            });
        });

                
        $app->group('/categories', function () use ($app) {
            $app->map('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Category', 'index');
            })->via('GET', 'POST');

            $app->map('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Category', 'index');
            })->via('GET', 'POST');

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Category', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Category', 'update', $data);
            })->via('GET', 'POST');

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Category', 'uploads');
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Category', 'destroy', $data);
            });

            $app->map('/load/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Category', 'load', $data);
            })->via('GET', 'POST');
        });

        //rotas institucional
        $app->group('/institucional', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Institucional', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Institucional', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Institucional', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Institucional', 'update', $data);
            })->via('GET', 'POST');

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Institucional', 'uploads');
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Institucional', 'destroy', $data);
            });
        });
        //rotas settings
        $app->group('/social', function () use ($app) {
            $app->map('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Social', 'index');
            })->via('GET', 'POST');

            $app->map('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Social', 'index');
            })->via('GET', 'POST');

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Social', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Social', 'update', $data);
            })->via('GET', 'POST');

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Social', 'uploads');
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Social', 'destroy', $data);
            });
        });

        //rotas menu-site
        $app->group('/menu-site', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'MenuSite', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'MenuSite', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'MenuSite', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'MenuSite', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'MenuSite', 'destroy', $data);
            });
        });

        //rotas empresa
        $app->group('/empresa', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Empresa', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Empresa', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Empresa', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Empresa', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Empresa', 'destroy', $data);
            });

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Empresa', 'uploads');
            })->via('GET', 'POST');
        });

        //rotas classificados
        $app->group('/cadastro-classificados', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Classificado', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Classificado', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Classificado', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Classificado', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Classificado', 'destroy', $data);
            });

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Classificado', 'uploads');
            })->via('GET', 'POST');
        });

        //rotas dicas
        $app->group('/dicas', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Dica', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Dica', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Dica', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Dica', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Dica', 'destroy', $data);
            });
        });

        //rotas Videoteca
        $app->group('/videoteca-cadastro', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Videoteca', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Videoteca', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Videoteca', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Videoteca', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Videoteca', 'destroy', $data);
            });

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Videoteca', 'uploads');
            })->via('GET', 'POST');
        });

        //rotas destaque
        $app->group('/destaques-home', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Destaque', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Destaque', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Destaque', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Destaque', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Destaque', 'destroy', $data);
            });

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Destaque', 'uploads');
            })->via('GET', 'POST');
        });

        //rotas retreats
        $app->group('/retreats', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Retreat', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Retreat', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Retreat', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Retreat', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Retreat', 'destroy', $data);
            });

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Retreat', 'uploads');
            })->via('GET', 'POST');

            $app->map('/upload-file', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Retreat', 'uploadFile');
            })->via('GET', 'POST');

            $app->map('/deleteFile', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Retreat', 'deleteFile');
            })->via('GET', 'POST');
        });

        //rotas retreats
        $app->group('/arquivos-retreats', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'index');
            });

            $app->map('/create', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'create');
            })->via('GET', 'POST');

            $app->map('/update/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'update', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'destroy', $data);
            });

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'uploads');
            })->via('GET', 'POST');

            $app->map('/upload-file', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'uploadFile');
            })->via('GET', 'POST');

            $app->map('/deleteFile', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'RetreatFile', 'deleteFile');
            })->via('GET', 'POST');
        });

        //rotas retreats
        $app->group('/solicitacoes-desconto', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Solicitacao', 'index');
            });

            $app->get('/index', function () use ($app) {
                $this->initroute('Ypo.Administrativo', 'Solicitacao', 'index');
            });

            $app->map('/view/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Solicitacao', 'view', $data);
            })->via('GET', 'POST');

            $app->get('/destroy/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Ypo.Administrativo', 'Solicitacao', 'destroy', $data);
            });
        });

        $app->map('/importToJson', function () use ($app) {
            $this->initroute('Ypo.Administrativo', 'User', 'importToJson');
        })->via('GET', 'POST');

        $app->map('/importToJsonSpouse', function () use ($app) {
            $this->initroute('Ypo.Administrativo', 'Spouse', 'importToJsonSpouse');
        })->via('GET', 'POST');

        $app->map('/perfil/:id', function ($id) use ($app) {
            $data[] = $id;
            $this->initroute('Ypo.Administrativo', 'Dashboard', 'perfil', $data);
        })->via('GET', 'POST');

        //rotas 
        foreach ($this->routes as $route) {
            $app->get($route, function () use ($app) {
                $this->initroute('Ypo.Institucional', 'Home', 'index');
            });
        }

        $app->map('/turismo', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Turismo', 'index');
        })->via('GET', 'POST');

        $app->map('/parcerias', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Parcerias', 'index');
        })->via('GET', 'POST');

        $app->map('/parcerias/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Parcerias', 'index', $data);
        })->via('GET', 'POST');

        $app->map('/parceiro/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Parcerias', 'post', $data);
        })->via('GET', 'POST');

        $app->map('/parceiro-solicitar', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Parcerias', 'solicitar');
        })->via('GET', 'POST');

        $app->map('/classificados', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Classificados', 'index');
        })->via('GET', 'POST');

        $app->map('/classificados/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Classificados', 'index', $data);
        })->via('GET', 'POST');

        $app->map('/classificado/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Classificados', 'post', $data);
        })->via('GET', 'POST');

        $app->map('/listagem-retreats', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Retreats', 'index');
        })->via('GET', 'POST');

        $app->map('/listagem-retreats/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Retreats', 'index', $data);
        })->via('GET', 'POST');

        $app->map('/retreat/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Retreats', 'post', $data);
        })->via('GET', 'POST');

        $app->map('/listagem-dicas', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Dicas', 'index');
        })->via('GET', 'POST');

        $app->map('/listagem-dicas/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Dicas', 'index', $data);
        })->via('GET', 'POST');

        $app->map('/enviar-dica', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Dicas', 'enviarDica');
        })->via('GET', 'POST');

        $app->map('/importardicas', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Dicas', 'importardicas');
        })->via('GET', 'POST');

        $app->map('/videoteca', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Videoteca', 'index');
        })->via('GET', 'POST');

        $app->map('/videoteca/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Videoteca', 'index', $data);
        })->via('GET', 'POST');

        $app->map('/videoteca/video/:id/:text', function ($id, $text) use ($app) {
            $data[] = $id;
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Videoteca', 'post', $data);
        })->via('GET', 'POST');

        $app->map('/buscar/:text', function ($text) use ($app) {
            $data[] = $text;
            $this->initroute('Ypo.Institucional', 'Home', 'buscar', $data);
        })->via('GET', 'POST');

        $app->get('/page/:hash', function ($hash) use ($app) {
            $data[] = $hash;
            $this->initroute('Ypo.Institucional', 'Home', 'institucional', $data);
        });

        $app->map('/loadPorItens', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Dicas', 'loadPorItens');
        })->via('GET', 'POST');

        $app->map('/diretoria', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Diretoria', 'index');
        })->via('GET', 'POST');

        $app->map('/contato', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Contato', 'index');
        })->via('GET', 'POST');

        $app->map('/contato/', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Contato', 'index');
        })->via('GET', 'POST');

        $app->map('/contato2', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Contato', 'index2');
        })->via('GET', 'POST');

        $app->map('/spouses', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Spouses', 'index');
        })->via('GET', 'POST');

        $app->map('/retreats-file', function () use ($app) {
            $this->initroute('Ypo.Institucional', 'Retreats', 'newIndex');
        })->via('GET', 'POST');

        $app->run();
    }
}
