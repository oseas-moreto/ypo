<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteEmpresa;
use App\Classes\Funcoes\Generate;
use App\Templates\Ypo\Modulos\TemplateLoad;
use App\Models\Entities\AccessUser;
use App\Classes\Funcoes\SessionControl;
use stdClass;

class TemplateParcerias extends Template{
    
    public static function index($idCategoria = 0){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Parcerias.index';
        $parameters->pagelink = 'home';

        $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'parceiros')
        ->orderBy('order', 'ASC')
        ->get();

        $pathbreadcrumb = [
            [
                'label'  => 'Parcerias',
                'link'   => '/parcerias',
                'active' => false
            ]
        ];

        foreach ($parameters->adds->categorias as $key => $value) {
           $value->url = '/parcerias/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        if($idCategoria == 0){
            $idCategoria = $parameters->adds->categorias[0]->idcategory;
        }

        $categoria   = SiteCategory::where('idlanguage', '=', 1)
        ->where('idcategory', '=', $idCategoria)
        ->first();

        $pathbreadcrumb[] = [
            'label'  => $categoria->title,
            'link'   => '/parcerias/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
            'active' => true
        ];
        
        $parameters->adds->list = SiteEmpresa::when($idCategoria > 0, function ($q)  use ($idCategoria) {
            return $q->where('idcategoria', '=', $idCategoria);
        })->where('ativo', '=', 's')
        ->orderBy('nome')
        ->get();

        foreach ($parameters->adds->list as $key => $value) {
           $value->url = '/parceiro/'.$value->idempresa.'/'.Generate::url_generate($value->nome);
        }

        $parameters->adds->idcategoria = $idCategoria;

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);

        self::pageDefault($parameters);
    }
    
    public static function post($id = 0){
        SessionControl::start_session();
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Parcerias.post';
        $parameters->pagelink = 'home';

        $parameters->adds->parceiro = SiteEmpresa::find($id);
        $parameters->adds->user     = new AccessUser;

        if(isset($_SESSION['user_id'])){
            $parameters->adds->user = AccessUser::find($_SESSION['user_id']);
        }

        $pathbreadcrumb = [
            [
                'label'  => 'Parcerias',
                'link'   => '/parcerias',
                'active' => false
            ]
        ];

        $categoria   = SiteCategory::where('idlanguage', '=', 1)
        ->where('idcategory', '=', $parameters->adds->parceiro->idcategoria)
        ->first();

        $pathbreadcrumb[] = [
            'label'  => $categoria->title,
            'link'   => '/parcerias/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
            'active' => false
        ];

        $pathbreadcrumb[] = [
            'label'  => $parameters->adds->parceiro->nome,
            'link'   => '',
            'active' => true
        ];

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);

        $parameters->adds->parceiro->arrayImages    = json_decode($parameters->adds->parceiro->imagens);

        self::pageDefault($parameters);
    }
}
