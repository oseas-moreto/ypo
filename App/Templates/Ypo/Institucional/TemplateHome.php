<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Models\Entities\SiteBanner;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteClassificado;
use App\Models\Entities\SiteDica;
use App\Models\Entities\SiteEmpresa;
use App\Models\Entities\SiteRetreat;
use App\Models\Entities\SiteVideoteca;
use App\Models\Entities\SiteDestaque;
use App\Models\Entities\SiteInstitucional;
use App\Classes\Funcoes\Generate;
use \App\Classes\Funcoes\SessionControl;
use App\Classes\Funcoes\DateManipulation;
use App\Models\Entities\AccessSpouse;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessDiretoria;
use stdClass;

class TemplateHome extends Template{
    
    public static function index(){
        $parameters = new stdClass;
        $parameters->adds = new stdClass;
        $parameters->title = "YPO Brasil";
        $parameters->class = 'usuarios';
        $parameters->page = 'layout';
        $parameters->view = 'Institucional.Home.index';
        $parameters->pagelink = 'home';

        $parameters->adds->banners = SiteBanner::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('hash', '=', 'banner-home')
        ->orderBy('order', 'ASC')
        ->get();

        $parameters->adds->firstBanner = $parameters->adds->banners[0];
        unset($parameters->adds->banners[0]);

        $parameters->adds->categoriasParceiros = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'parceiros')
        ->orderBy('order', 'ASC')
        ->get();

        foreach ($parameters->adds->categoriasParceiros as $key => $value) {
           $value->url = '/parcerias/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        $parameters->adds->categoriasClassificados = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'classificados')
        ->orderBy('order', 'ASC')
        ->limit(3)
        ->get();

        foreach ($parameters->adds->categoriasClassificados as $key => $value) {
           $value->url = '/classificados/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        $parameters->adds->categoriasRetreats = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'retreats')
        ->orderBy('order', 'ASC')
        ->get();

        foreach ($parameters->adds->categoriasRetreats as $key => $value) {
           $value->url = '/listagem-retreats/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        $parameters->adds->categoriasDicas = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'dicas')
        ->orderBy('order', 'ASC')
        ->get();

        foreach ($parameters->adds->categoriasDicas as $key => $value) {
           $value->url = '/listagem-dicas/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        $parameters->adds->cidadesDicas = SiteDica::where('ativo', '=', 's')
        ->select('cidade')
        ->groupBy('cidade')
        ->get();

        $parameters->adds->destaque = SiteDestaque::where('ativo', '=', 's')
        ->orderBy('ordem', 'ASC')
        ->get();

      foreach ($parameters->adds->destaque as $key => $value) {
         $mes = DateManipulation::returnmonthabr(date('m', strtotime($value->data)));
         $value->dataFormatada = date('d', strtotime($value->data)).' '.$mes.' '.date('Y', strtotime($value->data));
      }
      
      $parameters->adds->capitulos = AccessUser::where('idgroup', '=', '4')->where('capitulo', '!=', '')->where('status', '!=', 'd')->select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
      $parameters->adds->ramoatvs  = AccessUser::where('idgroup', '=', '4')->where('ramoatv', '!=', '')->where('status', '!=', 'd')->select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

      $parameters->adds->capitulosContato = AccessUser::where('idgroup', '=', '4')->where('capitulo', '!=', '')->where('status', '!=', 'd')->select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
      $parameters->adds->ramoatvsContato  = AccessUser::where('idgroup', '=', '4')->where('ramoatv', '!=', '')->where('status', '!=', 'd')->select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

      $parameters->adds->capitulosDiretor = AccessDiretoria::where('capitulo', '!=', '')->select('capitulo')->groupBy('capitulo')->orderBy('ordemCapitulo')->get();
      $parameters->adds->ramoatvsDiretor  = AccessDiretoria::join('access_user', 'diretoria.iduser', '=', 'access_user.iduser')
      ->where('ramoatv', '!=', '')->select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

      self::pageDefault($parameters);
    }
    
    public static function buscar($busca){
      $parameters           = new stdClass;
      $parameters->adds     = new stdClass;
      $parameters->title    = "YPO Brasil";
      $parameters->class    = 'usuarios';
      $parameters->page     = 'layout';
      $parameters->view     = 'Institucional.Home.buscar';
      $parameters->pagelink = 'home';

      $parameters->adds->busca = $busca;

      $classificados = SiteClassificado::where('ativo', '=', 's')
      ->whereRaw(self::getSqlBusca($busca, 'nome'))
      ->orderBy('nome')
      ->get();

      $parameters->adds->classificados = '';

      foreach ($classificados as $key => $value) {
         $value->url = '/classificado/'.$value->idclassificado.'/'.Generate::url_generate($value->nome);
         $parameters->adds->classificados .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->nome.'</a></div>';
      }

      $retreats = SiteRetreat::where('ativo', '=', 's')
      ->whereRaw(self::getSqlBusca($busca, 'nome'))
      ->orderBy('nome')
      ->get();

      $parameters->adds->retreats = '';

      foreach ($retreats as $key => $value) {
         $value->url = '/retreat/'.$value->idretreat.'/'.Generate::url_generate($value->nome);
         $parameters->adds->retreats .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->nome.'</a></div>';
      }

      $dicas = SiteDica::join('site_category as b', 'dicas.idcategoria2', '=', 'b.idcategory')
      ->where('ativo', '=', 's')
      ->whereRaw(self::getSqlBusca($busca, 'nome'))
      ->select('dicas.*', 'b.title as subcategoria')
      ->orderBy('nome')
      ->get();

      $parameters->adds->dicas = '';

      foreach ($dicas as $key => $value) {
         $value->url = '/listagem-dicas/'.$value->idcategoria2.'/'.Generate::url_generate($value->subcategoria).'#dica'.$value->iddica;
         $parameters->adds->dicas .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->nome.'</a></div>';
      }

      $parceiros = SiteEmpresa::where('ativo', '=', 's')
      ->whereRaw(self::getSqlBusca($busca, 'nome'))
      ->orderBy('nome')
      ->get();
      
      $parameters->adds->parceiros = '';

      foreach ($parceiros as $key => $value) {
         $value->url = '/parceiro/'.$value->idempresa.'/'.Generate::url_generate($value->nome);
         $parameters->adds->parceiros .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->nome.'</a></div>';
      }

      $videoteca = SiteVideoteca::where('ativo', '=', 's')
      ->whereRaw(self::getSqlBusca($busca, 'titulo'))
      ->orderBy('titulo')
      ->get();

      $parameters->adds->videoteca = '';

      foreach ($videoteca as $key => $value) {
         $value->url = '/videoteca/video/'.$value->idvideoteca.'/'.Generate::url_generate($value->titulo);
         $parameters->adds->videoteca .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->titulo.'</a></div>';
      }

      self::getBuscaContato($parameters);
      self::getBuscaDiretoria($parameters);
      self::getBuscaSpouse($parameters);
      self::getBuscaCategoriaDica($parameters);

      self::pageDefault($parameters);
   }

   public static function institucional($hash){
       SessionControl::start_session();

       $parameters = new stdClass;
       $parameters->adds = new stdClass;
       $parameters->adds->institucional = SiteInstitucional::where('hash', '=', $hash)
       ->where('idlanguage', '=', 1)->first();
       $parameters->title = $parameters->adds->institucional->title;

       $parameters->page = 'layout';
       $parameters->view = 'Institucional.Home.page';
       $parameters->pagelink = 'home';

       self::pageDefault($parameters);
   }

   public static function getSqlBusca($parametro, $campo){
      $arrayBusca = explode(' ', $parametro);
      $sqlArray = [];

      foreach ($arrayBusca as $key => $nome) {
         $sqlArray[] = ' '.$campo.' LIKE "%'.$nome.'%" ';
      }

      return '('.implode(' OR ', $sqlArray).')';
   }

   public static function getBuscaContato(&$parameters){
      $contatos = AccessUser::where('idgroup', '=', '4')->where('status', '=', 'a')
      ->whereRaw(self::getSqlBusca($parameters->adds->busca, 'name'))
      ->orderBy('name')
      ->get();
      
      $parameters->adds->contatos = '';

      foreach ($contatos as $key => $value) {
         $value->url = '/contato?nome='.$value->name;
         $parameters->adds->contatos .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->name.'</a></div>';
      }
   }

   public static function getBuscaDiretoria(&$parameters){
      $diretoria = AccessDiretoria::whereRaw(self::getSqlBusca($parameters->adds->busca, 'fantasia'))
      ->orderBy('fantasia')
      ->get();
      
      $parameters->adds->diretoria = '';

      foreach ($diretoria as $key => $value) {
         $value->url = '/diretoria?nome='.$value->fantasia;
         $parameters->adds->diretoria .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->fantasia.'</a></div>';
      }
   }

   public static function getBuscaSpouse(&$parameters){
      $spouse = AccessSpouse::whereRaw(self::getSqlBusca($parameters->adds->busca, 'nome'))
      ->orderBy('nome')
      ->get();
      
      $parameters->adds->spouse = '';

      foreach ($spouse as $key => $value) {
         $value->url = '/diretoria?nome='.$value->nome;
         $parameters->adds->spouse .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->nome.'</a></div>';
      }
   }

   public static function getBuscaCategoriaDica(&$parameters){
      $parameters->adds->dicasCategoria = SiteCategory::where('idlanguage', '=', 1)
      ->where('status', '=', 'a')
      ->where('type', '=', 'dicas')
      ->where('title', 'LIKE', '%'.$parameters->adds->busca.'%')
      ->orderBy('order', 'ASC')
      ->get();
      
      $parameters->adds->categoriasDicas = '';

      foreach ($parameters->adds->dicasCategoria as $key => $value) {
         $value->url = '/listagem-dicas/'.$value->idcategory.'/'.Generate::url_generate($value->title);
         $parameters->adds->categoriasDicas .= '<div class="col-lg-3"><a href="'.$value->url.'">'.$value->title.'</a></div>';
      }
   }
}
