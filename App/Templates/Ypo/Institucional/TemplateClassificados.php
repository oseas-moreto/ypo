<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteClassificado;
use App\Classes\Funcoes\Generate;
use App\Templates\Ypo\Modulos\TemplateLoad;
use stdClass;

class TemplateClassificados extends Template{
    
    public static function index($idCategoria = 0, $text = '', $page = 1){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Classificados.index';
        $parameters->pagelink = 'home';

        $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'classificados')
        ->orderBy('order', 'ASC')
        ->get();

        $pathbreadcrumb = [
            [
                'label'  => 'Classificados',
                'link'   => '/classificados',
                'active' => !($idCategoria > 0)
            ]
        ];

        $parameters->adds->mostrarSubCategoria = false;

        if($idCategoria > 0){
            $categoria   = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $idCategoria)
            ->first();

            if($categoria->nivel > 0) {
                $nivel   = SiteCategory::where('idlanguage', '=', 1)
                ->where('idcategory', '=', $categoria->nivel)
                ->first();
    
                $pathbreadcrumb[] = [
                    'label'  => $nivel->title,
                    'link'   => '/classificados/'.$nivel->idcategory.'/'.Generate::url_generate($nivel->title),
                    'active' => false
                ];
            }
    
            $pathbreadcrumb[] = [
                'label'  => $categoria->title,
                'link'   => '/classificados/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
                'active' => empty($_GET)
            ];

            $subscategoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('nivel', '=', $categoria->idcategory)
            ->orderBy('title')
            ->get();

            if(count($subscategoria) > 0){
                $parameters->adds->mostrarSubCategoria = true;
                $parameters->adds->subscategoria = $subscategoria;
                foreach ($parameters->adds->subscategoria as $key => $value) {
                    $value->url = '/classificados/'.$value->idcategory.'/'.Generate::url_generate($value->title);
                 }
            }
        }

        if(!empty($_GET)){
            $pathbreadcrumb[] = [
                'label'  => 'Busca',
                'link'   => '',
                'active' => true
            ];
        }

        $parameters->adds->cidades = SiteClassificado::getCidades();

        foreach ($parameters->adds->categorias as $key => $value) {
           $value->url = '/classificados/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }
        
        $_GET['categoria'] = $idCategoria;
        if(!isset($_GET['page'])) $_GET['page'] = $page;

        $listagem = SiteClassificado::getClassificados($_GET);

        $parameters->adds->maxValue = ceil(SiteClassificado::max('valor'));
        
        self::pagination($listagem, $_GET, $parameters);

        
        //LIMITA O OBJETO
        $listagem = $listagem->limit($parameters->adds->limit)->offset($parameters->adds->offset)->get();

        foreach ($listagem as $key => $value) {
            $value->url = '/classificado/'.$value->idclassificado.'/'.Generate::url_generate($value->nome);
        }

        $parameters->adds->listagem = $listagem;

        $parameters->adds->urlPagination = self::getUrlPagination($_GET, $text);

        $parameters->adds->checkTipo = ['aluguel' => '', 'venda' => ''];

        $parameters->adds->valor = ['min' => 0, 'max' => $parameters->adds->maxValue];

        if(isset($_GET['tipo'])){
            foreach ($_GET['tipo'] as $key => $value) {
                if($value == 'aluguel'){
                    $parameters->adds->checkTipo['aluguel'] = 'checked';
                    continue;
                }
                if($value == 'venda'){
                    $parameters->adds->checkTipo['venda'] = 'checked';
                    continue;
                }
            }
        }

        if(isset($_GET['cidade'])){
            foreach ($parameters->adds->cidades as $key => $value) {
                $value->check = \in_array($value->cidade, $_GET['cidade']) ? 'checked' : '';
            }
        }

        if(isset($_GET['min']) && isset($_GET['max'])){
            $parameters->adds->valor = ['min' => $_GET['min'], 'max' => $_GET['max']];
        }

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);
        
        self::pageDefault($parameters);
    }



    public static function pagination($listagem, $requestURL, $parameters){
        //paginacao
        $obListagem = clone $listagem;
        $total = $obListagem->count();
        //seta a quantidade de itens por página
        $registros =  10;
        //calcula o número de páginas arredondando o resultado para cima
        $numPaginas = ceil($total/$registros);

        $parameters->adds->limit = $registros;
        $parameters->adds->offset = ($registros * $requestURL['page']) - $registros;
        $parameters->adds->numPaginas    = $numPaginas;
        $parameters->adds->paginacao     = $requestURL['page'];
        $parameters->adds->max_links     = 10;
        $parameters->adds->total         = $total;
    }

    public static function getUrlPagination($requestURL, $url){
        switch (true) {
            case $requestURL['categoria'] > 0:
                $url = 'classificados/'.$requestURL['categoria'].'/'.$url;
            break;
            default:
               $url = 'classificados';
            break;
        }

        unset($requestURL['categoria']);
        $requestURL['page'] = '{page}';

        $url .= '?'.http_build_query($requestURL);
        return $url;
    }
    
    public static function post($id = 0){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Classificados.post';
        $parameters->pagelink = 'home';

        $parameters->adds->classificado = SiteClassificado::find($id);

        $parameters->adds->classificado->arrayImages    = json_decode($parameters->adds->classificado->imagens);

        $pathbreadcrumb = [
            [
                'label'  => 'Classificados',
                'link'   => '/classificados',
                'active' => false
            ]
        ];

        $categoria   = SiteCategory::where('idlanguage', '=', 1)
        ->where('idcategory', '=', $parameters->adds->classificado->idcategoria)
        ->first();

        $pathbreadcrumb[] = [
            'label'  => $categoria->title,
            'link'   => '/classificados/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
            'active' => false
        ];

        if($parameters->adds->classificado->idcategoria2 <> 0 && $parameters->adds->classificado->idcategoria <> ''){
            $categoria   = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $parameters->adds->classificado->idcategoria2)
            ->first();
    
            $pathbreadcrumb[] = [
                'label'  => $categoria->title,
                'link'   => '/classificados/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
                'active' => false
            ];

        }

        $pathbreadcrumb[] = [
            'label'  => $parameters->adds->classificado->nome,
            'link'   => '',
            'active' => true
        ];

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);

        self::pageDefault($parameters);
    }
}
