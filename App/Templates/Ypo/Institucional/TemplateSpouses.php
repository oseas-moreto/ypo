<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Classes\Funcoes\Generate;
use \App\Classes\Funcoes\SessionControl;
use App\Classes\Funcoes\DateManipulation;
use App\Models\Entities\AccessSpouse;
use stdClass;

class TemplateSpouses extends Template{
    
   public static function index(){
      $parameters = new stdClass;
      $parameters->adds = new stdClass;
      $parameters->title = "YPO Brasil";
      $parameters->class = 'usuarios';
      $parameters->page = 'layout';
      $parameters->view = 'Institucional.Spouses.index';
      $parameters->pagelink = 'home';

      $parameters->adds            = new stdClass;
      $parameters->adds->capitulos = AccessSpouse::where('capitulo', '!=', '')->where('parentesco', 'conjuge')->select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
      $parameters->adds->ramoatvs  = AccessSpouse::where('ramoatv', '!=', '')->where('parentesco', 'conjuge')->select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

      $parameters->adds->spouses = AccessSpouse::where('parentesco', 'conjuge');

      if(isset($_GET['nome']) && strlen($_GET['nome'])){
         $sql = [];
         $sqlEmpresa = [];
         $explodeNome = explode(' ', $_GET['nome']);
         foreach ($explodeNome as $key => $nome) {
            $sql[] = ' nome LIKE "%'.$nome.'%" ';
            $sqlEmpresa[] = ' empresa LIKE "%'.$nome.'%" ';
         }

         $sql = array_merge($sql, $sqlEmpresa);
         
         $textSql = '('.implode(' OR ', $sql).')';

         $parameters->adds->spouses->whereRaw($textSql);
      }

      if(isset($_GET['capitulo']) && strlen($_GET['capitulo'])){
         $parameters->adds->spouses->where('capitulo', $_GET['capitulo']);
      }

      if(isset($_GET['ramoatividade']) && strlen($_GET['ramoatividade'])){
         $parameters->adds->spouses->where('ramoatv', $_GET['ramoatividade']);
      }

      $order = 'nome';
      if(isset($_GET['classificar']) && strlen($_GET['classificar'])){
         switch ($_GET['classificar']) {
            case 'nome':
               $order = 'nome';
            break;
            case 'ramoatividade':
               $order = 'ramoatv';
            break;
            case 'capitulo':
               $order = 'capitulo';
            break;
            default:
               $order = 'nome';
            break;
         }
      }

      $parameters->adds->spouses->orderBy($order, 'ASC');

      if(!isset($_GET['mostrar'])) $_GET['mostrar'] = 'todos';
      if(!isset($_GET['page'])) $_GET['page'] = 1;

        
      self::pagination($_GET, $parameters);


      //LIMITA O OBJETO
      $parameters->adds->spouses = $parameters->adds->spouses
      ->limit($parameters->adds->limit)->offset($parameters->adds->offset)->get();

      foreach ($parameters->adds->spouses as $key => $spouse) {
          $select = AccessSpouse::where('iduser', $spouse->iduser)->where('parentesco', 'filho')
         ->selectRaw('group_concat(nome) as filhos')->groupBy('iduser')->first();

         $spouse->filhos = isset($select->filhos) ? $select->filhos : '';
      }

      $parameters->adds->urlPagination = self::getUrlPagination($_GET);

      self::pageDefault($parameters);
   }



   public static function pagination($requestURL, $parameters){
       //paginacao
       $obListagem = clone $parameters->adds->spouses;
       $total = $obListagem->count();
       //seta a quantidade de itens por página
       $registros =  (isset($requestURL['mostrar']) && $requestURL['mostrar'] != 'todos') ? $requestURL['mostrar'] : 190;
       //calcula o número de páginas arredondando o resultado para cima
       $numPaginas = ceil($total/$registros);

       $parameters->adds->limit = $registros;
       $parameters->adds->offset = ($registros * $requestURL['page']) - $registros;
       $parameters->adds->numPaginas    = $numPaginas;
       $parameters->adds->paginacao     = $requestURL['page'];
       $parameters->adds->max_links     = 10;
       $parameters->adds->total         = $total;
   }

   public static function getUrlPagination($requestURL){
      $url = 'spouses';

       $requestURL['page'] = '{page}';

       $url .= '?'.http_build_query($requestURL).'#listagem';
       return $url;
   }
}
