<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Classes\Funcoes\Generate;
use \App\Classes\Funcoes\SessionControl;
use App\Classes\Funcoes\DateManipulation;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessSpouse;
use stdClass;

class TemplateContato extends Template{

   public static function index2(){
      return self::index();
   }
    
   public static function index(){
      $parameters = new stdClass;
      $parameters->adds = new stdClass;
      $parameters->title = "YPO Brasil";
      $parameters->class = 'usuarios';
      $parameters->page = 'layout';
      $parameters->view = 'Institucional.Contato.index2';
      $parameters->pagelink = 'home';

      $parameters->adds            = new stdClass;
      $parameters->adds->capitulos = AccessUser::where('idgroup', '=', '4')->where('capitulo', '!=', '')->where('status', '!=', 'd')->select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
      $parameters->adds->ramoatvs  = AccessUser::where('idgroup', '=', '4')->where('ramoatv', '!=', '')->where('status', '!=', 'd')->select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

      $parameters->adds->contatos = AccessUser::join('spouse', 'access_user.iduser', '=', 'spouse.iduser', 'left outer')
      ->where('access_user.idgroup', '=', '4')->where('access_user.status', '=', 'a');

      if(isset($_GET['nome']) && strlen($_GET['nome'])){
         $sql = [];
         $sqlEmpresa = [];
         $explodeNome = explode(' ', $_GET['nome']);
         foreach ($explodeNome as $key => $nome) {
            $sql[] = ' (access_user.name LIKE "%'.$nome.'%" or spouse.nome LIKE "%'.$nome.'%")';
            $sqlEmpresa[] = ' (access_user.empresa LIKE "%'.$nome.'%" or spouse.empresa LIKE "%'.$nome.'%")';
         }

         $sql = array_merge($sql, $sqlEmpresa);
         
         $textSql = '('.implode(' OR ', $sql).')';

         $parameters->adds->contatos->whereRaw($textSql);
      }

      if(isset($_GET['capitulo']) && strlen($_GET['capitulo'])){
         $parameters->adds->contatos->where('access_user.capitulo', $_GET['capitulo']);
      }

      if(isset($_GET['ramoatividade']) && strlen($_GET['ramoatividade'])){
         $ramo = " (access_user.`ramoatv` = '". $_GET['ramoatividade']."' OR spouse.`ramoatv` = '". $_GET['ramoatividade']."') ";
         $parameters->adds->contatos->whereRaw($ramo);
      }

      $order = 'access_user.name';
      if(isset($_GET['classificar']) && strlen($_GET['classificar'])){
         switch ($_GET['classificar']) {
            case 'nome':
               $order = 'access_user.name';
            break;
            case 'ramoatividade':
               $order = 'access_user.ramoatv';
            break;
            case 'capitulo':
               $order = 'access_user.capitulo';
            break;
            default:
               $order = 'access_user.name';
            break;
         }
      }

      $parameters->adds->contatos->orderBy($order, 'ASC');

      if(!isset($_GET['mostrar'])) $_GET['mostrar'] = 'todos';
      if(!isset($_GET['page'])) $_GET['page'] = 1;

        
      self::pagination($_GET, $parameters);

      //LIMITA O OBJETO
      $parameters->adds->contatos = $parameters->adds->contatos->select("access_user.*")
      ->limit(190)->offset($parameters->adds->offset)->get();

      foreach ($parameters->adds->contatos as $key => $spouse) {
         $spouse->filhos = AccessSpouse::where('iduser', $spouse->iduser)->where('parentesco', 'filho')->get();
         
         if(count($spouse->filhos) > 0){
            foreach ($spouse->filhos as $key => $filho) {
               $filho->idade = strlen($filho->nascimento) ? DateManipulation::idade($filho->nascimento).' anos' : '';
            }
         }

         $conjuge         = AccessSpouse::where('iduser', $spouse->iduser)->where('parentesco', 'conjuge')->first();
         $spouse->conjuge = $conjuge instanceof AccessSpouse ? $conjuge : null;
         $spouse->layout  = $conjuge instanceof AccessSpouse ? 'dual' : 'single';
      }

      $parameters->adds->urlPagination = self::getUrlPagination($_GET);

      self::pageDefault($parameters);
   }
    
   public static function indexBkp(){
      $parameters = new stdClass;
      $parameters->adds = new stdClass;
      $parameters->title = "YPO Brasil";
      $parameters->class = 'usuarios';
      $parameters->page = 'layout';
      $parameters->view = 'Institucional.Contato.index';
      $parameters->pagelink = 'home';

      $parameters->adds            = new stdClass;
      $parameters->adds->capitulos = AccessUser::where('idgroup', '=', '4')->where('capitulo', '!=', '')->where('status', '!=', 'd')->select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
      $parameters->adds->ramoatvs  = AccessUser::where('idgroup', '=', '4')->where('ramoatv', '!=', '')->where('status', '!=', 'd')->select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

      $parameters->adds->contatos = AccessUser::where('idgroup', '=', '4')->where('status', '=', 'a');

      if(isset($_GET['nome']) && strlen($_GET['nome'])){
         $sql = [];
         $sqlEmpresa = [];
         $explodeNome = explode(' ', $_GET['nome']);
         foreach ($explodeNome as $key => $nome) {
            $sql[] = ' name LIKE "%'.$nome.'%" ';
            $sqlEmpresa[] = ' empresa LIKE "%'.$nome.'%" ';
         }

         $sql = array_merge($sql, $sqlEmpresa);
         
         $textSql = '('.implode(' OR ', $sql).')';

         $parameters->adds->contatos->whereRaw($textSql);
      }

      if(isset($_GET['capitulo']) && strlen($_GET['capitulo'])){
         $parameters->adds->contatos->where('capitulo', $_GET['capitulo']);
      }

      if(isset($_GET['ramoatividade']) && strlen($_GET['ramoatividade'])){
         $parameters->adds->contatos->where('ramoatv', $_GET['ramoatividade']);
      }

      $order = 'name';
      if(isset($_GET['classificar']) && strlen($_GET['classificar'])){
         switch ($_GET['classificar']) {
            case 'nome':
               $order = 'name';
            break;
            case 'ramoatividade':
               $order = 'ramoatv';
            break;
            case 'capitulo':
               $order = 'capitulo';
            break;
            default:
               $order = 'name';
            break;
         }
      }

      $parameters->adds->contatos->orderBy($order, 'ASC');

      if(!isset($_GET['mostrar'])) $_GET['mostrar'] = 'todos';
      if(!isset($_GET['page'])) $_GET['page'] = 1;

        
      self::pagination($_GET, $parameters);

      //LIMITA O OBJETO
      $parameters->adds->contatos = $parameters->adds->contatos
      ->limit($parameters->adds->limit)->offset($parameters->adds->offset)->get();

      foreach ($parameters->adds->contatos as $key => $spouse) {
         $filhos = AccessSpouse::where('iduser', $spouse->iduser)->where('parentesco', 'filho')
         ->selectRaw('group_concat(nome) as filhos')->groupBy('iduser')->first();
         $spouse->filhos = isset($filhos->filhos) ? $filhos->filhos : '';

         $conjuge = AccessSpouse::where('iduser', $spouse->iduser)->where('parentesco', 'conjuge')
         ->select('nome')->first();
         $spouse->conjuge = isset($conjuge->nome) ? $conjuge->nome : '';
      }

      $parameters->adds->urlPagination = self::getUrlPagination($_GET);

      self::pageDefault($parameters);
   }

   public static function pagination($requestURL, $parameters){
       //paginacao
       $obListagem = clone $parameters->adds->contatos;
       $total = $obListagem->count();
       //seta a quantidade de itens por página
       $registros =  (isset($requestURL['mostrar']) && $requestURL['mostrar'] != 'todos') ? $requestURL['mostrar'] : 190;
       //calcula o número de páginas arredondando o resultado para cima
       $numPaginas = ceil($total/$registros);

       $parameters->adds->limit = $registros;
       $parameters->adds->offset = ($registros * $requestURL['page']) - $registros;
       $parameters->adds->numPaginas    = $numPaginas;
       $parameters->adds->paginacao     = $requestURL['page'];
       $parameters->adds->max_links     = 10;
       $parameters->adds->total         = $total;
   }

   public static function getUrlPagination($requestURL, $prefix = ''){
      $url = 'contato'.$prefix;

       $requestURL['page'] = '{page}';

       $url .= '?'.http_build_query($requestURL).'#listagem';
       return $url;
   }
}
