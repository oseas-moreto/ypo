<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use stdClass;

class TemplateTurismo extends Template{
    
    public static function index(){
        $parameters = new stdClass;
        $parameters->title = "YPO Brasil";
        $parameters->class = 'usuarios';
        $parameters->page = 'layout-turismo';
        $parameters->view = 'Institucional.Turismo.index';
        $parameters->pagelink = 'home';

        self::pageDefault($parameters);
    }
}
