<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteRetreat;
use App\Models\Entities\SiteRetreatFile;
use App\Classes\Funcoes\Generate;
use App\Templates\Ypo\Modulos\TemplateLoad;
use stdClass;

class TemplateRetreats extends Template{
    
    public static function index($idCategoria = 0){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Retreats.index';
        $parameters->pagelink = 'home';

        $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'retreats')
        ->orderBy('order', 'ASC')
        ->get();

        if($idCategoria == 0){
            $idCategoria = $parameters->adds->categorias[0]->idcategory;
        }

        $_GET['categoria'] = $idCategoria;
        
        $parameters->adds->idcategoria = $idCategoria;

        $pathbreadcrumb = [
            [
                'label'  => 'Retreats',
                'link'   => '/listagem-retreats',
                'active' => !($idCategoria > 0)
            ]
        ];

        $parameters->adds->mostrarSubCategoria = false;

        $categoria = new SiteCategory;
        $categoria->listagem = 'n';

        if($idCategoria > 0){
            $categoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $idCategoria)
            ->first();

            if($categoria->nivel > 0) {
                $nivel   = SiteCategory::where('idlanguage', '=', 1)
                ->where('idcategory', '=', $categoria->nivel)
                ->first();
    
                $pathbreadcrumb[] = [
                    'label'  => $nivel->title,
                    'link'   => '/listagem-retreats/'.$nivel->idcategory.'/'.Generate::url_generate($nivel->title),
                    'active' => false
                ];
            }
    
            $pathbreadcrumb[] = [
                'label'  => $categoria->title,
                'link'   => '/listagem-retreats/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
                'active' => true
            ];

            $subscategoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('nivel', '=', $categoria->idcategory)
            ->orderBy('title')
            ->get();

            if(count($subscategoria) > 0){
                $parameters->adds->mostrarSubCategoria = true;
                $parameters->adds->subscategoria = $subscategoria;
                foreach ($parameters->adds->subscategoria as $key => $value) {
                    $value->url = '/listagem-retreats/'.$value->idcategory.'/'.Generate::url_generate($value->title);
                 }
            }

            if(count($subscategoria) == 0 && $categoria->nivel <> 0){
                $subscategoria = SiteCategory::where('idlanguage', '=', 1)
                ->where('nivel', '=', $categoria->nivel)
                ->orderBy('title')
                ->get();
                $parameters->adds->mostrarSubCategoria = true;
                $parameters->adds->subscategoria = $subscategoria;
                foreach ($parameters->adds->subscategoria as $key => $value) {
                    $value->url = '/listagem-retreats/'.$value->idcategory.'/'.Generate::url_generate($value->title);
                }
            }
        }

        foreach ($parameters->adds->categorias as $key => $value) {
           $value->url = '/listagem-retreats/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        
        $parameters->adds->list = SiteRetreat::when($idCategoria > 0, function ($q)  use ($idCategoria) {
            return $q->whereRaw('( idcategoria = '.$idCategoria.' OR idcategoria2 = '.$idCategoria.' )');
        })
        ->where('ativo', '=', 's')
        ->orderBy('nome', 'ASC')
        ->get();

        foreach ($parameters->adds->list as $key => $value) {
           $value->url = '/retreat/'.$value->idretreat.'/'.Generate::url_generate($value->nome);
        }

        if($categoria->listagem == 's'){
            $parameters->view = 'Institucional.Retreats.newIndex';
            $parameters->adds->list = SiteRetreatFile::when($idCategoria > 0, function ($q)  use ($idCategoria) {
                return $q->whereRaw('( idcategoria = '.$idCategoria.' OR idcategoria2 = '.$idCategoria.' )');
            })
            ->orderBy('nome', 'ASC')
            ->get();
        }

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);

        self::pageDefault($parameters);
    }
    
    public static function newIndex(){
       $parameters = new stdClass;
       $parameters->adds = new stdClass;
       $parameters->title = "YPO Brasil";
       $parameters->class = 'usuarios';
       $parameters->page = 'layout';
       $parameters->view = 'Institucional.Retreats.newIndex';
       $parameters->pagelink = 'home';
       $parameters->list = [];

       $idCategoria = 0;

       $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'retreats')
        ->orderBy('order', 'ASC')
        ->get();

        if($idCategoria == 0){
            $idCategoria = $parameters->adds->categorias[0]->idcategory;
        }

        $_GET['categoria'] = $idCategoria;
        
        $parameters->adds->idcategoria = $idCategoria;

        $pathbreadcrumb = [
            [
                'label'  => 'Retreats',
                'link'   => '/listagem-retreats',
                'active' => !($idCategoria > 0)
            ]
        ];

        $parameters->adds->mostrarSubCategoria = false;

        $categoria = new SiteCategory;
        $categoria->listagem = 'n';

        if($idCategoria > 0){
            $categoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $idCategoria)
            ->first();

            if($categoria->nivel > 0) {
                $nivel   = SiteCategory::where('idlanguage', '=', 1)
                ->where('idcategory', '=', $categoria->nivel)
                ->first();
    
                $pathbreadcrumb[] = [
                    'label'  => $nivel->title,
                    'link'   => '/listagem-retreats/'.$nivel->idcategory.'/'.Generate::url_generate($nivel->title),
                    'active' => false
                ];
            }
    
            $pathbreadcrumb[] = [
                'label'  => $categoria->title,
                'link'   => '/listagem-retreats/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
                'active' => true
            ];

            $subscategoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('nivel', '=', $categoria->idcategory)
            ->orderBy('title')
            ->get();

            if(count($subscategoria) > 0){
                $parameters->adds->mostrarSubCategoria = true;
                $parameters->adds->subscategoria = $subscategoria;
                foreach ($parameters->adds->subscategoria as $key => $value) {
                    $value->url = '/listagem-retreats/'.$value->idcategory.'/'.Generate::url_generate($value->title);
                 }
            }

            if(count($subscategoria) == 0 && $categoria->nivel <> 0){
                $subscategoria = SiteCategory::where('idlanguage', '=', 1)
                ->where('nivel', '=', $categoria->nivel)
                ->orderBy('title')
                ->get();
                $parameters->adds->mostrarSubCategoria = true;
                $parameters->adds->subscategoria = $subscategoria;
                foreach ($parameters->adds->subscategoria as $key => $value) {
                    $value->url = '/listagem-retreats/'.$value->idcategory.'/'.Generate::url_generate($value->title);
                }
            }
        }

        foreach ($parameters->adds->categorias as $key => $value) {
           $value->url = '/listagem-retreats/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);
 
       self::pageDefault($parameters);
    }
    
    public static function post($id = 0){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Retreats.post';
        $parameters->pagelink = 'home';

        $parameters->adds->retreat = SiteRetreat::find($id);
        $parameters->adds->retreat->arrayImages = json_decode($parameters->adds->retreat->imagens);

        $pathbreadcrumb = [
            [
                'label'  => 'Retreats',
                'link'   => '/listagem-retreats',
                'active' => false
            ]
        ];

        $categoria   = SiteCategory::where('idlanguage', '=', 1)
        ->where('idcategory', '=', $parameters->adds->retreat->idcategoria)
        ->first();

        $pathbreadcrumb[] = [
            'label'  => $categoria->title,
            'link'   => '/listagem-retreats/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
            'active' => false
        ];

        if($parameters->adds->retreat->idcategoria2 <> 0 && $parameters->adds->retreat->idcategoria <> ''){
            $categoria   = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $parameters->adds->retreat->idcategoria2)
            ->first();
    
            $pathbreadcrumb[] = [
                'label'  => $categoria->title,
                'link'   => '/listagem-retreats/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
                'active' => false
            ];

        }

        $pathbreadcrumb[] = [
            'label'  => $parameters->adds->retreat->nome,
            'link'   => '',
            'active' => true
        ];

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);

        self::pageDefault($parameters);
    }

    public static function loadFiles($caminho, $folder){
        // Sort in descending order
        if (!is_dir($caminho)) return [];
        $files = scandir($caminho,1);
        $newFiles = [];
        for($i=0; $i<=count($files); $i++){
            if($files[$i] == '.' or $files[$i] == '..'){
                unset($files[$i]);
                continue;
            } 
            $newFiles[$i]['file'] = $files[$i];
            $newFiles[$i]['path'] = '/arquivos/'.$folder.'/'.$files[$i];
        }

        return $newFiles;
    }
}
