<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Classes\Funcoes\Generate;
use \App\Classes\Funcoes\SessionControl;
use App\Classes\Funcoes\DateManipulation;
use App\Models\Entities\AccessDiretoria;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessSpouse;
use stdClass;

class TemplateDiretoria extends Template{
    
   public static function index(){
      $parameters           = new stdClass;
      $parameters->adds     = new stdClass;
      $parameters->title    = "YPO Brasil";
      $parameters->class    = 'usuarios';
      $parameters->page     = 'layout';
      $parameters->view     = 'Institucional.Diretoria.index';
      $parameters->pagelink = 'home';
      
      if(!isset($_GET['capitulo'])) $_GET['capitulo'] = '';
      
      $parameters->adds->labelCapitulo = !strlen($_GET['capitulo']) ? 'YPO Brasil' : $_GET['capitulo'];

      $parameters->adds->capitulos = AccessDiretoria::where('capitulo', '!=', '')->select('capitulo')->groupBy('capitulo')->orderBy('ordemCapitulo')->get();
      
      $parametrosBusca = [];
      if(isset($_GET['nome']) && strlen($_GET['nome'])){
         $sql         = [];
         $sqlFantasia = [];
         $sqlName     = [];
         $sqlNome     = [];
         $sqlEmpresa  = [];
         $sqlEmpresaS = [];
         $explodeNome = explode(' ', $_GET['nome']);
         foreach ($explodeNome as $key => $nome) {
            $sqlFantasia[] = ' diretoria.fantasia LIKE "%'.$nome.'%" ';
            $sqlName[]     = ' b.name LIKE "%'.$nome.'%" ';
            $sqlEmpresa[]  = ' b.empresa LIKE "%'.$nome.'%" ';
            $sqlNome[]     = ' c.nome LIKE "%'.$nome.'%" ';
            $sqlEmpresaS[] = ' c.empresa LIKE "%'.$nome.'%" ';
         }

         $sql = array_merge($sqlFantasia, $sqlName);
         $sql = array_merge($sql, $sqlEmpresa);
         $sql = array_merge($sql, $sqlNome);
         $sql = array_merge($sql, $sqlEmpresaS);
         
         $textSql = implode(' OR ', $sql);
         
         $parametrosBusca['nome'] = $textSql;
      }

      if(isset($_GET['capitulo']) && strlen($_GET['capitulo'])){
         $parametrosBusca['capitulo'] = $_GET['capitulo'];
      }

      if(isset($_GET['ramoatividade']) && strlen($_GET['ramoatividade'])){
         $parametrosBusca['ramo'] = 'b.ramoatv = "'.$_GET['ramoatividade'].'" OR c.ramoatv = "'.$_GET['ramoatividade'].'"' ;
      }

      $parameters->adds->diretores = AccessDiretoria::search($parametrosBusca);

      $parameters->adds->diretoresPorCapitulo = [];

      foreach ($parameters->adds->diretores as $key => $diretor) {
         if($diretor->usuario instanceof AccessUser){
            $diretor->registro = $diretor->usuario;
   
            $conjuge = AccessSpouse::where('iduser', $diretor->iduser)->where('parentesco', 'conjuge')
            ->select('nome')->first();
            $diretor->conjuge = isset($conjuge->nome) ? $conjuge->nome : '';
            $diretor->tipoconjuge = "Spouse";
         }

         if($diretor->spouse instanceof AccessSpouse){
            $diretor->registro = $diretor->spouse;
   
            $conjuge = AccessUser::where('iduser', $diretor->registro->iduser)->select('name as nome')->first();
            $diretor->conjuge = isset($conjuge->nome) ? $conjuge->nome : '';
            $diretor->tipoconjuge = "YPOer";
         }

         $filhos = AccessSpouse::where('iduser', $diretor->iduser)->where('parentesco', 'filho')
         ->selectRaw('group_concat(nome SEPARATOR ", ") as filhos')->groupBy('iduser')->first();
         $diretor->filhos = isset($filhos->filhos) ? $filhos->filhos : '';

         $parameters->adds->diretoresPorCapitulo[$diretor->capitulo]['diretores'][] = $diretor;
      }

      self::pageDefault($parameters);
   }
}
