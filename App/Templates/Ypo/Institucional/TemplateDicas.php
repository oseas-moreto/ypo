<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteDica;
use App\Classes\Funcoes\Generate;
use App\Templates\Ypo\Modulos\TemplateLoad;
use stdClass;

class TemplateDicas extends Template{
    
    public static function index($idCategoria = 0){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Dicas.index';
        $parameters->pagelink = 'home';

        $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'dicas')
        ->orderBy('order', 'ASC')
        ->get();

        if($idCategoria == 0){
            $idCategoria = $parameters->adds->categorias[0]->idcategory;
        }

        $pathbreadcrumb = [
            [
                'label'  => 'Dicas',
                'link'   => '/listagem-dicas',
                'active' => !($idCategoria > 0)
            ]
        ];

        $parameters->adds->mostrarSubCategoria = false;

        if($idCategoria > 0){
            $categoria   = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $idCategoria)
            ->first();

            if($categoria->nivel > 0) {
                $nivel   = SiteCategory::where('idlanguage', '=', 1)
                ->where('idcategory', '=', $categoria->nivel)
                ->first();
    
                $pathbreadcrumb[] = [
                    'label'  => $nivel->title,
                    'link'   => '/listagem-dicas/'.$nivel->idcategory.'/'.Generate::url_generate($nivel->title),
                    'active' => false
                ];
            }
    
            $pathbreadcrumb[] = [
                'label'  => $categoria->title,
                'link'   => '/listagem-dicas/'.$categoria->idcategory.'/'.Generate::url_generate($categoria->title),
                'active' => empty($_GET)
            ];

            $subscategoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('nivel', '=', $categoria->idcategory)
            ->orderBy('title')
            ->get();

            if(count($subscategoria) > 0){
                $parameters->adds->mostrarSubCategoria = true;
                $parameters->adds->subscategoria = $subscategoria;
                foreach ($parameters->adds->subscategoria as $key => $value) {
                    $value->url = '/listagem-dicas/'.$value->idcategory.'/'.Generate::url_generate($value->title);
                 }
            }

            if(count($subscategoria) == 0 && $categoria->nivel <> 0){
                $subscategoria = SiteCategory::where('idlanguage', '=', 1)
                ->where('nivel', '=', $categoria->nivel)
                ->orderBy('title')
                ->get();
                $parameters->adds->mostrarSubCategoria = true;
                $parameters->adds->subscategoria = $subscategoria;
                foreach ($parameters->adds->subscategoria as $key => $value) {
                    $value->url = '/listagem-dicas/'.$value->idcategory.'/'.Generate::url_generate($value->title);
                 }
            }
        }

        if(!empty($_GET)){
            $pathbreadcrumb[] = [
                'label'  => 'Busca',
                'link'   => '',
                'active' => true
            ];
        }

        $_GET['categoria'] = $idCategoria;
        
        $parameters->adds->idcategoria = $idCategoria;

        foreach ($parameters->adds->categorias as $key => $value) {
           $value->url = '/listagem-dicas/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }

        $list = SiteDica::join('site_category as a', 'dicas.idcategoria', '=', 'a.idcategory')
        ->join('site_category as b', 'dicas.idcategoria2', '=', 'b.idcategory')
        ->when($idCategoria > 0, function ($q)  use ($idCategoria) {
            return $q->whereRaw('( idcategoria = '.$idCategoria.' OR idcategoria2 = '.$idCategoria.' )');
        })
        ->where('ativo', '=', 's')
        ->select('dicas.*', 'a.title as categoria', 'b.title as subcategoria')
        ;

        if(isset($_GET['pais']) && is_array($_GET['pais']) && !empty($_GET['pais'][0])){
            $list->whereIn('pais', $_GET['pais']);
        }

        if(isset($_GET['estado']) && is_array($_GET['estado']) && !empty($_GET['estado'][0])){
            $list->whereIn('estado', $_GET['estado']);
        }

        if(isset($_GET['cidade']) && is_array($_GET['cidade']) && !empty($_GET['cidade'][0])){
            $list->whereIn('cidade', $_GET['cidade']);
        }

        $pais = $cidade = $estado = [];

        
        $parameters->adds->list = $list->orderBy('subcategoria')->get();
        
        $parameters->adds->categoria = $categoria;

        $parameters->adds->pais = self::getPaises($idCategoria);
        $pais                   = isset($_GET['pais']) ? $_GET['pais'] : [];
        
        $parameters->adds->estado = self::getEstados($pais,$idCategoria);
        $estado                   = isset($_GET['estado']) ? $_GET['estado'] : [];
        
        $parameters->adds->cidades = self::getCidades($pais,$estado,$idCategoria);
        $cidade                    = isset($_GET['cidade']) ? $_GET['cidade'] : [];

        $parameters->adds->breadcrumb = TemplateLoad::breadcrumb($pathbreadcrumb);

        self::pageDefault($parameters);
    }

    public static function getUrl(){
        $url = $_SERVER["REQUEST_URI"];
        $urlArray = explode("?", $url);
        return is_array($urlArray) ? $urlArray[0] : $url;
    }

    public static function getPaises($idCategoria){
        $pais = SiteDica::where('ativo', '=', 's')
        ->when($idCategoria > 0, function ($q)  use ($idCategoria) {
            return $q->whereRaw('( idcategoria = '.$idCategoria.' OR idcategoria2 = '.$idCategoria.' )');
        })
        ->where('pais', '!=', '')
        ->select('pais')->groupBy('pais')
        ->orderBy('pais')
        ->get();

        foreach ($pais as $key => $value) {
            $value->url = self::getUrl().'?pais[]='.$value->pais;
        }

        return $pais;
    }

    public static function getEstados($pais, $idCategoria){
        $estados = (!empty($pais) || $idCategoria == 37) ? SiteDica::where('ativo', '=', 's')
        ->when($idCategoria > 0, function ($q)  use ($idCategoria) {
            return $q->whereRaw('( idcategoria = '.$idCategoria.' OR idcategoria2 = '.$idCategoria.' )');
        })
        ->where('estado', '!=', '')
        ->when(!empty($pais), function ($q)  use ($pais) {
            return $q->whereIn('pais', [$pais]);
        })
        ->select('estado')->groupBy('estado')
        ->orderBy('estado')
        ->get() : [];

        foreach ($estados as $key => $value) {
            $value->url = self::getUrl().'?estado[]='.$value->estado;
        }

        return $estados;
    }

    public static function getCidades($pais, $estado, $idCategoria){
        $cidades = (!empty($pais) || !empty($estado)) ?SiteDica::where('ativo', '=', 's')
        ->when($idCategoria > 0, function ($q)  use ($idCategoria) {
            return $q->whereRaw('( idcategoria = '.$idCategoria.' OR idcategoria2 = '.$idCategoria.' )');
        })
        ->when(!empty($pais), function ($q)  use ($pais) {
            return $q->whereIn('pais', [$pais]);
        })
        ->when(!empty($estado), function ($q)  use ($estado) {
            return $q->whereIn('estado', [$estado]);
        })
        ->where('cidade', '!=', '')
        ->select('cidade')->groupBy('cidade')
        ->orderBy('cidade')
        ->get() : [];

        $urlArray  = [];
        $urlString = '';

        if(isset($_GET['pais'][0])   && strlen($_GET['pais'][0]))   $urlArray[] = 'pais[]='.$_GET['pais'][0];
        if(isset($_GET['estado'][0]) && strlen($_GET['estado'][0])) $urlArray[] = 'estado[]='.$_GET['estado'][0];

        $urlString = implode('&', $urlArray);

        foreach ($cidades as $key => $value) {
            $value->url = self::getUrl().'?'.$urlString.'&cidade[]='.$value->cidade;
        }

        return $cidades;
    }
}
