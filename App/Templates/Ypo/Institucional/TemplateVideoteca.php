<?php
namespace App\Templates\Ypo\Institucional;

use App\Core\Template;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteVideoteca;
use App\Classes\Funcoes\Generate;
use stdClass;

class TemplateVideoteca extends Template{
    
    public static function index($idCategoria = 0, $text = '', $page = 1){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Videotecas.index';
        $parameters->pagelink = 'home';

        $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'videoteca')
        ->orderBy('order', 'ASC')
        ->get();

        foreach ($parameters->adds->categorias as $key => $value) {
           $value->url = '/videoteca/'.$value->idcategory.'/'.Generate::url_generate($value->title);
        }
        
        $_GET['categoria'] = $idCategoria;
        if(!isset($_GET['page'])) $_GET['page'] = $page;

        $listagem = SiteVideoteca::getVideotecas($_GET);
        
        self::pagination($listagem, $_GET, $parameters);

        
        //LIMITA O OBJETO
        $listagem = $listagem->orderBy('data', 'DESC')
        ->limit($parameters->adds->limit)->offset($parameters->adds->offset)->get();

        foreach ($listagem as $key => $value) {
            $value->url = '/videoteca/video/'.$value->idvideoteca.'/'.Generate::url_generate($value->titulo);
        }

        $parameters->adds->listagem = $listagem;

        $parameters->adds->urlPagination = self::getUrlPagination($_GET, $text);
        
        self::pageDefault($parameters);
    }



    public static function pagination($listagem, $requestURL, $parameters){
        //paginacao
        $obListagem = clone $listagem;
        $total = $obListagem->count();
        //seta a quantidade de itens por página
        $registros =  10;
        //calcula o número de páginas arredondando o resultado para cima
        $numPaginas = ceil($total/$registros);

        $parameters->adds->limit = $registros;
        $parameters->adds->offset = ($registros * $requestURL['page']) - $registros;
        $parameters->adds->numPaginas    = $numPaginas;
        $parameters->adds->paginacao     = $requestURL['page'];
        $parameters->adds->max_links     = isset($requestURL['mostrar']) ? $requestURL['mostrar'] : 10;
        $parameters->adds->total         = $total;
    }

    public static function getUrlPagination($requestURL, $url){
        switch (true) {
            case $requestURL['categoria'] > 0:
                $url = 'videoteca/'.$requestURL['categoria'].'/'.$url;
            break;
            default:
               $url = 'videoteca';
            break;
        }

        unset($requestURL['categoria']);
        $requestURL['page'] = '{page}';

        $url .= '?'.http_build_query($requestURL);
        return $url;
    }
    
    public static function post($id = 0){
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "YPO Brasil";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout';
        $parameters->view     = 'Institucional.Videotecas.post';
        $parameters->pagelink = 'home';

        $parameters->adds->videoteca = SiteVideoteca::find($id);

        $parameters->adds->videoteca->categoria = SiteCategory::where('idcategory', '=', $parameters->adds->videoteca->idcategoria)
        ->where('idlanguage', '=', 1)
        ->first();
        
        //LIMITA O OBJETO
        $listagem = SiteVideoteca::where('idcategoria', '=', $parameters->adds->videoteca->idcategoria)
        ->where('idvideoteca', '!=', $parameters->adds->videoteca->idvideoteca)->limit(2)->get();

        foreach ($listagem as $key => $value) {
            $value->url = '/videoteca/video/'.$value->idvideoteca.'/'.Generate::url_generate($value->titulo);
        }

        $parameters->adds->listagem = $listagem;

        self::pageDefault($parameters);
    }
}
