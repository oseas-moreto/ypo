<?php

namespace App\Templates\Ypo\Modulos;

use App\Classes\Funcoes\SessionControl;
use App\Controllers\Ypo\Login;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessMenu;
use App\Models\Entities\Configuration;
use App\Models\Entities\SiteSeo;
use stdClass;

class TemplateSidebar{

    public static function sidebarMenu($itens){
        $menu  = $itens['leveltop'];
        $total = $itens['total']; 
        $html  = '';
        for ($i = 0; $i < $total; $i++) {
            if($menu[$i]['function'] != ""){
                $html  .= self::sidebarItemSimplesTitle($menu[$i]);
                continue;
            }

            $html  .= self::sidebarItemGrupo($menu[$i]);
        }
        
        return $html;
    }

    public static function sidebarItemSimples($item){
        return '<li class="'.$item['focus'].'"><a href="/'.$item['function'].'">'.$item['title'].' </a></li>';
    }

    public static function sidebarItemSimplesTitle($item){
        return '<ul style="padding-bottom: 20px;"><li class="'.$item['focus'].'"><a href="/'.$item['function'].'">'.$item['title'].' </a></li></ul>';
    }

    public static function sidebarItemGrupo($item){
        $menu = '<h2 class="menu-classificados-t1">
                    <i class="nav-icon '.$item['icon'].'"></i> 
                    '.$item['title'].'
                </h2>
                <ul style="padding-bottom: 20px;">';

        $sub = $item['submenus'];
        for($s = 0; $s < $item['total']; $s++) {
            $menu .= self::sidebarItemSimples($sub[$s]);
        }
        $menu .= '</ul>';

        return $menu;
    }
}
