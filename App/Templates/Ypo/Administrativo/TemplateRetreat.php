<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteRetreat;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\AccessUser;
use stdClass;

class TemplateRetreat extends Template
{
    public static function index()
    {
        $parameters       = new stdClass;
        $parameters->adds = new stdClass;

        $parameters->title = "Lista de Retreats";
        $parameters->class = 'usuarios';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Retreat.index';
        $parameters->pagelink = 'retreats';
        
        $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'retreats')
        ->where('listagem', '=', 'n')
        ->orderBy('title', 'ASC')
        ->get();
        
        $idCategoria    = isset($_GET['categoria'])    ? $_GET['categoria']    : 0;
        $idSubCategoria = isset($_GET['subcategoria']) ? $_GET['subcategoria'] : 0;
        $parameters->adds->subscategoria  = [];

        if($idSubCategoria > 0){
            $subcategoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $idSubCategoria)
            ->first();

            $_GET['categoria'] = $subcategoria->nivel;
            $idCategoria       = $subcategoria->nivel;
        }

        if($idCategoria > 0){
            $parameters->adds->subscategoria = SiteCategory::where('idlanguage', '=', 1)
                ->where('nivel', '=', $idCategoria)
                ->orderBy('title')
                ->get();
        }

        
        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->get();

        switch (true) {
            case $_SESSION['idgroup'] <> 1 :
                $parameters->list = SiteRetreat::join('site_category as a', 'retreats.idcategoria', '=', 'a.idcategory')
                ->where('ativo', '<>', 'd')->where('iduser', '=', $_SESSION['user_id']); 
            break;
            case isset($_GET['iduser']) :
                $parameters->list = SiteRetreat::join('site_category as a', 'retreats.idcategoria', '=', 'a.idcategory')
                ->where('ativo', '<>', 'd')
                ->where('iduser', '=', $_GET['iduser']);
            break;
            
            default:
                $parameters->list = SiteRetreat::join('site_category as a', 'retreats.idcategoria', '=', 'a.idcategory')
                ->where('ativo', '<>', 'd');
            break;
        }

        if($idCategoria > 0){ 
            $parameters->list->where('retreats.idcategoria', '=', $idCategoria);
        }

        if($idSubCategoria > 0){
            $parameters->list->where('retreats.idcategoria2', '=', $idSubCategoria);
        }

        $parameters->list = $parameters->list->select('retreats.*', 'a.title as categoria')->get();
        
        if($_SESSION['idgroup'] <> 1){
            $parameters->adds->usuario = $_SESSION['name'];
        }

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteRetreat;
        $obj->ativo = 's';

        $parameters->adds->categorias = [];
        $parameters->adds->categorias = SiteCategory::where('type', '=', 'retreats')
        ->where('nivel','=',0)->where('status', '=', 'a')
        ->where('listagem', '=', 'n')
        ->get();

        $parameters->adds->subcategorias = [];

        $parameters->id = '';
        $parameters->title = "Retreats - Cadastro";
        if ($id != '') {
            $obj = SiteRetreat::find($id);
            $parameters->adds->subcategorias = SiteCategory::where('nivel', '=', $obj->idcategoria)
            ->where('type', '=', 'retreats')->where('status', '=', 'a')->get();

            if(strlen($obj->imagens)){
                $obj->imgArray = \json_decode($obj->imagens, true);
            }

            $parameters->id = $id;
            $parameters->title = "Retreats - Editar";
        }
        $parameters->obj = $obj;

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->get();

        if($_SESSION['idgroup'] <> 1){
            $parameters->adds->iduser = $_SESSION['user_id'];
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Retreat.form';
        $parameters->pagelink = 'retreats';

        self::formSistemaDefault($parameters);
    }
}
