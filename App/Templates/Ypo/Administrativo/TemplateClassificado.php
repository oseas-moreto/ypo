<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteClassificado;
use App\Models\Entities\SiteCategory;
use App\Classes\Funcoes\SessionControl;
use App\Models\Entities\AccessUser;
use stdClass;

class TemplateClassificado extends Template
{
    public static function index()
    {
        SessionControl::start_session();
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "Lista de Classificados";
        $parameters->class    = 'usuarios';
        
        $parameters->page     = 'layout-dashboard';
        $parameters->view     = 'Administrativo.Classificado.index';
        $parameters->pagelink = 'cadastro-classificados';

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '=', 4)->get();

        switch (true) {
            case $_SESSION['idgroup'] <> 1 :
                $parameters->list = SiteClassificado::where('ativo', '<>', 'd')->where('iduser', '=', $_SESSION['user_id'])->get(); 
            break;
            case isset($_GET['iduser']) :
                $parameters->list = SiteClassificado::where('ativo', '<>', 'd')->where('iduser', '=', $_GET['iduser'])->get(); 
            break;
            
            default:
                $parameters->list = SiteClassificado::where('ativo', '<>', 'd')->get();
            break;
        }

        if($_SESSION['idgroup'] <> 1){
            $parameters->adds->usuario = $_SESSION['name'];
        }

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        SessionControl::start_session();
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteClassificado;
        $obj->ativo = 's';
        $obj->botaoZap = 'n';

        $parameters->adds->categorias = [];
        $parameters->adds->categorias = SiteCategory::where('type', '=', 'classificados')
        ->where('nivel','=',0)->where('status', '=', 'a')
        ->get();

        $parameters->adds->subcategorias = [];

        $parameters->id = '';
        $parameters->title = "Classificados - Cadastro";
        if ($id != '') {
            $obj = SiteClassificado::find($id);
            $parameters->adds->subcategorias = SiteCategory::where('nivel', '=', $obj->idcategoria)
            ->where('type', '=', 'classificados')->where('status', '=', 'a')->get();

            if(strlen($obj->imagens)){
                $obj->imgArray = \json_decode($obj->imagens, true);
            }

            $parameters->id = $id;
            $parameters->title = "Classificados - Editar";
        }
        $parameters->obj = $obj;

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '=', 4)->get();

        if($_SESSION['idgroup'] <> 1){
            $parameters->adds->iduser = $_SESSION['user_id'];
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Classificado.form';
        $parameters->pagelink = 'cadastro-classificados';

        self::formSistemaDefault($parameters);
    }
}
