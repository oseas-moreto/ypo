<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use stdClass;

class TemplateLogin extends Template
{
    public static function index(){
        $parameters = new stdClass;
        $parameters->title    = "Login";
        $parameters->class    = 'usuarios';
        $parameters->list     = new stdClass;
        $parameters->siderbar = false;

        $parameters->page     = 'layout';
        $parameters->view     = 'Administrativo.Login.index';
        $parameters->pagelink = '/login';

        self::indexSistemaDefault($parameters);
    }
}
