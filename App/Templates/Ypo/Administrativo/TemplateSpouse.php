<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessSpouse;
use App\Classes\Funcoes\Generate;
use stdClass;

class TemplateSpouse extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;
        $parameters->title = "Lista de Spouses";
        $parameters->class = 'usuarios';
        $parameters->page = 'layout-dashboard';
        $parameters->list = AccessSpouse::get();
        $parameters->view = 'Administrativo.Spouse.index';
        $parameters->pagelink = 'cadastro-spouse';

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->orderBy('name')->get();
        
        switch (true) {
            case (!in_array($_SESSION['idgroup'], [1,3])) :
                $parameters->list = AccessSpouse::where('iduser', '=', $_SESSION['user_id'])->get(); 
            break;
            case isset($_GET['iduser']) :
                $parameters->list = AccessSpouse::where('iduser', '=', $_GET['iduser'])->get();
            break;
            
            default:
                $parameters->list = AccessSpouse::get();
            break;
        }

        if(!in_array($_SESSION['idgroup'], [1,3])){
            $parameters->adds->iduser = $_SESSION['user_id'];
        }

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;

        $obj = new AccessSpouse;

        $parameters->id = '';
        $parameters->title = "Spouses - Cadastro";
        if ($id != '') {
            $obj = AccessSpouse::find($id);
            $parameters->id = $id;
            $parameters->title = "Spouses - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;
        $parameters->obj->parentesco = 'conjuge';

        $parameters->adds            = new stdClass;
        $parameters->adds->usuarios  = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->orderBy('name')->get();
        $parameters->adds->capitulos = AccessUser::select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
        $parameters->adds->ramoatvs  = AccessUser::select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

        if(!in_array($_SESSION['idgroup'], [1,3])){
            $parameters->adds->iduser = $_SESSION['user_id'];
        }

        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Spouse.form';
        $parameters->pagelink = 'cadastro-spouse';

        self::formSistemaDefault($parameters);
    }
}
