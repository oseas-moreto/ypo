<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessDiretoria;
use App\Models\Entities\AccessSpouse;
use App\Classes\Funcoes\Generate;
use stdClass;

class TemplateDiretoria extends Template
{
    public static function index()
    {
        $parameters           = new stdClass;
        $parameters->adds     = new stdClass;
        $parameters->title    = "Lista de Diretorias";
        $parameters->class    = 'usuarios';
        $parameters->page     = 'layout-dashboard';
        $parameters->list     = AccessDiretoria::get();
        $parameters->view     = 'Administrativo.Diretoria.index';
        $parameters->pagelink = 'cadastro-diretoria';

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->orderBy('name')->get();
        
        if(!isset($_GET['capitulo'])) $_GET['capitulo'] = 'YPO SÃO PAULO';


        switch (true) {
            case isset($_GET['capitulo']) :
                $parameters->list = AccessDiretoria::where('capitulo', '=', $_GET['capitulo'])->get();
            break;
            default:
                $parameters->list = AccessDiretoria::get();
            break;
        }
        $parameters->adds->ordemCapitulo = $parameters->list[0]->ordemCapitulo;

        $parameters->adds->usuarios  = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->orderBy('name')->get();
        $parameters->adds->spouses   = AccessSpouse::orderBy('nome')->orderBy('nome')->get();
        $parameters->adds->capitulos = AccessDiretoria::select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
        $parameters->adds->cargos    = AccessDiretoria::select('cargo')->groupBy('cargo')->orderBy('cargo')->get();

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters       = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new AccessDiretoria;
        $parameters->adds->usuarios  = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->orderBy('name')->get();
        $parameters->adds->spouses   = AccessSpouse::orderBy('nome')->get();
        $parameters->adds->capitulos = AccessDiretoria::select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
        $parameters->adds->cargos    = AccessDiretoria::select('cargo')->groupBy('cargo')->orderBy('cargo')->get();

        $parameters->id = '';
        $parameters->title = "Diretoria - Cadastro";
        if ($id != '') {
            $obj = AccessDiretoria::find($id);
            $parameters->id = $id;
            $parameters->title = "Diretoria - Editar";
        }
        $parameters->obj = $obj;

        $parameters->class    = 'cadastrar-anuncio';
        $parameters->page     = 'layout-dashboard';
        $parameters->view     = 'Administrativo.Diretoria.form';
        $parameters->pagelink = 'cadastro-diretoria';

        self::formSistemaDefault($parameters);
    }
}
