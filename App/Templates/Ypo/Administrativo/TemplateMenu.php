<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessMenu;
use App\Templates\Ypo\Modulos\TemplateLoad;
use stdClass;

class TemplateMenu extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Menus";
        $parameters->class = 'usuarios';
        $parameters->list = AccessMenu::where('status', '<>', 'd')->get();
        foreach ($parameters->list as $o) {
            $o->levelpreview = $o->level != 0 ? AccessMenu::find($o->level)->title . '->' : '';
        }
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Menus.index';
        $parameters->pagelink = 'menus';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;

        $obj = new AccessMenu;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Menus - Cadastro";
        if ($id != '') {
            $obj = AccessMenu::find($id);
            $parameters->id = $id;
            $parameters->title = "Menus - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $groups = [];
        $menusList = AccessGroup::where('status', '=', 'a')->get();

        foreach ($menusList as $m) {
            $groups['group'][] = $m;
            $groups['ckecked'][] = TemplateLoad::checkmenugroup($m->idgroup, $obj->idmenu);
        }

        $parameters->adds = new stdClass;
        $parameters->adds->groups = [];
        $parameters->adds->groups = $groups;

        $parameters->adds->levelzero = AccessMenu::where('level', '=', 0)->where('status', '=', 'a')->get();

        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Menus.form';
        $parameters->pagelink = 'menus';

        self::formSistemaDefault($parameters);
    }
}
