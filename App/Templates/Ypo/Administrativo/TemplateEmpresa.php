<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteEmpresa;
use App\Models\Entities\SiteCategory;
use stdClass;

class TemplateEmpresa extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Parceiros";
        $parameters->class = 'usuarios';
        $parameters->list = SiteEmpresa::join('site_category as a', 'empresa.idcategoria', '=', 'a.idcategory')
        ->where('ativo', '<>', 'd')
        ->select('empresa.*', 'a.title as categoria')
        ->get();
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Empresa.index';
        $parameters->pagelink = 'empresa';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteEmpresa;
        $obj->ativo = 's';
        $obj->especiaisYpo = 'n';
        $obj->botaoZap = 'n';

        $parameters->adds->categorias = [];
        $parameters->adds->categorias = SiteCategory::where('type', '=', 'parceiros')
        ->where('nivel','=',0)->where('status', '=', 'a')
        ->get();

        $parameters->adds->subcategorias = [];

        $parameters->id = '';
        $parameters->title = "Parceiros - Cadastro";
        if ($id != '') {
            $obj = SiteEmpresa::find($id);
            $parameters->adds->subcategorias = SiteCategory::where('nivel', '=', $obj->idcategoria)
            ->where('type', '=', 'parceiros')->where('status', '=', 'a')->get();

            if(strlen($obj->imagens)){
                $obj->imgArray = \json_decode($obj->imagens, true);
            }

            if(strlen($obj->redesSociais)){
                $obj->redeArray = \json_decode($obj->redesSociais, true);
            }

            $parameters->id = $id;
            $parameters->title = "Parceiros - Editar";
        }
        $parameters->obj = $obj;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Empresa.form';
        $parameters->pagelink = 'empresa';

        self::formSistemaDefault($parameters);
    }
}
