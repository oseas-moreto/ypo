<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\SiteSolicitacaoDesconto;
use App\Classes\Funcoes\SessionControl;
use App\Models\Entities\AccessUser;
use stdClass;

class TemplateSolicitacao extends Template
{
    public static function index()
    {
        SessionControl::start_session();
        $parameters = new stdClass;
        $parameters->adds = new stdClass;
        $parameters->title = "Lista de Solicitações de desconto";
        $parameters->class = 'usuarios';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Solicitacao.index';
        $parameters->pagelink = 'solicitacoes-desconto';

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '=', 4)->get();

        switch (true) {
            case $_SESSION['idgroup'] <> 1 :
                $parameters->list = SiteSolicitacaoDesconto::where('iduser', '=', $_SESSION['user_id'])->get(); 
            break;
            case isset($_GET['iduser']) :
                $parameters->list = SiteSolicitacaoDesconto::where('iduser', '=', $_GET['iduser'])->get(); 
            break;
            
            default:
                $parameters->list = SiteSolicitacaoDesconto::get();
            break;
        }

        if($_SESSION['idgroup'] <> 1){
            $parameters->adds->usuario = $_SESSION['name'];
        }

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        SessionControl::start_session();
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = SiteSolicitacaoDesconto::find($id);
        $parameters->id = $id;
        $parameters->title = "Solicitação de desconto";

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->adds->dados = json_decode($obj->dados);

        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Solicitacao.view';
        $parameters->pagelink = 'solicitacoes-desconto';

        self::formSistemaDefault($parameters);
    }
}
