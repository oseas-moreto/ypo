<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\SiteSeo;
use stdClass;

class TemplateSeo extends Template
{

    public static function index($id = '')
    {
        $parameters = new stdClass;

        $obj = new SiteSeo;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Seo - Cadastro";
        if ($id != '') {
            $obj = SiteSeo::find($id);
            $parameters->id = $id;
            $parameters->title = "Seo - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Seo.index';
        $parameters->pagelink = 'seo';

        self::formSistemaDefault($parameters);
    }
}
