<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessUser;
use App\Classes\Funcoes\Generate;
use stdClass;

class TemplateUser extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Usuários";
        $parameters->class = 'usuarios';
        $parameters->page = 'layout-dashboard';
        $parameters->list = AccessUser::where('status', '<>', 'd')->get();
        $parameters->view = 'Administrativo.User.index';
        $parameters->pagelink = 'users';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;

        $obj = new AccessUser;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Usuários - Cadastro";
        if ($id != '') {
            $obj = AccessUser::find($id);
            $parameters->id = $id;
            $parameters->title = "Usuários - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->adds            = new stdClass;
        $parameters->adds->groups    = [];
        $parameters->adds->groups    = AccessGroup::where('status', 'a')->get();
        $parameters->adds->ufs       = Generate::getUfs()->UF;
        $parameters->adds->capitulos = AccessUser::where('capitulo', '!=', '')->where('status', '!=', 'd')->select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
        $parameters->adds->ramoatvs  = AccessUser::where('ramoatv', '!=', '')->where('status', '!=', 'd')->select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();
        
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.User.form';
        $parameters->pagelink = 'users';

        self::formSistemaDefault($parameters);
    }
}
