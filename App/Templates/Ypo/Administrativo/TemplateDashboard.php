<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessSpouse;
use App\Classes\Funcoes\Generate;
use stdClass;

class TemplateDashboard extends Template
{
    public static function index(){
        $parameters = new stdClass;
        $parameters->title    = "Dashboard";
        $parameters->class    = 'usuarios';
        $parameters->list     = new stdClass;
        $parameters->siderbar = true;

        $parameters->page     = 'layout-dashboard';
        $parameters->view     = 'Administrativo.Dashboard.index';
        $parameters->pagelink = '/dashboard';

        self::indexSistemaDefault($parameters);
    }

    public static function perfil($id = '')
    {
        $parameters = new stdClass;

        $obj = AccessUser::find($id);
        $parameters->id = $id;
        $parameters->title = "Perfil";

        $parameters->class = 'cadastrar-anuncio';

        $parameters->adds = new stdClass;
        $parameters->adds->ufs = Generate::getUfs()->UF;
        $parameters->obj = $obj;
        $parameters->adds->capitulos = AccessUser::select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
        $parameters->adds->ramoatvs  = AccessUser::select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Dashboard.perfil';
        $parameters->pagelink = '/perfil/' . $id;

        self::formSistemaDefault($parameters);
    }

    public static function perfilSpouse($id = '')
    {
        $parameters = new stdClass;
        $obj = AccessSpouse::find($id);
        
        $parameters->id = $id;
        $parameters->title = "Perfil";

        $parameters->class = 'cadastrar-anuncio';

        $parameters->adds = new stdClass;
        $parameters->obj = $obj;
        $parameters->adds->capitulos = AccessSpouse::select('capitulo')->groupBy('capitulo')->orderBy('capitulo')->get();
        $parameters->adds->ramoatvs  = AccessSpouse::select('ramoatv')->groupBy('ramoatv')->orderBy('ramoatv')->get();

        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Dashboard.perfil-spouse';
        $parameters->pagelink = '/perfil/' . $id;

        self::formSistemaDefault($parameters);
    }
}
