<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteCategory;
use stdClass;

class TemplateCategory extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Categorias";
        $parameters->class = 'usuarios';
        $parameters->list = SiteCategory::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Category.index';
        $parameters->pagelink = 'categories';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteCategory;
        $parameters->adds->status = 'a';
        $parameters->adds->listagem = 'n';
        $parameters->adds->type   = 'parceiros';
        $parameters->adds->image  = '';
        $parameters->adds->order  = '';
        $parameters->adds->icon   = '';
        $parameters->adds->id     = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $parameters->adds->categorias = [];
        $parameters->adds->categorias = SiteCategory::where('nivel','=',0)->orderBy('type')->orderBy('title')
        ->get();

        $parameters->adds->types = SiteCategory::getTypes();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Categorias  - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteCategory::where('idcategory', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idcategory)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->idcategory;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->listagem = $objs[$l->idlanguage]->listagem;
                    $parameters->adds->imagem = $objs[$l->idlanguage]->imagem;
                    $parameters->adds->type   = $objs[$l->idlanguage]->type;
                    $parameters->adds->order  = $objs[$l->idlanguage]->order;
                    $parameters->adds->icon   = $objs[$l->idlanguage]->icon;
                } else {
                    $objs[$l->idlanguage] = new SiteCategory();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Categorias  - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Category.form';
        $parameters->pagelink = 'categories';

        self::formSistemaDefault($parameters);
    }
}
