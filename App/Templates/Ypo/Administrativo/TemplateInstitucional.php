<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteInstitucional;
use stdClass;

class TemplateInstitucional extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Páginas Institucionais";
        $parameters->class = 'usuarios';
        $parameters->list = SiteInstitucional::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Institucional.index';
        $parameters->pagelink = 'institucional';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteInstitucional;
        $parameters->adds->status = 'a';
        $parameters->adds->imagem = '';
        $parameters->adds->id = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Páginas Institucionais - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteInstitucional::where('idinstitucional', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idinstitucional)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->idinstitucional;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->imagem = $objs[$l->idlanguage]->imagem;
                } else {
                    $objs[$l->idlanguage] = new SiteInstitucional();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Páginas Institucionais - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Institucional.form';
        $parameters->pagelink = 'institucional';

        self::formSistemaDefault($parameters);
    }
}
