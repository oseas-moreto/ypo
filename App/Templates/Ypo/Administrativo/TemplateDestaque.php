<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteDestaque;
use App\Models\Entities\SiteCategory;
use stdClass;

class TemplateDestaque extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Destaques";
        $parameters->class = 'usuarios';
        $parameters->list = SiteDestaque::where('ativo', '<>', 'd')->get();
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Destaque.index';
        $parameters->pagelink = 'destaques-home';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteDestaque;
        $obj->ativo = 's';

        $parameters->id = '';
        $parameters->title = "Destaques - Cadastro";
        if ($id != '') {
            $obj = SiteDestaque::find($id);
            $parameters->id = $id;
            $parameters->title = "Destaques - Editar";
        }
        $parameters->obj = $obj;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Destaque.form';
        $parameters->pagelink = 'destaques-home';

        self::formSistemaDefault($parameters);
    }
}
