<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteVideoteca;
use App\Models\Entities\SiteCategory;
use stdClass;

class TemplateVideoteca extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Videos";
        $parameters->class = 'usuarios';
        $parameters->list = SiteVideoteca::where('ativo', '<>', 'd')->get();
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Videoteca.index';
        $parameters->pagelink = 'videoteca-cadastro';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteVideoteca;
        $obj->ativo = 's';

        $parameters->adds->categorias = [];
        $parameters->adds->categorias = SiteCategory::where('type', '=', 'videoteca')
        ->where('nivel','=',0)
        ->get();

        $parameters->id = '';
        $parameters->title = "Videos - Cadastro";
        if ($id != '') {
            $obj = SiteVideoteca::find($id);
            $parameters->id = $id;
            $parameters->title = "Videos - Editar";
        }
        $parameters->obj = $obj;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Videoteca.form';
        $parameters->pagelink = 'videoteca-cadastro';

        self::formSistemaDefault($parameters);
    }
}
