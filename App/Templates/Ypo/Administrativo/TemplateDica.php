<?php
namespace App\Templates\Ypo\Administrativo;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteDica;
use App\Models\Entities\SiteCategory;
use App\Classes\Funcoes\Generate;
use App\Models\Entities\AccessUser;
use stdClass;

class TemplateDica extends Template
{
    public static function index()
    {
        $parameters       = new stdClass;
        $parameters->adds = new stdClass;

        $parameters->title = "Lista de Dicas";
        $parameters->class = 'usuarios';
        
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Dica.index';
        $parameters->pagelink = 'dicas';

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1);

        switch (true) {
            case $_SESSION['idgroup'] <> 1 :
                $parameters->list = SiteDica::join('site_category as a', 'dicas.idcategoria', '=', 'a.idcategory')
                ->where('ativo', '<>', 'd')->where('iduser', '=', $_SESSION['user_id']); 
            break;
            case isset($_GET['iduser']) :
                $parameters->list = SiteDica::join('site_category as a', 'dicas.idcategoria', '=', 'a.idcategory')
                ->where('ativo', '<>', 'd')->where('iduser', '=', $_GET['iduser']); 
            break;
            
            default:
                $parameters->list = SiteDica::join('site_category as a', 'dicas.idcategoria', '=', 'a.idcategory')
                ->where('ativo', '<>', 'd');
            break;
        }
        
        $parameters->adds->categorias = SiteCategory::where('idlanguage', '=', 1)
        ->where('status', '=', 'a')
        ->where('nivel', '=', 0)
        ->where('type', '=', 'dicas')
        ->orderBy('title', 'ASC')
        ->get();
        
        $idCategoria    = isset($_GET['categoria'])    ? $_GET['categoria']    : 0;
        $idSubCategoria = isset($_GET['subcategoria']) ? $_GET['subcategoria'] : 0;
        $parameters->adds->subscategoria  = [];

        if($idSubCategoria > 0){
            $subcategoria = SiteCategory::where('idlanguage', '=', 1)
            ->where('idcategory', '=', $idSubCategoria)
            ->first();

            $_GET['categoria'] = $subcategoria->nivel;
            $idCategoria       = $subcategoria->nivel;
        }

        if($idCategoria > 0){
            $parameters->adds->subscategoria = SiteCategory::where('idlanguage', '=', 1)
                ->where('nivel', '=', $idCategoria)
                ->orderBy('title')
                ->get();
        }

        if($idCategoria > 0){ 
            $parameters->list->where('idcategoria', '=', $idCategoria);
        }

        if($idSubCategoria > 0){
            $parameters->list->where('idcategoria2', '=', $idSubCategoria);
        }

        $parameters->list = $parameters->list->select('dicas.*', 'a.title as categoria')->get();

        if($_SESSION['idgroup'] <> 1){
            $parameters->adds->usuario = $_SESSION['name'];
        }

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteDica;
        $obj->ativo = 's';

        $parameters->adds->categorias = [];
        $parameters->adds->categorias = SiteCategory::where('type', '=', 'dicas')
        ->where('nivel','=',0)
        ->where('status', '=', 'a')
        ->get();

        $parameters->adds->subcategorias = [];

        $parameters->id = '';
        $parameters->title = "Dicas - Cadastro";
        if ($id != '') {
            $obj = SiteDica::find($id);
            $parameters->id = $id;
            $parameters->title = "Dicas - Editar";
            $parameters->adds->subcategorias = SiteCategory::where('nivel', '=', $obj->idcategoria)
            ->where('type', '=', 'dicas')->where('status', '=', 'a')->get();
        }
        $parameters->obj = $obj;

        $parameters->adds->usuarios = AccessUser::where('status', '<>', 'd')->where('idgroup', '<>', 1)->get();

        if($_SESSION['idgroup'] <> 1){
            $parameters->adds->iduser = $_SESSION['user_id'];
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout-dashboard';
        $parameters->view = 'Administrativo.Dica.form';
        $parameters->pagelink = 'dicas';
        $parameters->adds->ufs = Generate::getUfs()->UF;

        self::formSistemaDefault($parameters);
    }
}
