<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteFaq;
use stdClass;

class TemplateFaq extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Faq";
        $parameters->class = 'usuarios';
        $parameters->list = SiteFaq::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Faq.index';
        $parameters->pagelink = 'sistema/faq';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteFaq;
        $parameters->adds->status   = 'a';
        $parameters->adds->response = [];
        $parameters->adds->order  = '';
        $parameters->adds->id     = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Faq - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteFaq::where('idfaq', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idfaq)) {
                    $dataJson = json_decode($objs[$l->idlanguage]->response, true);
                    $dataJson = self::ordenarRespostas($dataJson);
                    
                    $parameters->adds->id       = $objs[$l->idlanguage]->idfaq;
                    $parameters->adds->status   = $objs[$l->idlanguage]->status;
                    $parameters->adds->image    = $objs[$l->idlanguage]->image;
                    $objs[$l->idlanguage]->response = $dataJson;
                    $parameters->adds->order    = $objs[$l->idlanguage]->order;
                } else {
                    $objs[$l->idlanguage] = new SiteFaq();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Faq - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Faq.form';
        $parameters->pagelink = 'sistema/faq';

        self::formSistemaDefault($parameters);
    }

    public static function ordenarRespostas($data){
        $finalArray = [];
        foreach ($data as $key => $d) {
            $newData = [];
            foreach ($d as $faq) {
                $newData[$faq['orderResponse']] = $faq;
            }
            ksort($newData);
            $finalArray[$key] = array_values($newData);
        }
        
        return $finalArray;
    }
}
