<?php

namespace App\Templates\Sistema\Modulos;

use App\Classes\Funcoes\SessionControl;
use App\Controllers\Sistema\Login;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessMenu;
use App\Models\Entities\Configuration;
use App\Models\Entities\SiteSeo;
use App\Models\Entities\Contacts;
use App\Models\Entities\SiteBlog;
use stdClass;

class TemplateLoad
{
    public static function info()
    {
        $info = new stdClass;
        $info->datasite = Configuration::find(1);
        $info->seo = SiteSeo::find(1);
        $contacts = Contacts::where('view', '=', 'n')->groupBy('name')->orderBy('date_create', 'DESC');
        $info->countContacts = $contacts->count();
        $info->contacts = $contacts->get();


        return $info;
    }

    public static function sidebar()
    {
        SessionControl::start_session();

        if(!isset($_SESSION['idgroup']) or !is_numeric($_SESSION['idgroup'])){
            (new Login)->logout();
            exit;
        }

        $url = explode('/', $_SERVER['REQUEST_URI']);
        $current = isset($url[2]) ? $url[2] : 'home';
        $group_access = new AccessGroup();
        $leveltop = $group_access->pagelist($_SESSION['idgroup'], 0);
        $menu = [];
        $submenu = [];
        $i = 0;

        unset($url[0]);
        unset($url[1]);
        $arrayDelecoes = ['create', 'update'];
        foreach($url as $key => $link){
            if(in_array($link, $arrayDelecoes) || is_numeric($link)){
                unset($url[$key]);
                continue;
            }
        }
        
        $urlAtual = \implode('/', $url);

        foreach ($leveltop as $top) {
            $functionAtual = $top->urlmenu;
            $menu[$i]['title'] = $top->title;
            $menu[$i]['icon'] = $top->icon;
            $menu[$i]['level'] = $top->level;
            $menu[$i]['function'] = $top->urlmenu;
            $menu[$i]['idmenu'] = $top->idmenu;
            $menu[$i]['active'] = strlen($urlAtual) && $urlAtual == $functionAtual ? 'menu-open' : '';
            $menu[$i]['focus'] = strlen($urlAtual) && $urlAtual == $functionAtual ? 'bg-info' : '';
            $menu[$i]['submenus'] = [];

            $submenus = $group_access->pagelist($_SESSION['idgroup'], $top->idmenu);

            $menu[$i]['total'] = count($submenus);

            $s = 0;
            $active = 0;
            foreach ($submenus as $sub) {
                $functionAtual = $sub->urlmenu;
                $submenu[$s]['title'] = $sub->title;
                $submenu[$s]['icon'] = $sub->icon;
                $submenu[$s]['function'] = $sub->urlmenu;
                $submenu[$s]['focus'] = strlen($urlAtual) && $urlAtual == $functionAtual ? 'bg-submenu' : '';
                if(strlen($urlAtual) && $urlAtual == $functionAtual) $active ++;
                $s++;
            }
            $menu[$i]['active'] = $active >0 ? 'menu-open' : $menu[$i]['active'];
            $menu[$i]['submenus'] = $submenu;
            $i++;
        }
        
        $mretorno['leveltop'] = $menu;
        $mretorno['total'] = count($menu);
        $mretorno['current'] = $current;

        return $mretorno;
    }

    public static function checkmenugroup($group, $menu)
    {
        $return = AccessMenu::join('access_groupxmenu', 'access_menu.idmenu', '=', 'access_groupxmenu.idmenu')
            ->where('access_groupxmenu.idgroup', '=', $group)->where('access_groupxmenu.idmenu', '=', $menu)
            ->select('access_groupxmenu.idmenu')->count();

        if ($return > 0) {
            $checked = 'checked="checked"';
        } else {
            $checked = '';
        }

        return $checked;
    }

}
