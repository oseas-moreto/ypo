<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\SiteSocial;
use stdClass;

class TemplateSocial extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Redes Sociais";
        $parameters->class = 'usuarios';
        $parameters->list = SiteSocial::where('status', '<>', 'd')->get();
        $parameters->page = 'layout';
        $parameters->view = 'Social.index';
        $parameters->pagelink = 'sistema/social';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteSocial;
        $obj->status = 'a';
        $maxOrder = SiteSocial::orderBy('ordem', 'DESC')->first();
        $obj->ordem = isset($maxOrder->ordem) ? $maxOrder->ordem : 1;
        $parameters->id = '';
        $parameters->title = "Redes Sociais - Cadastro";
        if ($id != '') {
            $obj = SiteSocial::find($id);
            $parameters->id = $id;
            $parameters->title = "Redes Sociais - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->page = 'layout';
        $parameters->view = 'Social.form';
        $parameters->pagelink = 'sistema/social';

        self::formSistemaDefault($parameters);
    }
}
