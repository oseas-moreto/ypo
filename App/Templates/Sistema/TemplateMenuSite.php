<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteMenu;
use App\Templates\Sistema\Modulos\TemplateLoad;
use stdClass;

class TemplateMenuSite extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Menu do site";
        $parameters->class = 'usuarios';
        $parameters->list = SiteMenu::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'MenuSite.index';
        $parameters->pagelink = 'sistema/menu-site';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteMenu;
        $parameters->adds->status       = 'a';
        $parameters->adds->link_externo = 'n';
        $parameters->adds->image        = '';
        $parameters->adds->id           = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Menu - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteMenu::where('idmenu', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idmenu)) {
                    $parameters->adds->id = $objs[$l->idlanguage]->idmenu;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->link_externo = $objs[$l->idlanguage]->link_externo;

                } else {
                    $objs[$l->idlanguage] = new SiteMenu();
                }
            }
            $parameters->id = $id;
            $parameters->title = "Menu - Editar";
        }
        $parameters->obj = $objs;
        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'MenuSite.form';
        $parameters->pagelink = 'sistema/menu-site';

        self::formSistemaDefault($parameters);
    }
}
