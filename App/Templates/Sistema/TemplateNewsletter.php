<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Newsletter;;
use stdClass;

class TemplateNewsletter extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Newsletter";
        $parameters->class = 'usuarios';
        $parameters->list = Newsletter::all();
        $parameters->page = 'layout';
        $parameters->view = 'Newsletter.index';
        $parameters->pagelink = 'sistema/newsletter';

        self::indexSistemaDefault($parameters);
    }

}
