<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessUser;
use stdClass;

class TemplateUser extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Usuários";
        $parameters->class = 'usuarios';
        $parameters->page = 'layout';
        $parameters->list = AccessUser::where('status', '<>', 'd')->get();
        $parameters->view = 'User.index';
        $parameters->pagelink = 'sistema/users';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;

        $obj = new AccessUser;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Usuários - Cadastro";
        if ($id != '') {
            $obj = AccessUser::find($id);
            $parameters->id = $id;
            $parameters->title = "Usuários - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->adds = new stdClass;
        $parameters->adds->groups = [];
        $parameters->adds->groups = AccessGroup::where('status', '=', 'a')->get();

        $parameters->page = 'layout';
        $parameters->view = 'User.form';
        $parameters->pagelink = 'sistema/users';

        self::formSistemaDefault($parameters);
    }
}
