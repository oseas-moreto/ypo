<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Configuration;
use stdClass;

class TemplateSettings extends Template
{

    public static function index($id = '')
    {
        $parameters = new stdClass;

        $obj = new Configuration;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Configurações - Cadastro";
        if ($id != '') {
            $obj = Configuration::find($id);
            $parameters->id = $id;
            $parameters->title = "Configurações - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->page = 'layout';
        $parameters->view = 'Settings.index';
        $parameters->pagelink = 'sistema/settings';

        self::formSistemaDefault($parameters);
    }
}
