<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\SiteModule;
use App\Models\Entities\Language;
use stdClass;

class TemplateModule extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Modulos";
        $parameters->class = 'usuarios';
        $parameters->list = SiteModule::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Module.index';
        $parameters->pagelink = 'sistema/modules';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteModule;
        $parameters->adds->status = 'a';
        $parameters->adds->id = '';

        $parameters->id    = '';
        $parameters->title = "Modulos - Cadastro";
        $fields            = [];
        $inputJson         = [];
        
        $parameters->adds->fields = [];

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        
        if (is_numeric($id)) {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteModule::where('idmodule', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idmodule)) {
                    $parameters->adds->id = $objs[$l->idlanguage]->idmodule;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $fields[$l->idlanguage] = json_decode($objs[$l->idlanguage]->json, true);
                } else {
                    $objs[$l->idlanguage] = new SiteModule();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Modulos - Editar";

       
            //ITENS MODULO
            if(count($fields) > 0){
                $jsonModulo = [];
                foreach ($fields as $key => $json) {
                    foreach($json as $jkey => $j){
                        $jsonModulo[$jkey]['linguagem'][$key] = [
                            'title'        => $j['title'],
                            'titleSecond'  => $j['titleSecond'],
                            'text'         => $j['text'],
                            'order'        => $j['order']
                        ];
                        $jsonModulo[$jkey]['icon']   = $j['icon'];
                        $jsonModulo[$jkey]['image']  = $j['image'];
                        $jsonModulo[$jkey]['status'] = $j['status'];
                    }
                }

                foreach ($jsonModulo as $key => $value) {
                    $inputJson[] = json_encode($value);
                }
            }
        }
        $parameters->adds->inputJson = $inputJson;
        $parameters->obj = $objs;
        

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Module.form';
        $parameters->pagelink = 'sistema/modules';

        self::formSistemaDefault($parameters);
    }
}
