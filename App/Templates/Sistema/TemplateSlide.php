<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteBanner;
use stdClass;

class TemplateSlide extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Banners";
        $parameters->class = 'usuarios';
        $parameters->list = SiteBanner::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Slide.index';
        $parameters->pagelink = 'sistema/slides';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteBanner;
        $parameters->adds->status       = 'a';
        $parameters->adds->image        = '';
        $parameters->adds->image_mobile = '';
        $parameters->adds->hash         = '';
        $parameters->adds->order        = '';
        $parameters->adds->id           = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Banners - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteBanner::where('idbanner', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idbanner)) {
                    $parameters->adds->image        = $objs[$l->idlanguage]->image;
                    $parameters->adds->image_mobile = $objs[$l->idlanguage]->image_mobile;
                    $parameters->adds->hash         = $objs[$l->idlanguage]->hash;
                    $parameters->adds->order        = $objs[$l->idlanguage]->order;
                    $parameters->adds->id           = $objs[$l->idlanguage]->idbanner;
                    $parameters->adds->status       = $objs[$l->idlanguage]->status;
                } else {
                    $objs[$l->idlanguage] = new SiteBanner();
                }
            }
            $parameters->id = $id;
            $parameters->title = "Banners - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Slide.form';
        $parameters->pagelink = 'sistema/slides';

        self::formSistemaDefault($parameters);
    }
}
