<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteProductCategory;
use stdClass;

class TemplateProductCategory extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Categorias de produtos & serviços";
        $parameters->class = 'usuarios';
        $parameters->list = SiteProductCategory::
        orWhere(function($q){
            $q->where('type', '=', 'servico');
            $q->orWhere('type', '=', 'produto');
        })
        ->where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Product.Category.index';
        $parameters->pagelink = 'sistema/products/categories';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteProductCategory;
        $parameters->adds->status = 'a';
        $parameters->adds->type   = 'produto';
        $parameters->adds->image  = '';
        $parameters->adds->order  = '';
        $parameters->adds->icon   = '';
        $parameters->adds->id     = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Categorias de produtos & serviços - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteProductCategory::where('idcategory', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idcategory)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->idcategory;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->imagem = $objs[$l->idlanguage]->imagem;
                    $parameters->adds->type   = $objs[$l->idlanguage]->type;
                    $parameters->adds->order  = $objs[$l->idlanguage]->order;
                    $parameters->adds->icon   = $objs[$l->idlanguage]->icon;
                } else {
                    $objs[$l->idlanguage] = new SiteProductCategory();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Categorias de produtos & serviços - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Product.Category.form';
        $parameters->pagelink = 'sistema/products/categories';

        self::formSistemaDefault($parameters);
    }
}
