<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\Team;
use App\Models\Entities\TeamOffice;
use stdClass;

class TemplateTeam extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Equipe";
        $parameters->class = 'usuarios';
        $parameters->list = Team::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Teams.index';
        $parameters->pagelink = 'sistema/teams';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new Team;
        $parameters->adds->status       = 'a';
        $parameters->adds->whatsapp     = 'n';
        $parameters->adds->image        = '';
        $parameters->adds->order        = '';
        $parameters->adds->mobilephone  = '';
        $parameters->adds->vcard        = '';
        $parameters->adds->email        = '';
        $parameters->adds->skype        = '';
        $parameters->adds->idcargo      = '';
        $parameters->adds->idposicao    = '';
        $parameters->adds->idescritorio = '';
        $parameters->adds->id           = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $parameters->adds->cargo      = TeamOffice::where('status', '=', 'a')->where('type', '=', 'cargo')->where('idlanguage', '=', 1)->get();
        $parameters->adds->posicao    = TeamOffice::where('status', '=', 'a')->where('type', '=', 'posicao')->where('idlanguage', '=', 1)->get();
        $parameters->adds->escritorio = TeamOffice::where('status', '=', 'a')->where('type', '=', 'escritorio')->where('idlanguage', '=', 1)->get();
        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Equipe - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = Team::where('idteam', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idteam)) {
                    $parameters->adds->image        = $objs[$l->idlanguage]->image;
                    $parameters->adds->id           = $objs[$l->idlanguage]->idteam;
                    $parameters->adds->status       = $objs[$l->idlanguage]->status;
                    $parameters->adds->image        = $objs[$l->idlanguage]->image;
                    $parameters->adds->vcard        = $objs[$l->idlanguage]->vcard;
                    $parameters->adds->order        = $objs[$l->idlanguage]->order;
                    $parameters->adds->mobilephone  = $objs[$l->idlanguage]->mobilephone;
                    $parameters->adds->whatsapp     = $objs[$l->idlanguage]->whatsapp;
                    $parameters->adds->email        = $objs[$l->idlanguage]->email;
                    $parameters->adds->skype        = $objs[$l->idlanguage]->skype;
                    $parameters->adds->idcargo      = $objs[$l->idlanguage]->idcargo;
                    $parameters->adds->idposicao    = $objs[$l->idlanguage]->idposicao;
                    $parameters->adds->idescritorio = $objs[$l->idlanguage]->idescritorio;
                } else {
                    $objs[$l->idlanguage] = new Team();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Equipe - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Teams.form';
        $parameters->pagelink = 'sistema/teams';

        self::formSistemaDefault($parameters);
    }
}
