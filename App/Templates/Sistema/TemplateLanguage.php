<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\AccessMenu;
use App\Templates\Sistema\Modulos\TemplateLoad;
use stdClass;

class TemplateLanguage extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Idiomas";
        $parameters->class = 'usuarios';
        $parameters->list = Language::where('status', '<>', 'd')->get();
        $parameters->page = 'layout';
        $parameters->view = 'Languages.index';
        $parameters->pagelink = 'sistema/languages';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;

        $obj = new Language;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Idiomas - Cadastro";
        if ($id <> '') {
            $obj = Language::find($id);
            $parameters->id = $id;
            $parameters->title = "Idiomas - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->page = 'layout';
        $parameters->view = 'Languages.form';
        $parameters->pagelink = 'sistema/languages';

        self::formSistemaDefault($parameters);
    }
}
