<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteBlogAuthor;
use stdClass;

class TemplateBlogAuthor extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Autores ";
        $parameters->class = 'usuarios';
        $parameters->list = SiteBlogAuthor::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'BlogAuthor.index';
        $parameters->pagelink = 'sistema/blogs/authors';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteBlogAuthor;
        $parameters->adds->status = 'a';
        $parameters->adds->id = '';
        $parameters->adds->image = '';
        $parameters->adds->image_mobile = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Autores - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteBlogAuthor::where('idauthor', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idauthor)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->idauthor;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                } else {
                    $objs[$l->idlanguage] = new SiteBlogAuthor();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Autores - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'BlogAuthor.form';
        $parameters->pagelink = 'sistema/blogs/authors';

        self::formSistemaDefault($parameters);
    }
}
