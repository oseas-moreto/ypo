<?php
namespace App\Templates\Sistema;

use App\Classes\Funcoes\Generate;
use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteBlogAuthor;
use App\Models\Entities\SiteBlog;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteBlogType;

use stdClass;

class TemplateBlog extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Blog";
        $parameters->class = 'usuarios';
        $parameters->list = SiteBlog::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        foreach ($parameters->list as $v) {
            $v->title_url = Generate::url_generate($v->title);

        }
        $parameters->page = 'layout';
        $parameters->view = 'Blog.index';
        $parameters->pagelink = 'sistema/blogs';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteBlog;
        $maxOrder = SiteBlog::orderBy('ordem', 'DESC')->first();
        $obj->ordem = isset($maxOrder->ordem) ? $maxOrder->ordem : 1;
        $parameters->adds->status = 'a';
        $parameters->adds->spotlight = 'n';
        $parameters->adds->image = '';
        $parameters->adds->id = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();
        $parameters->adds->authors = [];
        $parameters->adds->authors = SiteBlogAuthor::where('status', '=', 'a')->where('idlanguage', '=', 1)->get();
        $parameters->adds->categories = SiteCategory::where('status', '=', 'a')->where('idlanguage', '=', 1)->where('type', '=', 'blog')->get();
        $parameters->adds->types = SiteBlogType::where('status', '=', 'a')->where('idlanguage', '=', 1)->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Blog - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteBlog::where('idblog', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idblog)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->idblog;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->spotlight = $objs[$l->idlanguage]->spotlight;
                } else {
                    $objs[$l->idlanguage] = new SiteBlog();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Blog - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Blog.form';
        $parameters->pagelink = 'sistema/blogs';

        self::formSistemaDefault($parameters);
    }
}
