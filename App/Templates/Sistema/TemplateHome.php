<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\AccessUser;
use App\Models\Entities\Contacts;
use App\Models\Entities\SiteBlog;
use \App\Classes\GoogleAPi\Gapi;
use stdClass;

class TemplateHome extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;
        
        $parameters->title = "Dashboard";
        $parameters->class = 'usuarios';
        $contacts = Contacts::where('view', '=', 'n')->groupBy('name')->orderBy('date_create', 'DESC')->get();
        $parameters->adds->countBlog = SiteBlog::where('status', '=', 'a')->where('idlanguage', '=', 1)->count();

        $id = 182514602;
        $ga = ONLINE ? new Gapi('oseas-api-teste@oseas-teste-api.iam.gserviceaccount.com','../../../bin/oseas-teste-api-36437271b3b5.p12') : false;
        $viewsTrintaDias = 1;
        $viewsSessentaDias = 1;
        if($ga){
            // Define o periodo do relatório
            $inicio = date('Y-m-01', strtotime('-1 month')); // 1° dia do mês passado
            $fim = date('Y-m-t', strtotime('-1 month')); // Último dia do mês passado
    
            $ga->requestReportData($id, 'month', array('pageviews', 'visits'), null, null, $inicio, $fim);
            
            $viewsTrintaDias = $ga->getVisits();
    
            // Define o periodo do relatório
            $inicio = date('Y-m-01', strtotime('-2 month')); // 1° dia do mês passado
            $fim = date('Y-m-t', strtotime('-2 month')); // Último dia do mês passado
            
            $ga->requestReportData($id, 'month', array('pageviews', 'visits'), null, null, $inicio, $fim);
            $viewsSessentaDias = $ga->getVisits();
        }

        $parameters->adds->countContacts        = $contacts->count();
        $parameters->adds->visitasUnicas        = $viewsTrintaDias;
        $parameters->adds->porcentagemDiferenca = round((($viewsTrintaDias-$viewsSessentaDias)/$viewsSessentaDias)*100);
        $parameters->list = $contacts;
        $parameters->page = 'layout';
        $parameters->view = 'Home.index';
        $parameters->pagelink = 'sistema/home';

        self::indexSistemaDefault($parameters);
    }

    public static function perfil($id = '')
    {
        $parameters = new stdClass;

        $obj = AccessUser::find($id);
        $parameters->id = $id;
        $parameters->title = "Perfil";

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->page = 'layout';
        $parameters->view = 'Home.perfil';
        $parameters->pagelink = 'sistema/users/' . $id;

        self::formSistemaDefault($parameters);
    }
}
