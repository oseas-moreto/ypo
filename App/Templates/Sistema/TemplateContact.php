<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Contacts;;
use stdClass;

class TemplateContact extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Contatos";
        $parameters->class = 'usuarios';
        $parameters->list = Contacts::all();
        $parameters->page = 'layout';
        $parameters->view = 'Contact.index';
        $parameters->pagelink = 'sistema/contacts';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;

        $obj = new Contacts;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Contatos - Cadastro";
        if ($id <> '') {
            $obj = Contacts::find($id);
            $obj->view = 's';
            $obj->save();
            $parameters->id = $id;
            $parameters->title = "Contatos - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->page = 'layout';
        $parameters->view = 'Contact.view';
        $parameters->pagelink = 'sistema/contacts';

        self::formSistemaDefault($parameters);
    }
}
