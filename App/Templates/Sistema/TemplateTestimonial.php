<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteTestimonial;
use stdClass;

class TemplateTestimonial extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Depoimentos";
        $parameters->class = 'usuarios';
        $parameters->list = SiteTestimonial::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Testimonial.index';
        $parameters->pagelink = 'sistema/testimonial';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteTestimonial;
        $parameters->adds->status = 'a';
        $parameters->adds->image = '';
        $parameters->adds->home = 'n';
        $parameters->adds->order = '';
        $parameters->adds->site = '';
        $parameters->adds->link = '';
        $parameters->adds->id = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();
        $parameters->adds->categories = [];
        $parameters->adds->categories = SiteCategory::where('status', '=', 'a')->where('type', '=', 'cliente')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Depoimentos";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteTestimonial::where('idtestimonial', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idtestimonial)) {
                    $parameters->adds->image  = $objs[$l->idlanguage]->image;
                    $parameters->adds->id     = $objs[$l->idlanguage]->idtestimonial;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->home   = $objs[$l->idlanguage]->home;
                    $parameters->adds->order  = $objs[$l->idlanguage]->order;
                    $parameters->adds->site   = $objs[$l->idlanguage]->site;
                    $parameters->adds->link   = $objs[$l->idlanguage]->link;
                } else {
                    $objs[$l->idlanguage] = new SiteTestimonial();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Depoimentos";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Testimonial.form';
        $parameters->pagelink = 'sistema/testimonial';

        self::formSistemaDefault($parameters);
    }
}
