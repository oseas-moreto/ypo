<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteProductService;
use App\Models\Entities\SiteProductCategory;
use App\Models\Entities\SiteProductSegment;
use stdClass;

class TemplateProductService extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Serviços";
        $parameters->class = 'usuarios';
        $parameters->list = SiteProductService::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Product.Service.index';
        $parameters->pagelink = 'sistema/products/services';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteProductService;
        $parameters->adds->status      = 'a';
        $parameters->adds->home        = 'n';
        $parameters->adds->image       = '';
        $parameters->adds->order       = '';
        $parameters->adds->icon        = '';
        $parameters->adds->video       = '';
        $parameters->adds->doc         = '';
        $parameters->adds->price       = '';
        $parameters->adds->productcode = '';
        $parameters->adds->photos      = ['name'];
        $parameters->adds->id          = '';
        $parameters->adds->idcategory  = '';
        $parameters->adds->idsegment   = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $parameters->adds->categories = [];
        $parameters->adds->categories = SiteProductCategory::where('status', '=', 'a')->where('type', '=', 'servico')->where('idlanguage', '=', 1)->get();

        $parameters->adds->segments = [];
        $parameters->adds->segments = SiteProductSegment::where('status', '=', 'a')->where('type', '=', 'servico')->where('idlanguage', '=', 1)->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Serviços - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteProductService::where('idservice', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idservice)) {
                    $parameters->adds->image       = $objs[$l->idlanguage]->image;
                    $parameters->adds->id          = $objs[$l->idlanguage]->idservice;
                    $parameters->adds->status      = $objs[$l->idlanguage]->status;
                    $parameters->adds->image       = $objs[$l->idlanguage]->image;
                    $parameters->adds->order       = $objs[$l->idlanguage]->order;
                    $parameters->adds->icon        = $objs[$l->idlanguage]->icon;
                    $parameters->adds->home        = $objs[$l->idlanguage]->home;
                    $parameters->adds->video       = $objs[$l->idlanguage]->video;
                    $parameters->adds->doc         = $objs[$l->idlanguage]->doc;
                    $parameters->adds->price       = $objs[$l->idlanguage]->price;
                    $parameters->adds->productcode = $objs[$l->idlanguage]->productcode;
                    $parameters->adds->photos      = json_decode($objs[$l->idlanguage]->photos,true);
                    $parameters->adds->idcategory  = $objs[$l->idlanguage]->idcategory;
                    $parameters->adds->idsegment   = $objs[$l->idlanguage]->idsegment;
                } else {
                    $objs[$l->idlanguage] = new SiteProductService();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Serviços - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Product.Service.form';
        $parameters->pagelink = 'sistema/products/services';

        self::formSistemaDefault($parameters);
    }
}
