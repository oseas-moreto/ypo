<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteProductSegment;
use stdClass;

class TemplateProductSegment extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Segmentos de produtos & serviços";
        $parameters->class = 'usuarios';
        $parameters->list = SiteProductSegment::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Product.Segment.index';
        $parameters->pagelink = 'sistema/products/segments';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteProductSegment;
        $parameters->adds->status = 'a';
        $parameters->adds->type   = 'produto';
        $parameters->adds->image  = '';
        $parameters->adds->order  = '';
        $parameters->adds->icon   = '';
        $parameters->adds->id     = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Segmentos de produtos & serviços - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteProductSegment::where('idsegment', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idsegment)) {
                    $parameters->adds->image  = $objs[$l->idlanguage]->image;
                    $parameters->adds->id     = $objs[$l->idlanguage]->idsegment;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->type   = $objs[$l->idlanguage]->type;
                    $parameters->adds->order  = $objs[$l->idlanguage]->order;
                    $parameters->adds->icon   = $objs[$l->idlanguage]->icon;
                } else {
                    $objs[$l->idlanguage] = new SiteProductSegment();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Segmentos de produtos & serviços - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Product.Segment.form';
        $parameters->pagelink = 'sistema/products/segments';

        self::formSistemaDefault($parameters);
    }
}
