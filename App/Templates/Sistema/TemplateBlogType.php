<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteBlogType;
use stdClass;

class TemplateBlogType extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Tipo de postagem";
        $parameters->class = 'usuarios';
        $parameters->list = SiteBlogType::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'BlogType.index';
        $parameters->pagelink = 'sistema/blogs/types';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteBlogType;
        $parameters->adds->status = 'a';
        $parameters->adds->id = '';
        $parameters->adds->image = '';
        $parameters->adds->image_mobile = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Tipo de postagem - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteBlogType::where('idtype', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idtype)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->image_mobile = $objs[$l->idlanguage]->image_mobile;
                    $parameters->adds->id = $objs[$l->idlanguage]->idtype;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                } else {
                    $objs[$l->idlanguage] = new SiteBlogType();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Tipo de postagem - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'BlogType.form';
        $parameters->pagelink = 'sistema/blogs/types';

        self::formSistemaDefault($parameters);
    }
}
