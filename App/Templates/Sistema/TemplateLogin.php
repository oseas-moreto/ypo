<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use stdClass;

class TemplateLogin extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Login";
        $parameters->class = 'usuarios';
        $parameters->list = new stdClass;
        $parameters->siderbar = false;

        $parameters->page = 'login';
        $parameters->view = 'Login.index';
        $parameters->pagelink = 'sistema/login';

        self::indexSistemaDefault($parameters);
    }
}
