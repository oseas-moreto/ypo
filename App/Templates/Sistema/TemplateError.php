<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\AccessUser;
use App\Models\Entities\Contacts;
use stdClass;

class TemplateError extends Template
{
    public static function error404()
    {
        $parameters = new stdClass;
        $parameters->title = "Error 404";
        $parameters->class = 'usuarios';
        $parameters->list = [];

        $parameters->page = 'layout';
        $parameters->view = 'Error.404';
        $parameters->pagelink = 'sistema/home';

        self::indexSistemaDefault($parameters);
    }

    public static function error500()
    {
        $parameters = new stdClass;
        $parameters->title = "Error 500";
        $parameters->class = 'usuarios';
        $parameters->list = [];

        $parameters->page = 'layout';
        $parameters->view = 'Error.500';
        $parameters->pagelink = 'sistema/home';

        self::indexSistemaDefault($parameters);
    }
}
