<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessMenu;
use App\Templates\Sistema\Modulos\TemplateLoad;
use stdClass;

class TemplateGroup extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Grupos";
        $parameters->class = 'usuarios';
        $parameters->list = AccessGroup::where('status', '<>', 'd')->get();
        $parameters->page = 'layout';
        $parameters->view = 'Groups.index';
        $parameters->pagelink = 'sistema/gruposacesso';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;

        $obj = new AccessGroup;
        $obj->status = 'a';

        $parameters->id = '';
        $parameters->title = "Grupos de Acesso - Cadastro";
        if ($id <> '') {
            $obj = AccessGroup::find($id);
            $parameters->id = $id;
            $parameters->title = "Grupos de Acesso - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $menus = [];
        $menusList = AccessMenu::where('status', '=', 'a')->where('level', '=', 0)->orderBy('order')->get();

        foreach ($menusList as $m) {
            $subMenus = AccessMenu::where('status', '=', 'a')->where('level', '=', $m->idmenu)->orderBy('order')->get();
            $menus['menu'][] = $m;
            $menus['ckecked'][] = TemplateLoad::checkmenugroup($obj->idgroup, $m->idmenu);
            foreach ($subMenus as $key => $sub) {
                $menus[$m->idmenu]['submenu'][] = $sub;
                $menus[$m->idmenu]['ckecked'][] = TemplateLoad::checkmenugroup($obj->idgroup, $sub->idmenu);
            }
        }
       
        $parameters->adds = new stdClass;
        $parameters->adds->menus = [];
        $parameters->adds->menus = $menus;
        $parameters->page = 'layout';
        $parameters->view = 'Groups.form';
        $parameters->pagelink = 'sistema/gruposacesso';

        self::formSistemaDefault($parameters);
    }
}
