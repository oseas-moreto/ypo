<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteTranslate;
use stdClass;

class TemplateTranslate extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Traduções Avulsas";
        $parameters->class = 'usuarios';
        $parameters->list = SiteTranslate::where('status', '<>', 'd')->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Translate.index';
        $parameters->pagelink = 'sistema/translates';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteTranslate;
        $parameters->adds->status = 'a';
        $parameters->adds->id = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();
        
        $parameters->adds->fields = [];

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
            $parameters->adds->fields[$l->idlanguage] = [];
        }

        $parameters->id = '';
        $parameters->title = "Traduções Avulsas - Cadastro";
        
        if (is_numeric($id)) {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteTranslate::where('idtranslate', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idtranslate)) {
                    $parameters->adds->id = $objs[$l->idlanguage]->idtranslate;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->fields[$l->idlanguage] = json_decode($objs[$l->idlanguage]->json, true);
                } else {
                    $objs[$l->idlanguage] = new SiteTranslate();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Traduções Avulsas - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Translate.form';
        $parameters->pagelink = 'sistema/translates';

        self::formSistemaDefault($parameters);
    }
}
