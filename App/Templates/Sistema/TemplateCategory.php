<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteCategory;
use stdClass;

class TemplateCategory extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Categorias auxiliares";
        $parameters->class = 'usuarios';
        $parameters->list = SiteCategory::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Category.index';
        $parameters->pagelink = 'sistema/categories';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteCategory;
        $parameters->adds->status = 'a';
        $parameters->adds->type   = 'cliente';
        $parameters->adds->image  = '';
        $parameters->adds->order  = '';
        $parameters->adds->icon   = '';
        $parameters->adds->id     = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $parameters->adds->types = SiteCategory::getTypes();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Categorias auxiliares - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteCategory::where('idcategory', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idcategory)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->idcategory;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->imagem = $objs[$l->idlanguage]->imagem;
                    $parameters->adds->type   = $objs[$l->idlanguage]->type;
                    $parameters->adds->order  = $objs[$l->idlanguage]->order;
                    $parameters->adds->icon   = $objs[$l->idlanguage]->icon;
                } else {
                    $objs[$l->idlanguage] = new SiteCategory();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Categorias auxiliares - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Category.form';
        $parameters->pagelink = 'sistema/categories';

        self::formSistemaDefault($parameters);
    }
}
