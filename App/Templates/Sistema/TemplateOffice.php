<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\TeamOffice;
use stdClass;

class TemplateOffice extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Cargos/Posição/Escritório";
        $parameters->class = 'usuarios';
        $parameters->list = TeamOffice::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Office.index';
        $parameters->pagelink = 'sistema/office';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new TeamOffice;
        $parameters->adds->status = 'a';
        $parameters->adds->order  = '';
        $parameters->adds->type   = 'cargo';
        $parameters->adds->id     = '';
        $parameters->adds->types  = ['cargo', 'posicao', 'escritorio'];

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Cargos/Posição/Escritório - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = TeamOffice::where('idoffice', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idoffice)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->idoffice;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->order  = $objs[$l->idlanguage]->order;
                    $parameters->adds->type  = $objs[$l->idlanguage]->type;
                } else {
                    $objs[$l->idlanguage] = new TeamOffice();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Cargos/Posição/Escritório - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Office.form';
        $parameters->pagelink = 'sistema/office';

        self::formSistemaDefault($parameters);
    }
}
