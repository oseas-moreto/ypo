<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\ContactDepartment;
use stdClass;

class TemplateDepartment extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Departamentos";
        $parameters->class = 'usuarios';
        $parameters->list = ContactDepartment::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Department.index';
        $parameters->pagelink = 'sistema/departments';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new ContactDepartment;
        $parameters->adds->status = 'a';
        $parameters->adds->order  = '';
        $parameters->adds->id     = '';
        $parameters->adds->email  = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Departamentos - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = ContactDepartment::where('iddepartment', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->iddepartment)) {
                    $parameters->adds->image = $objs[$l->idlanguage]->image;
                    $parameters->adds->id = $objs[$l->idlanguage]->iddepartment;
                    $parameters->adds->status = $objs[$l->idlanguage]->status;
                    $parameters->adds->order  = $objs[$l->idlanguage]->order;
                    $parameters->adds->email  = $objs[$l->idlanguage]->email;
                } else {
                    $objs[$l->idlanguage] = new ContactDepartment();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Departamentos - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Department.form';
        $parameters->pagelink = 'sistema/departments';

        self::formSistemaDefault($parameters);
    }
}
