<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\SiteUnit;
use App\Classes\Funcoes\Generate;
use stdClass;

class TemplateUnit extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Onde Estamos";
        $parameters->class = 'usuarios';
        $parameters->list = SiteUnit::where('status', '<>', 'd')->get();
        $parameters->page = 'layout';
        $parameters->view = 'Unit.index';
        $parameters->pagelink = 'sistema/units';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteUnit;
        $obj->status = 'a';
        $parameters->id = '';
        $parameters->title = "Onde Estamos - Cadastro";
        if ($id != '') {
            $obj = SiteUnit::find($id);
            $parameters->id = $id;
            $parameters->title = "Onde Estamos - Editar";
        }

        $parameters->class = 'cadastrar-anuncio';
        $parameters->obj = $obj;

        $parameters->page      = 'layout';
        $parameters->view      = 'Unit.form';
        $parameters->pagelink  = 'sistema/units';
        $parameters->adds->ufs = Generate::getUfs()->UF;

        self::formSistemaDefault($parameters);
    }
}
