<?php
namespace App\Templates\Sistema;

use App\Core\Template;
use App\Models\Entities\Language;
use App\Models\Entities\SiteProduct;
use App\Models\Entities\SiteProductCategory;
use App\Models\Entities\SiteProductSegment;
use stdClass;

class TemplateProduct extends Template
{
    public static function index()
    {
        $parameters = new stdClass;
        $parameters->title = "Lista de Produtos";
        $parameters->class = 'usuarios';
        $parameters->list = SiteProduct::where('status', '<>', 'd')
        ->where('idlanguage', '=', 1)->get();
        $parameters->page = 'layout';
        $parameters->view = 'Product.Product.index';
        $parameters->pagelink = 'sistema/products';

        self::indexSistemaDefault($parameters);
    }

    public static function form($id = '')
    {
        $parameters = new stdClass;
        $parameters->adds = new stdClass;

        $obj = new SiteProduct;
        $parameters->adds->status      = 'a';
        $parameters->adds->home        = 'n';
        $parameters->adds->image       = '';
        $parameters->adds->order       = '';
        $parameters->adds->icon        = '';
        $parameters->adds->video       = '';
        $parameters->adds->doc         = '';
        $parameters->adds->price       = '';
        $parameters->adds->productcode = '';
        $parameters->adds->photos      = ['name'];
        $parameters->adds->id          = '';
        $parameters->adds->idcategory  = '';
        $parameters->adds->idsegment   = '';

        $parameters->adds->languages = [];
        $parameters->adds->languages = Language::where('status', '=', 'a')->get();

        $parameters->adds->categories = [];
        $parameters->adds->categories = SiteProductCategory::where('status', '=', 'a')->where('type', '=', 'produto')->where('idlanguage', '=', 1)->get();

        $parameters->adds->segments = [];
        $parameters->adds->segments = SiteProductSegment::where('status', '=', 'a')->where('type', '=', 'produto')->where('idlanguage', '=', 1)->get();

        $objs = [];
        foreach ($parameters->adds->languages as $l) {
            $objs[$l->idlanguage] = $obj;
        }

        $parameters->id = '';
        $parameters->title = "Produtos - Cadastro";
        if ($id != '') {
            foreach ($parameters->adds->languages as $l) {
                $objs[$l->idlanguage] = SiteProduct::where('idproduct', '=', $id)->where('idlanguage', '=', $l->idlanguage)->first();

                if (isset($objs[$l->idlanguage]->idproduct)) {
                    $parameters->adds->image       = $objs[$l->idlanguage]->image;
                    $parameters->adds->id          = $objs[$l->idlanguage]->idproduct;
                    $parameters->adds->status      = $objs[$l->idlanguage]->status;
                    $parameters->adds->image       = $objs[$l->idlanguage]->image;
                    $parameters->adds->order       = $objs[$l->idlanguage]->order;
                    $parameters->adds->icon        = $objs[$l->idlanguage]->icon;
                    $parameters->adds->home        = $objs[$l->idlanguage]->home;
                    $parameters->adds->video       = $objs[$l->idlanguage]->video;
                    $parameters->adds->doc         = $objs[$l->idlanguage]->doc;
                    $parameters->adds->price       = $objs[$l->idlanguage]->price;
                    $parameters->adds->productcode = $objs[$l->idlanguage]->productcode;
                    $parameters->adds->photos      = json_decode($objs[$l->idlanguage]->photos,true);
                    $parameters->adds->idcategory  = $objs[$l->idlanguage]->idcategory;
                    $parameters->adds->idsegment   = $objs[$l->idlanguage]->idsegment;
                } else {
                    $objs[$l->idlanguage] = new SiteProduct();
                }
            }

            $parameters->id = $id;
            $parameters->title = "Produtos - Editar";
        }
        $parameters->obj = $objs;

        $parameters->class = 'cadastrar-anuncio';
        $parameters->page = 'layout';
        $parameters->view = 'Product.Product.form';
        $parameters->pagelink = 'sistema/products';

        self::formSistemaDefault($parameters);
    }
}
