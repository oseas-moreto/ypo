<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08/01/2019
 * Time: 15:14
 */

namespace App\Validators;
use App\Models\Entities\AccessUser;


class ValidatorAccessUser
{
    protected $fields = [];

    public function __construct(){
        $this->fields = ['idgroup' => true, 'user' => true, 'email' => true, 'password' => false, 'name' => true, 'status' => true];
    }

    public function validate($data){
        $validations = false;
        $inputs = '';
        $return['success'] = true;

        foreach ($this->fields as $k => $v){
            if($this->fields[$k] == true && $data[$k] == ''){
                $validations = true;
                $inputs .= $this->fields[$k].',';
            }
        }

        if($validations == true){
            $return['success'] = false;
            $return['message'] = "Por favor preencha todos os campos! (".$inputs.")";
            $return['data'] = $data;

            return $return;
        }

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $return['success'] = false;
            $return['message'] = "E-mail <strong>{$data['email']}</strong> inválido!";
            $return['data'] = $data;

            return $return;
        }

        $admin = AccessUser::where('email', '=', $data['email'])->where('status', '=', 'a')->first();
        if($data['id'] == '' && isset($admin->email)){
            $return['success'] = false;
            $return['message'] = "E-mail <strong>{$admin->email}</strong> já cadastrado em nossa base!";
            $return['data'] = $data;

            return $return;
        }

        $admin = AccessUser::where('email', '=', $data['email'])->where('iduser', '<>', $data['id'])->where('status', '=', 'a')->first();
        if(isset($admin->email)){
            $return['success'] = false;
            $return['message'] = "E-mail <strong>{$admin->email}</strong> já cadastrado em nossa base!";
            $return['data'] = $data;

            return $return;
        }

        $admin = AccessUser::where('user', '=', $data['user'])->where('status', '=', 'a')->first();
        if($data['id'] == '' && isset($admin->user)){
            $return['success'] = false;
            $return['message'] = "Usuário <strong>{$admin->user}</strong> já cadastrado em nossa base!";
            $return['data'] = $data;

            return $return;
        }

        $admin = AccessUser::where('user', '=', $data['user'])->where('iduser', '<>', $data['id'])->where('status', '=', 'a')->first();
        if(isset($admin->user)){
            $return['success'] = false;
            $return['message'] = "Usuário <strong>{$admin->user}</strong> já cadastrado em nossa base!";
            $return['data'] = $data;

            return $return;
        }

        return $return;

    }
}
