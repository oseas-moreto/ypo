<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Classes\Funcoes\SessionControl;
use \App\Models\Entities\SiteTranslate;
use App\Services\ServiceTranslate;
use App\Templates\Sistema\TemplateTranslate;

/**
 * Description of Home
 *
 * @author oseas
 */
class Translate extends Common{

    protected $service;
    protected $pagelink = 'sistema/translates';

    public function __construct(){
        $this->service = new ServiceTranslate();
    }

    public function index() {
        SessionControl::start_session();
        $this->logado();
        TemplateTranslate::index();
    }

    public function create(){
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Titulo <strong>{$obj->title}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Titulo <strong>{$obj->title}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateTranslate::form();
    }

    public function update($id){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Titulo <strong>{$obj->title}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Titulo <strong>{$obj->title}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateTranslate::form($id);
    }

    public function destroy($id){
        SessionControl::start_session();
        $obj = SiteTranslate::where('idtranslate', '=', $id)->where('idlanguage', '=', 1)->first();
        $request = $this->service->destroy($id);

        if($request['success']){
            $_SESSION['message'] = "Titulo <strong>{$obj->title}</strong> excluido com sucesso!";
        }else{
            $_SESSION['message'] = "Não foi possivel excluir o Translate <strong>{$obj->title}</strong>";
        }

        print_r(json_encode($request));
        exit();

    }
}
