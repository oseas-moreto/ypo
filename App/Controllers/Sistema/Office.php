<?php

namespace App\Controllers\Sistema;

use App\Services\ServiceOffice;
use App\Templates\Sistema\TemplateOffice;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Sistema\Common;
use \App\Models\Entities\TeamOffice;

/**
 * Description of Home
 *
 * @author oseas
 */
class Office extends Common
{

    protected $service;
    protected $pagelink = 'sistema/office';
    protected $obj;

    public function __construct()
    {   error_reporting(E_ALL);
    ini_set('display_errors', true);
        $this->service = new ServiceOffice();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateOffice::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Registro <strong>{$obj->title}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateOffice::form();
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Registro <strong>{$obj->title}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateOffice::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = TeamOffice::where('iddepartment', '=', $id)->where('idlanguage', '=', 1)->first();
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> excluido com sucesso!";
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o Office <strong>{$obj->title}</strong>";
        }

        print_r(json_encode($request));
        exit();

    }
}
