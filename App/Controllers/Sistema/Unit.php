<?php

namespace App\Controllers\Sistema;

use App\Services\ServiceUnit;
use App\Templates\Sistema\TemplateUnit;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Sistema\Common;

/**
 * Description of Home
 *
 * @author oseas
 */
class Unit extends Common
{

    protected $service;
    protected $pagelink = 'sistema/units';
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct()
    {
        $this->service = new ServiceUnit();

    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateUnit::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $usuarioExiste = 
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        TemplateUnit::form();
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $_POST['iduser'] = $_SESSION['user_id'];
            $_POST['date_update'] = date('Y-m-d H:i:s');
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        TemplateUnit::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = SiteUnit::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o Registro <strong>{$obj->title}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }

    public function uploads(){ 
      if(isset($_FILES['file'.$_GET['inputFile']]['name']) && $_FILES['file'.$_GET['inputFile']]['name'] <> ''){
        $file = [];
        $file['file'] = $_FILES['file'.$_GET['inputFile']];
        
        $responseimg  = $this->upload($file, 'units');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }


}
