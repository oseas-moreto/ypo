<?php

namespace App\Controllers\Sistema;

use App\Templates\Sistema\TemplateError;
use \App\Classes\Funcoes\SessionControl;
use \App\Services\ServiceAccessUser;
use \App\Models\Entities\AccessUser;
use \App\Controllers\Sistema\Common;

/**
 * Description of Error
 *
 * @author oseas
 */
class Error extends Common
{

    protected $pagelink = 'sistema';

    public function __construct(){
        SessionControl::start_session();
        $this->logado();
        $this->view = new \stdClass();
        $this->uservice = new ServiceAccessUser();

        $user = AccessUser::find($_SESSION['user_id']);
        $this->client = $user->client;
    }

    public function error404(){
        SessionControl::start_session();
        $this->logado();
        TemplateError::error404();
    }

    public function error500(){
        SessionControl::start_session();
        $this->logado();
        TemplateError::error500();
    }

}
