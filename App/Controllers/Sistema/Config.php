<?php

namespace App\Controllers\Sistema;

use \App\Core\Controller;


/**
 * Description of Config
 *
 * @author oseas
 */
class Config extends Controller{
    protected $folder = 'Sistema';
    protected $page = 'layout';
    protected $template = 'Default';
    protected $gkey = '';
    protected $header = '';
    protected $url_default_template = URL_DEFAULT_TEMPLATE_SISTEMA;
    protected $url_default_site = URL_DEFAULT_SITE.'/';
    protected $atual = 'd';
    protected $titulo_pagina = 'Dashboard';
    protected $pagelink = '';
    protected $datasite;

}
