<?php

namespace App\Controllers\Sistema;

use App\Services\ServiceModule;
use App\Templates\Sistema\TemplateModule;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Sistema\Common;
use \App\Models\Entities\SiteModule;

/**
 * Description of Home
 *
 * @author oseas
 */
class Module extends Common
{

    protected $service;
    protected $pagelink = 'sistema/modules';
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct()
    {
        $this->service = new ServiceModule();

    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateModule::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();
        error_reporting(E_ALL);
        ini_set('display_errors', true);
        if ($_POST) {
            $_POST['json'] = self::formatarJson($_POST['json'], $_POST['idlanguage']);
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        TemplateModule::form();
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $_POST['json'] = self::formatarJson($_POST['json'], $_POST['idlanguage']);
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        TemplateModule::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = SiteModule::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o Registro <strong>{$obj->title}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }

    public function uploads(){ 
      if(isset($_FILES['file'.$_GET['inputFile']]['name']) && $_FILES['file'.$_GET['inputFile']]['name'] <> ''){
        $file = [];
        $file['file'] = $_FILES['file'.$_GET['inputFile']];
        
        $responseimg  = $this->upload($file, 'modules');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }

    public static function formatarJson($dados, $linguagens){
        $jsonAux   = [];
        $jsonFinal = [];    

        foreach($dados as $json){
            $jsonDecode = json_decode($json, true);
            foreach($jsonDecode['linguagem'] as $key => $linguagem){
                $jsonAux = $linguagem;
                $jsonAux['icon']   = $jsonDecode['icon'];
                $jsonAux['image']  = $jsonDecode['image'];
                $jsonAux['status'] = $jsonDecode['status'];
                if(!isset($jsonFinal[$key])) $jsonFinal[$key] = [];
                $jsonFinal[$key][] = $jsonAux;
            }
        }

        foreach($jsonFinal as $key => $json){
            $jsonFinal[$key] = json_encode($json);
        }

        return array_values($jsonFinal);
    }
}
