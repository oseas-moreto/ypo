<?php

namespace App\Controllers\Sistema;

use App\Services\ServiceBlogAuthor;
use App\Templates\Sistema\TemplateBlogAuthor;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Sistema\Common;
use \App\Models\Entities\SiteBlog;

/**
 * Description of Home
 *
 * @author oseas
 */
class BlogAuthor extends Common
{

    protected $service;
    protected $pagelink = 'sistema/blogs/authors';
    protected $obj;

    public function __construct()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', true);
        $this->service = new ServiceBlogAuthor();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateBlogAuthor::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Registro <strong>{$obj->title}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateBlogAuthor::form();
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Registro <strong>{$obj->title}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateBlogAuthor::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = SiteBlog::where('idauthor', '=', $id)->where('idlanguage', '=', 1)->first();
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> excluido com sucesso!";
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o registro <strong>{$obj->title}</strong>";
        }

        print_r(json_encode($request));
        exit();

    }

    public function uploads(){ 
      if(isset($_FILES['file'.$_GET['inputFile']]['name']) && $_FILES['file'.$_GET['inputFile']]['name'] <> ''){
        $file = [];
        $file['file'] = $_FILES['file'.$_GET['inputFile']];
        
        $responseimg  = $this->upload($file, 'blogs-authors');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }
}
