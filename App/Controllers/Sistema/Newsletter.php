<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Classes\Funcoes\SessionControl;
use App\Templates\Sistema\TemplateNewsletter;

/**
 * Description of Home
 *
 * @author oseas
 */
class Newsletter extends Common
{

  protected $pagelink = 'newsletter';

  public function index()
  {
    SessionControl::start_session();
    $this->logado();
    TemplateNewsletter::index();
  }

  public function export(){
    SessionControl::start_session();
    $this->logado();
  }

}
