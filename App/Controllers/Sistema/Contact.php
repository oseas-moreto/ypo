<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Classes\Funcoes\SessionControl;
use App\Templates\Sistema\TemplateContact;
use App\Services\ServiceContact;
use App\Models\Contacts;

/**
 * Description of Home
 *
 * @author oseas
 */
class Contact extends Common
{

  protected $pagelink = 'contacts';

  public function __construct(){
      $this->service = new ServiceContact();
  }

  public function index()
  {
    SessionControl::start_session();
    $this->logado();
    TemplateContact::index();
  }

  public function view($id){
    SessionControl::start_session();
    $this->logado();
    TemplateContact::form($id);
  }

  public function destroy($id)
  {
      SessionControl::start_session();
      $obj = Contacts::find($id);
      $request = $this->service->destroy($id);

      if ($request['success']) {
          $_SESSION['message'] = "Registro <strong>{$obj->name}</strong> excluido com sucesso!";
      } else {
          $_SESSION['message'] = "Não foi possivel excluir o registro <strong>{$obj->name}</strong>";
      }

      print_r(json_encode($request));
      exit();

  }

}
