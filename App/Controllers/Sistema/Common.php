<?php

namespace App\Controllers\Sistema;

use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessUser;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Sistema\Config;

/**
 * Description of Common
 *
 * @author oseas
 */
class Common extends Config
{

    public function logado()
    {
        SessionControl::start_session();

        if (!isset($_SESSION['user_id'])) {
            $this->redirect('sistema.login');
        }

        $url = explode('/', $_SERVER['REQUEST_URI']);
        $current = isset($url[2]) ? $url[2] : 'index';

        $current_url = isset($url[2]) ? explode('?', $url[2]) : [];

        if (count($current_url) > 0) {
            $atual = $current_url[0];
            $url[2] = $current_url[0];
        }

        $data['user'] = $_SESSION['user'];
        $data['password'] = $_SESSION['password'];

        $admin = AccessUser::where('user', '=', $data['user'])->where('password', '=', $data['password'])->where('status', '=', 'a')->first();

        if (!isset($admin->iduser)) {
            $this->redirect('sistema.login');
        }

        if (isset($url[2]) && $current == 'home' && $url[2] == 'perfil' && $url[3] != $_SESSION['user_id']) {
            $this->redirect('sistema.login');
        }

        if ($current != '' && $current != 'home' && $current != 'index' && $current != 'dashboard' && $current != 'perfil' && $current != 'uploads' && $current != 'data' && $current != 'planning') {
            if (isset($admin->idgroup)) {
                $group = new AccessGroup();
                $menu = $group->checkpage($current, $admin->idgroup);
            }

            if (!isset($menu->idmenu) || $menu->idmenu == '') {
                $this->redirect('sistema.login');
            }
        }

        //$this->view->usertipo = $_SESSION['tipo'];
    }

}
