<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Classes\Funcoes\SessionControl;
use \App\Models\Entities\AccessUser;
use \App\Services\ServiceAccessUser;
use App\Templates\Sistema\TemplateUser;

/**
 * Description of Home
 *
 * @author oseas
 */
class User extends Common
{

    protected $service;
    protected $cservice;
    protected $pagelink = 'sistema/users';
    protected $groups;
    protected $clients;
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct(){
        $this->service = new ServiceAccessUser();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateUser::index();
    }

    public function create(){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $_POST['date_create'] = date('Y-m-d H:i:s');
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateUser::form();
    }

    public function update($id){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateUser::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = AccessUser::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o usuário <strong>{$obj->name}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }

    public function block($id, $s)
    {
        SessionControl::start_session();
        $obj = AccessUser::find($id);
        $obj->status = $s;
        $request = $obj->save();

        if ($request) {
            $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> bloqueado com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel bloqueado o usuário <strong>{$obj->name}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
