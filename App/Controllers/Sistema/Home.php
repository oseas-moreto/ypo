<?php

namespace App\Controllers\Sistema;

use App\Templates\Sistema\TemplateHome;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Sistema\Common;
use \App\Models\Entities\AccessUser;
use \App\Services\ServiceAccessUser;

/**
 * Description of Home
 *
 * @author oseas
 */
class Home extends Common
{

    protected $pagelink = 'sistema';

    public function __construct()
    {
        SessionControl::start_session();
        $this->logado();
        $this->view = new \stdClass();
        $this->uservice = new ServiceAccessUser();

        $user = AccessUser::find($_SESSION['user_id']);
        $this->client = $user->client;
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateHome::index();
    }

    public function perfil($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->uservice->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Perfil <strong>{$obj->name}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink . '/perfil/' . $id;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateHome::perfil($id);
    }

    public function uploads()
    {
        SessionControl::start_session();
        if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
            $responseimg = $this->upload($_FILES, 'usuarios');

            if ($responseimg['success']) {
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['image'] = $responseimg['image'];
                $return['response']['status'] = 1;
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
                $return['response']['status'] = 0;
            }

            $return['response']['mensagem'] = $responseimg['message'];
            print_r(json_encode($return));
            exit();
        }
    }

    public function trocatipo(){
        SessionControl::start_session();
        $_SESSION['tipo'] = $_GET['tipo'];
        $response = ['tipo' => $_GET['tipo']];
        print_r(json_encode($response));
        exit();

    }

    public function uploadgeneric($path, $type, $name)
    {
        SessionControl::start_session();
        //REMOVE O DELIMITADOR DE PASTAS E TROCA POR BARRA 
        $pathReal = str_replace('|', '/', $path);
        //VERIFICA SE VAI SUBIR NA PASTA DE THUMBS OU NÂO
        $pathReal = $_SESSION['tipo'] == 'r' ? str_replace('/thumbs', '', $pathReal) : $pathReal;

        if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != '') {
            $_FILES['file'] = $_FILES[$name];
            $responseimg = $this->upload($_FILES, $pathReal, false);

            if ($responseimg['success']) {
                $return['response']['classe']   = 'alert-success';
                $return['response']['result']   = 'success';
                $return['response']['image']    = $responseimg['image'];
                $return['response']['status']   = 1;
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['classe']   = 'alert-danger';
                $return['response']['result']   = 'error';
                $return['response']['redirect'] = '';
                $return['response']['status']   = 0;
            }

            $return['response']['mensagem'] = $responseimg['message'];
            print_r(json_encode($return));
            exit();
        }
    }

    public function apagarImagem(){
        $imagem    = $_GET['file'];
        $_GET['path'] = str_replace('|', '/', $_GET['path']);
        $pathThumb = $_SERVER['DOCUMENT_ROOT'] . '/images/' .$_GET['path'];
        $path      = str_replace('/thumbs', '', $pathThumb);

        if(file_exists($pathThumb.'/'.$imagem)){
            chmod ($pathThumb, 777);
            unlink($pathThumb.'/'.$imagem);
        }

        if(file_exists($path.'/'.$imagem)){
            chmod ($path, 777);
            unlink($path.'/'.$imagem);
        }

        $response = ['mensagem' => 'Imagem removida com sucesso!'];
        print_r(json_encode($response)); exit();
    }

}
