<?php

namespace App\Controllers\Ypo\Institucional;

use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateTurismo;
use \App\Classes\Funcoes\SessionControl;

/**
 * Description of Turismo
 *
 * @author oseas¹
 */
class Turismo extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
        $this->logadoInterno();
    }

    public function index()
    {
        SessionControl::start_session();
        TemplateTurismo::index();
    }
}
