<?php

namespace App\Controllers\Ypo\Institucional;

use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateClassificados;
use \App\Classes\Funcoes\SessionControl;

/**
 * Description of Classificados
 *
 * @author oseas¹
 */
class Classificados extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
        $this->logadoInterno();
    }

    public function index($id = 0, $text = '', $page = 1)
    {
        SessionControl::start_session();
        TemplateClassificados::index($id, $text, $page);
    }

    public function post($id = 0, $text = '')
    {
        SessionControl::start_session();
        TemplateClassificados::post($id);
    }
}
