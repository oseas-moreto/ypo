<?php

namespace App\Controllers\Ypo\Institucional;

use App\Classes\Mail;
use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateContato;
use \App\Classes\Funcoes\SessionControl;

/**
 * Description of Contato
 *
 * @author oseas¹
 */
class Contato extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
        $this->logadoInterno();
    }

    public function index()
    {
        SessionControl::start_session();
        TemplateContato::index();
    }

    public function index2()
    {
        SessionControl::start_session();
        TemplateContato::index2();
    }
}
