<?php

namespace App\Controllers\Ypo\Institucional;

use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateParcerias;
use App\Models\Entities\SiteEmpresa;
use \App\Classes\Funcoes\SessionControl;
use App\Services\ServiceSolicitacao;
use App\Templates\Ypo\Modulos\TemplateLoad;
use App\Classes\Mail;

/**
 * Description of Parcerias
 *
 * @author oseas¹
 */
class Parcerias extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
    }

    public function index($id = 0, $text = '')
    {
        SessionControl::start_session();
        $this->logadoInterno();
        TemplateParcerias::index($id);
    }

    public function post($id = 0, $text = '')
    {
        if(!isset($_GET['token']) or $_GET['token'] <> md5('meuPastelÉMaisBaratoRuaAbacate675677654845876@@@098jjhhjhjjhdfhfh'+$id)){
            $this->logadoInterno();
        }
        SessionControl::start_session();
        TemplateParcerias::post($id);
    }

    public function solicitar(){
        SessionControl::start_session();
        $request = (new ServiceSolicitacao)->register($_POST);

        $dadosSolicitacao = \json_decode($request['data']->dados);

        $empresa = SiteEmpresa::find($_POST['idempresa']);

        $info = TemplateLoad::info();

        $email = [];
        $email[] = array('name' => 'YPO', 'email' => $empresa->emailContato);

        $mail = new Mail(
            $info->datasite->emailuser, 
            $info->datasite->emailpassword, 
            $info->datasite->smtp, 
            $info->datasite->port, 
            '#01253d',
            utf8_decode($info->datasite->title),
            URL_DEFAULT_SITE.'/templates/ypo/images/Logo.jpg');
        $mail->fromname = $info->datasite->title;
        $mail->recipient = $email;
        $mail->subject = addslashes(utf8_decode($info->datasite->title.' | Solicitação de desconto!'));
        $mail->body = utf8_decode('
                      <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; padding-bottom:40px; border-collapse:collapse" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 24px; line-height: 1.5; text-align: center;">
                        <strong>Solicitação de desconto YPO</strong></div>
                        </td>
                        </tr>
                        <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                        <b>Dados:</b><br><br>
                        <b>Nome:</b> ' . $dadosSolicitacao->nome . '<br>
                        <b>E-mail:</b> ' . $dadosSolicitacao->email . '<br>
                        <b>Empresa:</b> ' . $dadosSolicitacao->empresa . '<br>
                        <b>Tel/Whatsapp:</b> ' . $dadosSolicitacao->whatsapp . '<br>
                        <b>Mensagem:</b> ' . $dadosSolicitacao->mensagem . '<br>
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse;" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center; margin-top: 10px">

                        </div>
                        </td>
                        </tr>
                ');

        $mail->altbody = utf8_decode(utf8_decode($info->datasite->title).' | Solicitação de desconto!');

        $send = $mail->send();

        if ($send) {
            $return['response']['mensagem'] = "Solicitação enviada com sucesso!";
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        } else {
            $return['response']['mensagem'] = 'Não foi possivel enviar a solicitação.';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        }
    }
}
