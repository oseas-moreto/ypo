<?php

namespace App\Controllers\Ypo\Institucional;

use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateRetreats;
use \App\Classes\Funcoes\SessionControl;

/**
 * Description of Retreats
 *
 * @author oseas¹
 */
class Retreats extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
        $this->logadoInterno();
    }

    public function index($id = 0, $text = '')
    {
        SessionControl::start_session();
        TemplateRetreats::index($id);
    }

    public function newIndex()
    {
        SessionControl::start_session();
        TemplateRetreats::newIndex();
    }

    public function post($id = 0, $text = '')
    {
        SessionControl::start_session();
        TemplateRetreats::post($id);
    }
}
