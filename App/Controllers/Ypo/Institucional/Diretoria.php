<?php

namespace App\Controllers\Ypo\Institucional;

use App\Classes\Mail;
use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateDiretoria;
use \App\Classes\Funcoes\SessionControl;

/**
 * Description of Diretoria
 *
 * @author oseas¹
 */
class Diretoria extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
        $this->logadoInterno();
    }

    public function index()
    {
        SessionControl::start_session();
        TemplateDiretoria::index();
    }
}
