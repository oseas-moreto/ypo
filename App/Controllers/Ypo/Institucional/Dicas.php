<?php

namespace App\Controllers\Ypo\Institucional;

use App\Services\ServiceDica;
use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateDicas;
use \App\Classes\Funcoes\SessionControl;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteDica;

/**
 * Description of Dicas
 *
 * @author oseas¹
 */
class Dicas extends Common
{

    protected $pagelink = '';

    protected $service;

    public function __construct()
    {
        parent::__construct();
        $this->logadoInterno();
        $this->service = new ServiceDica();
    }

    public function index($id = 0, $text = '')
    {
        SessionControl::start_session();
        TemplateDicas::index($id);
    }

    public function importardicas()
    {
        SessionControl::start_session();
        $input = [];

        foreach($input as $key => $value){
            if($key == 0) continue;
            if(!isset($value[0]) || 
            !isset($value[1]) || 
            !isset($value[2]) || 
            !isset($value[3]) || 
            !isset($value[4]) || 
            !isset($value[5]) || 
            !isset($value[6]) || 
            !isset($value[7])|| 
            !isset($value[8])) continue;

            if(!strlen($value[0])) continue;
            if(!strlen($value[2]) && !strlen($value[3])) continue;

            $dica               = new SiteDica;
            $dica->idcategoria  = $value[0];
            if(isset($value[1]) && strlen($value[1])){
                $subcategoria = SiteCategory::where('title',$value[1])->first();
                $dica->idcategoria2 = isset($subcategoria->idcategory) ? $subcategoria->idcategory : 0;
            }
            
            $dica->nome         = $value[2] ?? null;
            $dica->empresa      = $value[3] ?? null;
            $dica->texto        = $value[4] ?? null;
            $dica->whatsapp     = $value[5] ?? null;
            $dica->emailContato = $value[6] ?? null;
            $dica->site         = $value[7] ?? null;
            $dica->endereco     = $value[8] ?? null;
            $dica->save();
            echo "<pre>"; print_r($dica->iddica); echo "</pre>";
        }
    }

    public function enviarDica()
    {
        SessionControl::start_session();
        if ($_POST) {
            $_POST['ativo']          = 's';
            $_POST['idcategoria']    = $_POST['idcategory'];
            $_POST['iduser']         = $_SESSION['user_id'];
            $_POST['palavrasChaves'] = '';
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Dica <strong>{$obj->nome}</strong> enviada com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Dica <strong>{$obj->nome}</strong> enviada com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/listagem-dicas';

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }
    }

    public function loadPorItens(){
        SessionControl::start_session();
        error_reporting(E_ALL);
        ini_set('display_errors', true);

        if(!isset($_GET['item'])){
            echo json_encode([]);
            exit;
        } 

        $request = $_GET;

        switch ($request['item']) {
            case 'estado': 
                $registros = SiteDica::where('ativo', '=', 's')
                ->when($request['categoria'] > 0, function ($q)  use ($request) {
                    return $q->whereRaw('( idcategoria = '.$request['categoria'].' OR idcategoria2 = '.$request['categoria'].' )');
                })
                ->where('estado', '!=', '')
                ->when(!empty($request['pais']), function ($q)  use ($request) {
                    return $q->where('pais', $request['pais']);
                })
                ->select('estado')->groupBy('estado')
                ->orderBy('estado')
                ->get();
                
                $requestItens = [];

                if(count($registros) == 0){
                    $registros = SiteDica::where('ativo', '=', 's')
                    ->when($request['categoria'] > 0, function ($q)  use ($request) {
                        return $q->whereRaw('( idcategoria = '.$request['categoria'].' OR idcategoria2 = '.$request['categoria'].' )');
                    })
                    ->when(isset($request['estado']), function ($q)  use ($request) {
                        return $q->where('estado', $request['estado']);
                    })
                    ->when(isset($request['pais']), function ($q)  use ($request) {
                        return $q->where('pais', $request['pais']);
                    })
                    ->where('cidade', '!=', '')
                    ->select('cidade')->groupBy('cidade')
                    ->orderBy('cidade')
                    ->get();
            
                    foreach($registros as $obj){
                        $requestItens[] = [
                            'title' => ucwords(strtolower($obj->cidade)),
                            'id'    => $obj->cidade
                        ];
                    }

                    $retorno = [
                        'item'      => 'cidade',
                        'ocultar'   => 'estado',
                        'registros' => $requestItens
                    ];
                
                    print_r(json_encode($retorno));
                    exit();
                }
        
                foreach($registros as $obj){
                    $requestItens[] = [
                        'title' => $obj->estado,
                        'id'    => $obj->estado
                    ];
                }

                $retorno = [
                    'item'      => 'estado',
                    'ocultar'   => '',
                    'registros' => $requestItens
                ];
            
                print_r(json_encode($retorno));
                exit();
            break;
            case 'cidade':

                $registros = SiteDica::where('ativo', '=', 's')
                ->when($request['categoria'] > 0, function ($q)  use ($request) {
                    return $q->whereRaw('( idcategoria = '.$request['categoria'].' OR idcategoria2 = '.$request['categoria'].' )');
                })
                ->when(isset($request['estado']), function ($q)  use ($request) {
                    return $q->where('estado', $request['estado']);
                })
                ->when(isset($request['pais']), function ($q)  use ($request) {
                    return $q->where('pais', $request['pais']);
                })
                ->where('cidade', '!=', '')
                ->select('cidade')->groupBy('cidade')
                ->orderBy('cidade')
                ->get();
                
                $requestItens = [];
        
                foreach($registros as $obj){
                    $requestItens[] = [
                        'title' => ucwords(strtolower($obj->cidade)),
                        'id'    => $obj->cidade
                    ];
                }

                $retorno = [
                    'item'      => 'cidade',
                    'ocultar'   => '',
                    'registros' => $requestItens
                ];
            
                print_r(json_encode($retorno));
                exit();
            break;
            default:
                echo json_encode([]);
                exit;
            break;
        }

    }
}
