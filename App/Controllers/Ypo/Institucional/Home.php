<?php

namespace App\Controllers\Ypo\Institucional;

use App\Classes\Mail;
use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateHome;
use \App\Classes\Funcoes\SessionControl;

/**
 * Description of Home
 *
 * @author oseas¹
 */
class Home extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
        $this->logadoInterno();
    }

    public function index()
    {
        SessionControl::start_session();
        TemplateHome::index();
    }

    public function buscar($busca = '')
    {
        SessionControl::start_session();
        TemplateHome::buscar($busca);
    }

    public function institucional($hash){
        SessionControl::start_session();
        TemplateHome::institucional($hash);
    }

}
