<?php

namespace App\Controllers\Ypo\Institucional;

use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Institucional\TemplateVideoteca;
use \App\Classes\Funcoes\SessionControl;

/**
 * Description of Videoteca
 *
 * @author oseas¹
 */
class Videoteca extends Common
{

    protected $pagelink = '';

    public function __construct(){
        parent::__construct();
        $this->logadoInterno();
    }

    public function index($id = 0, $text = '', $page = 1)
    {
        SessionControl::start_session();
        TemplateVideoteca::index($id, $text, $page);
    }

    public function post($id = 0, $text = '')
    {
        SessionControl::start_session();
        TemplateVideoteca::post($id);
    }
}
