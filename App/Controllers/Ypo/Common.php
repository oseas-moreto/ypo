<?php

namespace App\Controllers\Ypo;

use \App\Controllers\Ypo\Config;
use App\Classes\Funcoes\SessionControl;

use App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessSpouse;



/**
 * Description of Common
 *
 * @author oseas
 */
class Common extends Config{

    public function __construct(){
        parent::__construct();
        SessionControl::start_session();
        
        if(!isset($_SESSION['name'])) {
            $_SESSION['name']  = 'Minha conta';
            $_SESSION['class'] = '';
            $_SESSION['link']  = '/login';
        }

    }

    public function logadoInterno()
    {
        SessionControl::start_session();

        $url   = explode('/', $_SERVER['REQUEST_URI']);
        $login = (isset($url[1]) && $url[1] == 'login');

        if (!isset($_SESSION['user_id']) && !$login) {
            $this->redirect('login');
        }
    }

    public function logado()
    {
        SessionControl::start_session();
        
        if (!isset($_SESSION['user_id'])) {
            $this->redirect('login');
        }

        $url = explode('/', $_SERVER['REQUEST_URI']);
        $current = isset($url[1]) ? $url[1] : 'index';

        $current_url = isset($url[1]) ? explode('?', $url[1]) : [];

        if (count($current_url) > 0) {
            $atual = $current_url[0];
            $url[1] = $current_url[0];
        }

        $data['user'] = $_SESSION['user'];
        $data['password'] = $_SESSION['password'];

        $admin  = AccessUser::where('user', '=', $data['user'])->where('password', '=', $data['password'])->where('status', '=', 'a')->first();

        $spouse = AccessSpouse::where('user', '=', $data['user'])->where('password', '=', $data['password'])->first(); 
  
        if (!isset($admin->iduser) && !isset($spouse->idspouse)) {
            $this->redirect('login');
        }

        if (isset($url[1]) && $current == 'home' && $url[1] == 'perfil' && $url[2] != $_SESSION['user_id']) {
            $this->redirect('login');
        }

        if ($current != '' && $current != 'home' && $current != 'index' && $current != 'dashboard' && $current != 'perfil' && $current != 'uploads' && $current != 'data' && $current != 'planning') {
            
            if (isset($_SESSION['idgroup'])) {
                $group = new AccessGroup();
                $menu = $group->checkpage($current, $_SESSION['idgroup']);
            }

            if (!isset($menu->idmenu) || $menu->idmenu == '') {
                $this->redirect('login');
            }
        }
    }

}
