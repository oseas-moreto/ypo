<?php

namespace App\Controllers\Ypo;

use \App\Core\Controller;

/**
 * Description of Config
 *
 * @author oseas
 */
class Config extends Controller
{
    protected $folder = 'Ypo';
    protected $page = 'layout';
    protected $template = 'Default';
    protected $gkey = MAPSKEY;
    protected $header = '';
    protected $url_default_template = URL_DEFAULT_TEMPLATE_SITE;
    protected $url_default_site = URL_DEFAULT_SITE . '/';
    protected $atual = 'd';
    protected $titulo_pagina = 'Home';
    protected $pagelink = '';
    protected $datasite;
}
