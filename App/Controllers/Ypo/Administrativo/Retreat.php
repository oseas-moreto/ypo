<?php

namespace App\Controllers\Ypo\Administrativo;

use App\Services\ServiceRetreat;
use App\Templates\Ypo\Administrativo\TemplateRetreat;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Ypo\Common;
use \App\Models\Entities\SiteRetreat;

/**
 * Description of Home
 *
 * @author oseas
 */
class Retreat extends Common
{

    protected $service;
    protected $pagelink = 'retreats';
    protected $obj;

    public function __construct()
    {
        $this->service = new ServiceRetreat();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateRetreat::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Retreat <strong>{$obj->nome}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Retreat <strong>{$obj->nome}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateRetreat::form();
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Retreat <strong>{$obj->nome}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Retreat <strong>{$obj->nome}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateRetreat::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = SiteRetreat::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Retreat <strong>{$obj->nome}</strong> excluido com sucesso!";
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o Retreat <strong>{$obj->nome}</strong>";
        }

        print_r(json_encode($request));
        exit();

    }

    public function uploads(){ 
      if(isset($_FILES['file'.$_GET['inputFile']]['name']) && $_FILES['file'.$_GET['inputFile']]['name'] <> ''){
        $file = [];
        $file['file'] = $_FILES['file'.$_GET['inputFile']];
        
        $responseimg  = $this->upload($file, 'retreat');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }

    public function uploadFile(){ 
        // Count # of uploaded files in array
        $total    = count($_FILES['fileinputarquivo']['name']);
        $response = [];
        $html     = '';
        // Loop through each file
        for( $i=0 ; $i < $total ; $i++ ) {

            //Get the temp file path
            $tmpFilePath = $_FILES['fileinputarquivo']['tmp_name'][$i];

            //Make sure we have a file path
            if ($tmpFilePath != ""){
                $caminho = $_SERVER['DOCUMENT_ROOT'] . '/arquivos/' . $_GET['folder'];
                //Setup our new file path
                $newFilePath = $caminho."/" . $_FILES['fileinputarquivo']['name'][$i];
                
                if (!is_dir($caminho)) {
                    mkdir($caminho, 0777);
                }

                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                    $response['success'] = true;
                    $response['message'] = 'Upload feito com sucesso!';
                    $html               .= '<p><a target="_blank" href="/arquivos/'.$_GET['folder'].'/'. $_FILES['fileinputarquivo']['name'][$i].'">Ver arquivo ('.$_FILES['fileinputarquivo']['name'][$i].')</a></p>';
                }else{
                    $response['success'] = false;
                    $response['message'] = 'Ocorreu algum erro ao subir os arquivos!';
                }
            }
        }

        $response['html'] = self::loadFiles($caminho, $_GET['folder']);
        echo json_encode($response);
        exit;
    }

    public static function loadFiles($caminho, $folder){
        $html = '';
        // Sort in descending order
        $files = scandir($caminho,1);
        for($i=0; $i<count($files); $i++){
            if($files[$i] == '.' or $files[$i] == '..') continue;
            $html .= '<p><a target="_blank" href="/arquivos/'.$folder.'/'. $files[$i].'">Ver arquivo ('.$files[$i].')</a><a href="#." class="text-danger" onclick=\'deleteFile("'.$files[$i].'","'.$folder.'", this)\'><i class="fa fa-trash"></i></a></p>';
        }

        return $html;
    }

    public function deleteFile(){
        $return = [];
        if(!isset($_POST)){
            $return['response']['mensagem'] = 'Erro de requisição';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
            echo json_encode($return);
            exit;
        }

        $caminho = $_SERVER['DOCUMENT_ROOT'] . '/arquivos/' . $_POST['folder'];
        
        if(unlink($caminho.'/'.$_POST['file'])){
            $return['response']['mensagem'] = 'Arquivo deletado com sucesso';
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
            echo json_encode($return);
            exit;
        }


        $return['response']['mensagem'] = 'Erro de requisição';
        $return['response']['classe'] = 'alert-danger';
        $return['response']['result'] = 'error';
        $return['response']['redirect'] = '';
        $return['response']['status'] = 0;
        echo json_encode($return);
        exit;
    }

}
