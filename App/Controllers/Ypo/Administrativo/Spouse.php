<?php

namespace App\Controllers\Ypo\Administrativo;

use \App\Controllers\Ypo\Common;
use \App\Classes\Funcoes\SessionControl;
use \App\Models\Entities\AccessSpouse;
use \App\Models\Entities\AccessUser;
use \App\Services\ServiceAccessSpouse;
use App\Templates\Ypo\Administrativo\TemplateSpouse;

/**
 * Description of Home
 *
 * @author oseas
 */
class Spouse extends Common
{

    protected $service;
    protected $cservice;
    protected $pagelink = 'cadastro-spouse';
    protected $groups;
    protected $clients;
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct(){
        $this->service = new ServiceAccessSpouse();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateSpouse::index();
    }

    public function create(){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $this->uploadFoto();  
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Spouse <strong>{$obj->nome}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink.'/index';

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateSpouse::form();
    }

    public function update($id){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $this->uploadFoto();  
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Spouse <strong>{$obj->nome}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink.'/index';

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }
        
        TemplateSpouse::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = AccessSpouse::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Spouse <strong>{$obj->nome}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o spouse <strong>{$obj->nome}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
    
    public function uploadFoto(){
        $file = [];
        $_POST['image'] = '';
        if(isset($_FILES['image'])){
            $file['file']    = $_FILES['image'];
            $responseimg1    = $this->upload($file, 'spouses');
            $_POST['image'] = isset($responseimg1['image']) ? $responseimg1['image'] : '';
        }
    }

    public function block($id, $s)
    {
        SessionControl::start_session();
        $obj = AccessSpouse::find($id);
        $obj->status = $s;
        $request = $obj->save();

        if ($request) {
            $_SESSION['message'] = "Spouse <strong>{$obj->nome}</strong> bloqueado com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel bloqueado o spouse <strong>{$obj->nome}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }

    public function importToJsonSpouse(){
        return false;
        set_time_limit(10000000);
        $dados = \file_get_contents($_SERVER['DOCUMENT_ROOT'].'/spouse.json');

        $dados = json_decode($dados, true);
        foreach($dados as $request){
            $obj = new AccessSpouse;

            if(strlen($request['cpf'])){
                $cpf = preg_replace( '/[^0-9]/', '', $request['cpf']);
                $objCPFExiste = AccessSpouse::where('user', $cpf)->first();
                if($objCPFExiste instanceof AccessSpouse) continue;
            }

            if(!strlen($request['nome'])) continue;
            if(strlen($request['nome']) == 1) continue;

            $nascimento = date('Y-m-d', strtotime($request['nascimento']));
            echo $nascimento;
            $request['parentesco'] = \str_replace('CÔNJUGE', 'conjuge', $request['parentesco']);

            $obj->capitulo   = $request['capitulo'];
            $obj->iduser     = $request['iduser'];
            $obj->nome       = $request['nome'];
            $obj->empresa    = $request['empresa'];
            $obj->ramoatv    = $request['ramoatv'];
            $obj->phone      = $request['phone'];
            $obj->email      = $request['email'];
            $obj->nascimento = date('Y-m-d', strtotime($request['nascimento']));
            $obj->cpf        = $request['cpf'];
            $obj->parentesco = strtolower($request['parentesco']);
            $obj->user       = trim(preg_replace( '/[^0-9]/', '', $request['cpf']));
            if(strlen($request['cpf'])) $obj->password =  md5(preg_replace( '/[^0-9]/', '', $request['cpf']));
            $obj->save();

            echo "<pre>"; print_r($obj->idspouse.' - '.$obj->nome); echo "</pre>";
        }
    }
}
