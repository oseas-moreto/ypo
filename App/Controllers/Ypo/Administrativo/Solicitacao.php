<?php

namespace App\Controllers\Ypo\Administrativo;

use \App\Controllers\Ypo\Common;
use \App\Classes\Funcoes\SessionControl;
use App\Templates\Ypo\Administrativo\TemplateSolicitacao;
use App\Services\ServiceSolicitacao;
use App\Models\SiteSolicitacaoDesconto;

/**
 * Description of Home
 *
 * @author oseas
 */
class Solicitacao extends Common
{

  protected $pagelink = 'solicitacoes-desconto';

  public function __construct(){
      $this->service = new ServiceSolicitacao();
  }

  public function index()
  {
    SessionControl::start_session();
    $this->logado();
    TemplateSolicitacao::index();
  }

  public function view($id){
    SessionControl::start_session();
    $this->logado();
    TemplateSolicitacao::form($id);
  }

  public function destroy($id)
  {
      SessionControl::start_session();
      $obj = SiteSolicitacaoDesconto::find($id);
      $request = $this->service->destroy($id);

      if ($request['success']) {
          $_SESSION['message'] = "Registro excluido com sucesso!";
      } else {
          $_SESSION['message'] = "Não foi possivel excluir o registro.";
      }

      print_r(json_encode($request));
      exit();

  }

}
