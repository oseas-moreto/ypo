<?php

namespace App\Controllers\Ypo\Administrativo;

use \App\Controllers\Ypo\Common;
use \App\Classes\Funcoes\SessionControl;
use \App\Models\Entities\Configuration;
use App\Services\ServiceConfiguration;
use App\Templates\Ypo\Administrativo\TemplateSettings;

/**
 * Description of Home
 *
 * @author oseas
 */
class Settings extends Common{

    protected $service;
    protected $pagelink = 'settings';
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct(){
        SessionControl::start_session();
        $this->view = new \stdClass();
        $this->service = new ServiceConfiguration();
        $this->obj = new Configuration();
        $this->obj->status = 'a';

    }

    public function index() {

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Configurações Atualizadas com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        TemplateSettings::index(1);
    }

    public function uploads(){   
      if(isset($_FILES['file'.$_GET['inputFile']]['name']) && $_FILES['file'.$_GET['inputFile']]['name'] <> ''){
        $file = [];
        $file['file'] = $_FILES['file'.$_GET['inputFile']];
        
        $responseimg  = $this->upload($file, 'settings');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }

}
