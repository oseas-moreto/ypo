<?php

namespace App\Controllers\Ypo\Administrativo;

use \App\Controllers\Ypo\Common;
use \App\Classes\Funcoes\SessionControl;
use App\Models\Entities\AccessMenu;
use App\Services\ServiceMenu;
use App\Templates\Ypo\Administrativo\TemplateMenu;

/**
 * Description of Home
 *
 * @author oseas
 */
class Menus extends Common
{

    protected $service;
    protected $pagelink = 'menus';

    public function __construct(){
        $this->service = new ServiceMenu();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateMenu::index();
    }

    public function create(){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Menu <strong>{$obj->titulo}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Menu <strong>{$obj->titulo}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateMenu::form();
    }

    public function update($id){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Menu <strong>{$obj->titulo}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Menu <strong>{$obj->titulo}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateMenu::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = AccessMenu::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Página <strong>{$obj->titulo}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir a página <strong>{$obj->titulo}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
