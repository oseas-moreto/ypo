<?php

namespace App\Controllers\Ypo\Administrativo;

use App\Services\ServiceSeo;
use App\Templates\Ypo\Administrativo\TemplateSeo;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Ypo\Common;

/**
 * Description of Home
 *
 * @author oseas
 */
class Seo extends Common
{

    protected $service;
    protected $pagelink = 'seo';

    public function __construct(){
        $this->service = new ServiceSeo();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Seo Atualizado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        TemplateSeo::index(1);
    }
}
