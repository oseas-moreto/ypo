<?php

namespace App\Controllers\Ypo\Administrativo;

use \App\Controllers\Ypo\Common;
use \App\Classes\Funcoes\SessionControl;
use App\Models\Entities\SiteMenu;
use App\Services\ServiceMenuSite;
use App\Templates\Ypo\Administrativo\TemplateMenuSite;

/**
 * Description of Home
 *
 * @author oseas
 */
class MenuSite extends Common
{

    protected $service;
    protected $pagelink = 'menu-site';

    public function __construct(){
        $this->service = new ServiceMenuSite();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateMenuSite::index();
    }

    public function create(){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "MenuSite <strong>{$obj->title}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "MenuSite <strong>{$obj->title}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateMenuSite::form();
    }

    public function update($id){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "MenuSite <strong>{$obj->title}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "MenuSite <strong>{$obj->title}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateMenuSite::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = SiteMenu::where('idmenu', '=', $id)->first();
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Página <strong>{$obj->title}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir a página <strong>{$obj->title}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
