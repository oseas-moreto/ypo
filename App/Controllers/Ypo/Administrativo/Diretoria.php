<?php

namespace App\Controllers\Ypo\Administrativo;

use \App\Controllers\Ypo\Common;
use \App\Classes\Funcoes\SessionControl;
use \App\Models\Entities\AccessDiretoria;
use \App\Models\Entities\AccessUser;
use App\Models\Entities\AccessSpouse;
use \App\Services\ServiceAccessDiretoria;
use App\Templates\Ypo\Administrativo\TemplateDiretoria;

/**
 * Description of Home
 *
 * @author oseas
 */
class Diretoria extends Common
{

    protected $service;
    protected $cservice;
    protected $pagelink = 'cadastro-diretoria';
    protected $groups;
    protected $clients;
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct(){
        $this->service = new ServiceAccessDiretoria();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateDiretoria::index();
    }

    public function create(){

        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $_POST['iduser']   = 0;
            $_POST['idspouse'] = 0;
            $capitulo  = AccessDiretoria::where('capitulo', $_POST['capitulo'])->first();
            
            $usuario   = AccessUser::where('name', $_POST['usuario'])->first();

            if($usuario instanceof AccessUser) $spouse  = new AccessSpouse;
            if(!$usuario instanceof AccessUser){
                $spouse = AccessSpouse::where('nome', $_POST['spouse'])->first();
                $usuario = new AccessUser;
            }
            

            if(!$usuario instanceof AccessUser && !$spouse instanceof AccessSpouse){
                $return['response']['mensagem'] = 'Insira ou um usuário ou um spouse.';
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }

            if($usuario instanceof AccessUser){ 
                $_POST['iduser']   = $usuario->iduser;
            }
            if($spouse instanceof AccessSpouse){
                $_POST['idspouse'] = $spouse->idspouse;
            }

            if($capitulo instanceof AccessDiretoria){
                $_POST['ordemCapitulo'] = $capitulo->ordemCapitulo;
            }

            $this->uploadFoto();

            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Diretoria <strong>{$obj->fantasia}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink.'/index?iduser='.$obj->iduser;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateDiretoria::form();
    }

    public function update($id){

        SessionControl::start_session();
        $this->logado();
        if ($_POST) {
            $_POST['iduser']   = 0;
            $_POST['idspouse'] = 0;
            $usuario = AccessUser::where('name', $_POST['usuario'])->first();
            $spouse  = AccessSpouse::where('nome', $_POST['spouse'])->first();

            if(!$usuario instanceof AccessUser && !$spouse instanceof AccessSpouse){
                $return['response']['mensagem'] = 'Insira ou um usuário ou um spouse.';
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }

            if($usuario instanceof AccessUser){ 
                $_POST['iduser']   = $usuario->iduser;
            }

            if($spouse instanceof AccessSpouse){
                $_POST['idspouse'] = $spouse->idspouse;
            }

            $this->uploadFoto();
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Diretoria <strong>{$obj->fantasia}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['imagem'] = $_POST['imagem'];
                $return['response']['redirect'] = '/' . $this->pagelink.'/index?iduser='.$obj->iduser;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }
    }

    public function atualizarOrdemCapitulo(){
        if(!is_numeric($_POST['ordemCapitulo'])){
            $return['response']['mensagem'] = 'A ordem precisa ser numérica.';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        }
        if(!strlen($_POST['capitulo'])){
            $return['response']['mensagem'] = 'Selecione um capitulo válido.';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        }

        $obsDiretoria = AccessDiretoria::where('capitulo', $_POST['capitulo'])->get();
        
        foreach ($obsDiretoria as $key => $obDiretoria) {
            $obDiretoria->ordemCapitulo = $_POST['ordemCapitulo'];
            $obDiretoria->save();
        }

        $return['response']['mensagem'] = 'Ordem alterada com sucesso.';
        $return['response']['classe']   = 'alert-success';
        $return['response']['result']   = 'success';
        $return['response']['imagem']   = '';
        $return['response']['redirect'] = '';

        print_r(json_encode($return));
        exit(); 
    }
    
    public function uploadFoto(){
        $file = [];
        $_POST['imagem'] = '';
        if(isset($_FILES['imagem'])){
            $file['file']    = $_FILES['imagem'];
            $responseimg1    = $this->upload($file, 'diretor');
            $_POST['imagem'] = isset($responseimg1['image']) ? $responseimg1['image'] : '';
        }
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = AccessDiretoria::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Diretoria <strong>{$obj->fantasia}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o diretoria <strong>{$obj->fantasia}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
