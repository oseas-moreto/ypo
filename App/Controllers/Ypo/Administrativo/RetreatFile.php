<?php

namespace App\Controllers\Ypo\Administrativo;

use App\Services\ServiceRetreatFile;
use App\Templates\Ypo\Administrativo\TemplateRetreatFile;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Ypo\Common;
use \App\Models\Entities\SiteRetreatFile;
use \App\Models\Entities\SiteCategory;
use App\Classes\Funcoes\Generate;

/**
 * Description of Home
 *
 * @author oseas
 */
class RetreatFile extends Common
{

    protected $service;
    protected $pagelink = 'arquivos-retreats';
    protected $obj;

    public function __construct()
    {
        $this->service = new ServiceRetreatFile();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateRetreatFile::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();
        if ($_POST) {
            $this->uploadRetreatsFile();
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "RetreatFile <strong>{$obj->nome}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "RetreatFile <strong>{$obj->nome}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $obj = SiteRetreatFile::find($id);
            if(is_numeric($obj->idcategoria))  $_POST['idcategoria']  = $obj->idcategoria;
            if(is_numeric($obj->idcategoria2)) $_POST['idcategoria2'] = $obj->idcategoria2;
            if(is_numeric($obj->iduser))       $_POST['iduser']       = $obj->iduser;

            $this->uploadRetreatsFile();
            if(!isset($_POST['portugues']) || !strlen($_POST['portugues']) || $_POST['portugues'] == 'undefined'){
                $_POST['portugues'] = $obj->portugues;
            }
            if(!isset($_POST['ingles']) || !strlen($_POST['ingles']) || $_POST['ingles'] == 'undefined'){
                $_POST['ingles'] = $obj->ingles;
            }
            
            $request = $this->service->update($_POST);
            $obj    = $request['success'] ? $request['data'] : null;
            $objNew = SiteRetreatFile::find($id);

            if ($request['success']) {
                $_SESSION['message']            = "RetreatFile <strong>{$obj->nome}</strong> alterado com sucesso!";
                $_SESSION['classe']             = 'alert-success';
                $return['response']['mensagem'] = "RetreatFile <strong>{$obj->nome}</strong> alterado com sucesso!";
                $return['response']['classe']   = 'alert-success';
                $return['response']['result']   = 'success';
                $return['response']['arquivo1'] = $objNew->portugues;
                $return['response']['arquivo2'] = $objNew->ingles;
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = SiteRetreatFile::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "RetreatFile <strong>{$obj->nome}</strong> excluido com sucesso!";
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o RetreatFile <strong>{$obj->nome}</strong>";
        }

        print_r(json_encode($request));
        exit();

    }
    
    public function uploadRetreatsFile(){
        $pasta = '';
        if(isset($_POST['idcategoria'])){
            $pasta .= SiteCategory::where('idcategory', $_POST['idcategoria'])->first()->title;
        }
        if(isset($_POST['idcategoria2'])){
            $pasta .= ' '.SiteCategory::where('idcategory', $_POST['idcategoria2'])->first()->title;
        }

        $pasta = Generate::url_generate($pasta);

        $responseimg1 = [];
        if(isset($_FILES['arquivopt'])){
            $file = [];
            $file['file'] = $_FILES['arquivopt'];
            
            $responseimg1 = $this->upload($file, $pasta);
        }

        $responseimg2 = [];
        if(isset($_FILES['arquivopt'])){
            $file = [];
            $file['file'] = $_FILES['arquivoen'];
            $responseimg2 = $this->upload($file, $pasta);
        }

        $_POST['portugues'] = isset($responseimg1['image']) ? $responseimg1['image'] : '';
        $_POST['ingles']    = isset($responseimg2['image']) ? $responseimg2['image'] : '';
    }

    public function deleteFile(){
        $return = [];
        if(!isset($_POST)){
            $return['response']['mensagem'] = 'Erro de requisição';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
            echo json_encode($return);
            exit;
        }

        $caminho = $_SERVER['DOCUMENT_ROOT'] . '/arquivos/' . $_POST['folder'];
        
        if(unlink($caminho.'/'.$_POST['file'])){
            $return['response']['mensagem'] = 'Arquivo deletado com sucesso';
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
            echo json_encode($return);
            exit;
        }


        $return['response']['mensagem'] = 'Erro de requisição';
        $return['response']['classe'] = 'alert-danger';
        $return['response']['result'] = 'error';
        $return['response']['redirect'] = '';
        $return['response']['status'] = 0;
        echo json_encode($return);
        exit;
    }

}
