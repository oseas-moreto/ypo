<?php

namespace App\Controllers\Ypo\Administrativo;

use App\Classes\Mail;
use \App\Controllers\Ypo\Common;
use App\Templates\Ypo\Administrativo\TemplateDashboard;
use \App\Classes\Funcoes\SessionControl;
use \App\Services\ServiceAccessUser;
use \App\Services\ServiceAccessSpouse;

/**
 * Description of Dashboard
 *
 * @author oseas
 */
class Dashboard extends Common
{

    protected $pagelink = '';

    public function __construct()
    {
        SessionControl::start_session();
        $this->logado();
        $this->view = new \stdClass();
        $this->uservice = new ServiceAccessUser();
        $this->sservice = new ServiceAccessSpouse();
    }

    public function index()
    {
        SessionControl::start_session();
        TemplateDashboard::index();
    }

    public function perfil($id)
    {
        SessionControl::start_session();
        $this->logado();
        $id = $_SESSION['user_id'];

        if ($_POST) {
            if($_SESSION['tipo'] == 'usuario') $request = $this->uservice->update($_POST);
            if($_SESSION['tipo'] == 'spouser') $request = $this->sservice->update($_POST);

            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Perfil <strong>{$obj->name}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/perfil/' . $id;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }
        
        if($_SESSION['tipo'] == 'usuario') TemplateDashboard::perfil($id);
        if($_SESSION['tipo'] == 'spouser') TemplateDashboard::perfilSpouse($_SESSION['idspouse']);
    }
}
