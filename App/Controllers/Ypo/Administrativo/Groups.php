<?php

namespace App\Controllers\Ypo\Administrativo;

use App\Models\Entities\AccessMenu;
use App\Services\ServiceGroup;
use App\Templates\Ypo\Administrativo\TemplateGroup;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Ypo\Common;
use \App\Models\Entities\AccessGroup;

/**
 * Description of Home
 *
 * @author oseas
 */
class Groups extends Common
{

    protected $service;
    protected $pagelink = 'gruposacesso';

    public function __construct(){
        $this->view = new \stdClass();
        $this->service = new ServiceGroup();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateGroup::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Grupo <strong>{$obj->description}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Grupo <strong>{$obj->description}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateGroup::form();
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Grupo <strong>{$obj->description}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Grupo <strong>{$obj->description}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateGroup::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = AccessGroup::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Grupo <strong>{$obj->description}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o grupo <strong>{$obj->description}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
