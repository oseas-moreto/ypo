<?php

namespace App\Controllers\Ypo\Administrativo;

use App\Services\ServiceClassificado;
use App\Templates\Ypo\Administrativo\TemplateClassificado;
use \App\Classes\Funcoes\SessionControl;
use \App\Controllers\Ypo\Common;
use \App\Models\Entities\SiteClassificado;

/**
 * Description of Home
 *
 * @author oseas
 */
class Classificado extends Common
{

    protected $service;
    protected $pagelink = 'cadastro-classificados';
    protected $obj;

    public function __construct()
    {
        $this->service = new ServiceClassificado();
    }

    public function index()
    {
        SessionControl::start_session();
        $this->logado();
        TemplateClassificado::index();
    }

    public function create()
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Classificado <strong>{$obj->nome}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Classificado <strong>{$obj->nome}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateClassificado::form();
    }

    public function update($id)
    {
        SessionControl::start_session();
        $this->logado();

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Classificado <strong>{$obj->nome}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Classificado <strong>{$obj->nome}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        TemplateClassificado::form($id);
    }

    public function destroy($id)
    {
        SessionControl::start_session();
        $obj = SiteClassificado::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Classificado <strong>{$obj->nome}</strong> excluido com sucesso!";
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o Classificado <strong>{$obj->nome}</strong>";
        }

        print_r(json_encode($request));
        exit();

    }

    public function uploads(){ 
      if(isset($_FILES['file'.$_GET['inputFile']]['name']) && $_FILES['file'.$_GET['inputFile']]['name'] <> ''){
        $file = [];
        $file['file'] = $_FILES['file'.$_GET['inputFile']];
        
        $responseimg  = $this->upload($file, 'classificado');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }

}
