<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use Illuminate\Database\Capsule\Manager as DB;
use \App\Models\Entities\SiteMenu;

class ServiceMenuSite
{

    public function create($request)
    {
        if ($request) {
            $obj1 = new SiteMenu();
            $request['id'] = $obj1->getNextUserID('site_menu', 'idmenu', $obj1);
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteMenu();
                $this->save($request, $i, $obj);
            }

            $return = [];
            if ($obj->idmenu != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Menu <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteMenu();
                $obj = SiteMenu::where('idmenu', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();

                if (!isset($obj->idmenu)) {
                    $obj = new SiteMenu;
                }
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
            }

            $return = [];
            if ($obj->idmenu != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Menu <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $objs = SiteMenu::where('idmenu', '=', $id)->get();
        $return = [];
        $resp = '';
        foreach ($objs as $key => $obj) {
            $resp =  $obj->delete();
        }

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $i, $obj)
    {
        $obj->idmenu      = $request['id'];
        $obj->idlanguage  = $request['idlanguage'][$i];
        $obj->title       = $request['title'][$i];
        $obj->link        = $request['link'][$i];
        $obj->order       = $request['order'][$i];
        $obj->status      = $request['status'];
        $obj->link_externo = $request['link_externo'];

        $obj->save();
    }

}
