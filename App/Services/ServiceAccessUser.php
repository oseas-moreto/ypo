<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use App\Models\Entities\AccessUser;
use App\Validators\ValidatorAccessUser;

class ServiceAccessUser
{
    protected $validator;

    public function __construct()
    {
        $this->validator = new ValidatorAccessUser();
    }

    public function create($request)
    {
        if ($request) {
            $obj = new AccessUser();
            $return = [];

            $return = $this->validator->validate($request);
            if ($return['success'] == true) {
                $this->save($request, $obj);
            } else {
                $this->setobj($request, $obj);
            }

            if ($obj->iduser <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "Não foi possivel inserir o admin <strong>{$obj->name}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = AccessUser::find(isset($request['iduser']) ? $request[ 'iduser'] : $request['id']);

            $return = [];
            $return = $this->validator->validate($request);
            if ($return['success'] == true) {
                $this->save($request, $obj);
            } else {
                $this->setobj($request, $obj);
            }


            if ($obj->iduser <> '') {
                $return['success'] = isset($return['success']) ? $return['success'] : true;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "";
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "Não foi possivel atualizar o admin <strong>{$obj->name}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = AccessUser::find($id);
        $resp = $obj->delete();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $this->setobj($request, $obj);
        $obj->save();
    }

    public function setobj($request, $obj)
    {
        $obj->codmember  = $request['codmember'];
        $obj->name       = $request['name'];
        $obj->user       = trim($request['user']);
        $obj->email      = $request['email'];
        $obj->phone      = $request['phone'];
        $obj->cpf        = $request['cpf'];
        $obj->estado     = $request['estado'];
        $obj->cidade     = $request['cidade'];
        $obj->capitulo   = $request['capitulo'];
        $obj->empresa    = $request['empresa'];
        $obj->fantasia   = $request['fantasia'];
        $obj->ramoatv    = $request['ramoatv'];
        $obj->nascimento = $request['nascimento'];
        $obj->website    = $request['website'];
        $obj->hobbies    = $request['hobbies'];
        if (isset($request['image']) && strlen($request['image'])) {
            $obj->image    = $request['image'];
        }
        if ($request['password'] <> '') {
            $obj->password = md5($request['password']);
        } else {
            $obj->password = $request['password_old'];
        }
        $obj->idgroup      = $request['idgroup'];
        $obj->status       = $request['status'];
    }
}
