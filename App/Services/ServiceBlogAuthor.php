<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteBlogAuthor;

class ServiceBlogAuthor
{

    public function create($request)
    {
        if ($request) {
            $obj1 = new SiteBlogAuthor();
            $request['id'] = $obj1->getNextUserID('site_blog_author', 'idauthor', $obj1);
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteBlogAuthor();
                if (!isset($obj->idauthor)) {
                    $obj = new SiteBlogAuthor;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if ($obj->idauthor != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteBlogAuthor();
                $obj = SiteBlogAuthor::where('idauthor', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);

            }

            $return = [];
            if ($obj->idauthor != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = SiteBlogAuthor::where('idauthor', '=', $id)->get();

        foreach ($obj as $ob) {
            $ob->status = 'd';
            $resp = $ob->save();
        }

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $i, $obj)
    {
        $obj->idauthor   = $request['id'];
        $obj->idlanguage = $request['idlanguage'][$i];
        $obj->title      = $request['title'][$i];
        $obj->text       = $request['text'][$i];
        $obj->status     = $request['status'];
        $obj->email      = $request['email'];
        $obj->image      = $request['image'];
        $obj->save();

    }

}
