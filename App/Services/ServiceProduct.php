<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteProduct;


class ServiceProduct{

    public function create($request) {
        if($request){
            $obj1 = new SiteProduct();
            $request['id'] = $obj1->getNextUserID('site_product', 'idproduct', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteProduct();
                if(!isset($obj->idproduct)){
                    $obj = new SiteProduct;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idproduct <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteProduct();
                $obj = SiteProduct::where('idproduct', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                if(!$obj instanceof SiteProduct) $obj = new SiteProduct();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idproduct <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteProduct::where('idproduct', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idproduct   = $request['id'];
        $obj->idlanguage  = $request['idlanguage'][$i];
        $obj->title       = $request['title'][$i];
        $obj->mintext     = $request['mintext'][$i];
        $obj->text        = $request['text'][$i];
        $obj->description = $request['description'][$i];
        $obj->keywords    = $request['keywords'][$i];
        $obj->idcategory  = $request['idcategory'];
        $obj->idsegment   = $request['idsegment'];
        $obj->video       = $request['video'];
        $obj->image       = $request['image'];
        $obj->price       = $request['price'];
        $obj->icon        = $request['icon'];
        $obj->order       = $request['order'];
        $obj->productcode = $request['productcode'];
        $obj->doc         = $request['doc'];
        $obj->status      = $request['status'];
        $obj->home        = $request['home'];
        $obj->photos      = isset($request['photos']) ? json_encode($request['photos']) : '';

        $obj->save();
    }

}
