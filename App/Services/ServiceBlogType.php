<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteBlogType;

class ServiceBlogType
{

    public function create($request)
    {
        if ($request) {
            $obj1 = new SiteBlogType();
            $request['id'] = $obj1->getNextUserID('site_blog_type', 'idtype', $obj1);
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteBlogType();
                if (!isset($obj->idtype)) {
                    $obj = new SiteBlogType;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if ($obj->idtype != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteBlogType();
                $obj = SiteBlogType::where('idtype', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);

            }

            $return = [];
            if ($obj->idtype != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = SiteBlogType::where('idtype', '=', $id)->get();

        foreach ($obj as $ob) {
            $ob->status = 'd';
            $resp = $ob->save();
        }

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $i, $obj)
    {
        $obj->idtype       = $request['id'];
        $obj->idlanguage   = $request['idlanguage'][$i];
        $obj->title        = $request['title'][$i];
        $obj->status       = $request['status'];
        /*$obj->image        = $request['image'];
        $obj->image_mobile = $request['mobile'];*/
        $obj->save();

    }

}
