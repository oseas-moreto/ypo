<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use App\Models\Entities\Contacts;

class ServiceContact
{
    public function destroy($id)
    {
        $obj = Contacts::find($id);
        $resp = $obj->delete();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }
}
