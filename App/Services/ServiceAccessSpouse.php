<?php
/**
 * Created by PhpStorm.
 * Spouse: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use App\Models\Entities\AccessSpouse;

class ServiceAccessSpouse
{

    public function create($request)
    {
        if ($request) {
            $obj = new AccessSpouse();
            $this->save($request, $obj);
            $return = [];

            if ($obj->idspouse <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "Não foi possivel inserir o admin <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = AccessSpouse::find($request['id']);
            $this->save($request, $obj);

            $return = [];


            if ($obj->idspouse <> '') {
                $return['success'] = isset($return['success']) ? $return['success'] : true;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "";
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "Não foi possivel atualizar o admin <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = AccessSpouse::find($id);
        $resp = $obj->delete();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $this->setobj($request, $obj);
        $obj->save();
    }

    public function setobj($request, $obj)
    {
        $obj->nome       = $request['nome'];
        $obj->iduser     = trim($request['iduser']);
        $obj->email      = $request['email'];
        $obj->phone      = $request['phone'];
        $obj->cpf        = $request['cpf'];
        $obj->user       = $request['user'];
        $obj->capitulo   = $request['capitulo'];
        $obj->empresa    = $request['empresa'];
        $obj->ramoatv    = $request['ramoatv'];
        $obj->nascimento = $request['nascimento'];
        $obj->parentesco = $request['parentesco'];
        $obj->hobbies    = $request['hobbies'];
        if (isset($request['image']) && strlen($request['image'])) {
            $obj->image  = $request['image'];
        }
        
        if ($request['password'] <> '') {
            $obj->password = md5($request['password']);
        } else {
            $obj->password = $request['password_old'];
        }
    }
}
