<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteTranslate;


class ServiceTranslate{

    public function create($request) {
        if($request){
            $obj1 = new SiteTranslate();
            $request['id'] = $obj1->getNextUserID('site_translate', 'idtranslate', $obj1);
            $request['json'] = [];
            for ($i=0; $i<count($request['idlanguage']); $i++) {
                $obj = new SiteTranslate();
                $request['json'][$i] = $this->formatJson($request['index'], $request['value'], $i);
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idtranslate <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Translate <strong>{$obj->section}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $request['json'] = [];
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteTranslate();
                $request['json'][$i] = $this->formatJson($request['index'], $request['value'], $i);
                $obj = SiteTranslate::where('idtranslate', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();

                if(!isset($obj->idtranslate)){
                    $obj = new SiteTranslate;
                }
                //echo $obj->section.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['section'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idtranslate <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Translate <strong>{$obj->section}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteTranslate::where('idtranslate', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idtranslate = $request['id'];
        $obj->idlanguage = $request['idlanguage'][$i];
        $obj->json = $request['json'][$i];
        $obj->section = $request['section'];
        $obj->status = $request['status'];

        $obj->save();
    }

    public function formatJson($index = [], $value = [], $indiceLinguagem){
        $json = [];
        $indiceLinguagem++;
        for($x=0;$x<count($index[$indiceLinguagem]);$x++){
          $json[$index[$indiceLinguagem][$x]] = $value[$indiceLinguagem][$x];
        }  
        return json_encode($json);
    }

}
