<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteSocial;

class ServiceSocial
{

    public function create($request)
    {
        if ($request) {
            $obj = new SiteSocial();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idsocial != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = SiteSocial::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idsocial != '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = SiteSocial::find($id);
        $obj->status = 'd';
        $resp = $obj->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $obj->title  = $request['title'];
        $obj->status = $request['status'];
        $obj->icon   = $request['icon'];
        $obj->image  = $request['image'];
        $obj->link   = $request['link'];
        $obj->ordem  = $request['ordem'];
        $obj->type   = $request['type'];

        $obj->save();
    }
}
