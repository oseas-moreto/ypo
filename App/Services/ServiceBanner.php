<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteBanner;


class ServiceBanner{

    public function create($request) {
        if($request){
            $obj1 = new SiteBanner();
            $request['id'] = $obj1->getNextUserID('site_banners', 'idbanner', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteBanner();
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idbanner <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Banner <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteBanner();
                $obj = SiteBanner::where('idbanner', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();

                if(!isset($obj->idbanner)){
                    $obj = new SiteBanner;
                }
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idbanner <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Banner <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteBanner::where('idbanner', '=', $id)->get();
        foreach($obj as $o){
            $o->status = 'd';
            $resp = $o->save();
        }
        

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idbanner     = $request['id'];
        $obj->idlanguage   = $request['idlanguage'][$i];
        $obj->title        = $request['title'][$i];
        $obj->link         = $request['link'][$i];
        $obj->image        = $request['image'];
        $obj->image_mobile = $request['image_mobile'];
        $obj->hash         = $request['hash'];
        $obj->status       = $request['status'];
        $obj->order        = $request['order'];

        $obj->save();

        /* echo $obj->title.'<br>';
        exit(); */

    }

}
