<?php
/**
 * Created by PhpStorm.
 * Diretoria: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use App\Models\Entities\AccessDiretoria;

class ServiceAccessDiretoria
{

    public function create($request)
    {
        if ($request) {
            $obj = new AccessDiretoria();
            $this->save($request, $obj);
            $return = [];

            if ($obj->iddiretoria <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "Não foi possivel inserir o admin <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = AccessDiretoria::find($request['id']);
            $this->save($request, $obj);

            $return = [];


            if ($obj->iddiretoria <> '') {
                $return['success'] = isset($return['success']) ? $return['success'] : true;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "";
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = isset($return['message']) && $return['message'] <> '' ? $return['message'] : "Não foi possivel atualizar o admin <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = AccessDiretoria::find($id);
        $resp = $obj->delete();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $this->setobj($request, $obj);
        $obj->save();
    }

    public function setobj($request, $obj)
    {
        $obj->fantasia   = $request['fantasia'];
        $obj->iduser     = $request['iduser'];
        $obj->idspouse   = $request['idspouse'];
        if(isset($request['imagem']) && $request['imagem'] <> 'undefined' && $request['imagem'] <> '' && $request['imagem'] <> null){
            $obj->imagem   = $request['imagem'];
        }   
        $obj->cargo          = $request['cargo'];
        $obj->capitulo       = $request['capitulo'];
        $obj->ordemDiretoria = isset($request['ordem']) ? $request['ordem'] : 0;
        $obj->ordemCapitulo  = isset($request['ordemCapitulo']) ? $request['ordemCapitulo'] : 0;
    }
}
