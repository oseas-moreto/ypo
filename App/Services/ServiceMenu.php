<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\AccessMenu;


class ServiceMenu{

    public function create($request) {
        if($request){
            $obj = new AccessMenu();
            $this->save($request, $obj);

            $return = [];
            if($obj->idmenu <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a página <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = AccessMenu::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idmenu <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a página <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $menu = AccessMenu::find($id);
        $menu->groups()->detach();
        $resp = AccessMenu::where('idmenu', $id)->delete();
        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->title = $request['title'];
        $obj->urlmenu = $request['function'];
        $obj->level = $request['level'];
        $obj->order = $request['order'];
        $obj->icon = $request['icon'];
        $obj->status = $request['status'];

        $obj->save();

        if(count($request['groups']) > 0){
            $obj->groups()->sync($request['groups']);
        }

    }

}
