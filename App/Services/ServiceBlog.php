<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteBlog;


class ServiceBlog{

    public function create($request) {
        if($request){
            $obj1 = new SiteBlog();
            $request['id'] = $obj1->getNextUserID('site_blog', 'idblog', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteBlog();
                if(!isset($obj->idblog)){
                    $obj = new SiteBlog;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idblog <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Post <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i < count($request['idlanguage']);$i++) {
                $obj = new SiteBlog();
                $obj = SiteBlog::where('idblog', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idblog <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Post <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $objs = SiteBlog::where('idblog', '=', $id)->get();
        foreach($objs as $obj){
            $obj->status = 'd';
            $resp = $obj->save();
        }
        

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idblog      = $request['id'];
        $obj->idlanguage  = $request['idlanguage'][$i];
        $obj->idcategory  = $request['idcategory'];
        $obj->idtype      = $request['idtype'];
        $obj->idauthor    = $request['idauthor'];
        $obj->title       = $request['title'][$i];
        $obj->ordem       = $request['ordem'];
        $obj->date        = $request['date'];
        $obj->text        = $request['text'][$i];
        $obj->mintext     = $request['mintext'][$i];
        $obj->link        = $request['link'];
        $obj->reference   = $request['reference'][$i];
        $obj->status      = $request['status'];
        $obj->spotlight   = $request['spotlight'];
        $obj->link_video  = $request['link_video'];
        $obj->image       = $request['image'];
        $obj->keywords    = $request['keywords'][$i];
        $obj->description = $request['description'][$i];

        if(!is_numeric($obj->idblog)){
            $obj->date_create = date('Y-m-d H:i:s');
        }

        $obj->save();

    }

}
