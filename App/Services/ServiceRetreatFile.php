<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteRetreatFile;


class ServiceRetreatFile{

    public function create($request) {
        if($request){
            $obj = new SiteRetreatFile();
            $this->save($request, $obj);

            $return = [];
            if($obj->idretreat <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o RetreatFile <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteRetreatFile::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idretreat <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o RetreatFile <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj  = SiteRetreatFile::find($id);
        $resp = $obj->delete($id);

        $return = [];
        $return['success'] = $resp;

        return $return;
    }

    public function save($request, $obj){
        $obj->iduser       = $request['iduser'];
        $obj->idcategoria  = $request['idcategoria'];
        $obj->idcategoria2 = $request['idcategoria2'];
        $obj->nome         = $request['nome'];
        $obj->portugues    = $request['portugues'];
        $obj->ingles       = $request['ingles'];    

        $obj->save();
    }
}
