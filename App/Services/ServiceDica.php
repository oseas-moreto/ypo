<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteDica;


class ServiceDica{

    public function create($request) {
        if($request){
            $obj = new SiteDica();
            $this->save($request, $obj);

            $return = [];
            if($obj->iddica <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a Dica <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteDica::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->iddica <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a Dica <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteDica::find($id);
        $obj->ativo = 'd';
        $resp = $obj->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->iduser         = $request['iduser'];
        $obj->nome           = $request['nome'];
        $obj->texto          = $request['texto'];
        $obj->site           = $request['site']; 
        $obj->cidade         = $request['cidade'];  
        $obj->idcategoria2   = $request['idcategoria2'] ?? null;  
        $obj->estado         = $request['estado'] ?? null; 
        $obj->pais           = $request['pais'] ?? null;  
        $obj->nomeContato    = $request['nomeContato'];
        $obj->emailContato   = $request['emailContato'];
        $obj->whatsapp       = $request['whatsapp'];
        $obj->telefone       = $request['telefone'];
        $obj->idcategoria    = $request['idcategoria'];
        $obj->palavrasChaves = $request['palavrasChaves'];
        $obj->ativo          = $request['ativo'];

        $obj->save();
    }
}
