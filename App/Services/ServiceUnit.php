<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteUnit;

class ServiceUnit
{

    public function create($request)
    {
        if ($request) {
            $obj = new SiteUnit();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idunit != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = SiteUnit::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idunit != '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = SiteUnit::find($id);
        $obj->status = 'd';
        $resp = $obj->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $obj->title       = $request['title'];
        $obj->status      = $request['status'];
        $obj->address     = $request['address'];
        $obj->image       = $request['image'];
        $obj->cep         = $request['cep'];
        $obj->ordem       = $request['ordem'];
        $obj->country     = $request['country'];
        $obj->state       = $request['state'];
        $obj->city        = $request['city'];
        $obj->phone       = $request['phone'];
        $obj->mobilephone = $request['mobilephone'];
        $obj->email       = $request['email'];

        $obj->save();
    }
}
