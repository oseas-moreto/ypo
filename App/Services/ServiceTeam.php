<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Team;


class ServiceTeam{

    public function create($request) {
        if($request){
            $obj1 = new Team();
            $request['id'] = $obj1->getNextUserID('site_team', 'idteam', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new Team();
                if(!isset($obj->idteam)){
                    $obj = new Team;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idteam <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new Team();
                $obj = Team::where('idteam', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idteam <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = Team::where('idteam', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idteam       = $request['id'];
        $obj->idlanguage   = $request['idlanguage'][$i];
        $obj->idcargo      = $request['idcargo'];
        $obj->idposicao    = $request['idposicao'];
        $obj->idescritorio = $request['idescritorio'];
        $obj->title        = $request['title'][$i];
        $obj->image        = $request['image'];
        $obj->text         = $request['text'][$i];
        $obj->mobilephone  = $request['mobilephone'];
        $obj->whatsapp     = $request['whatsapp'];
        $obj->email        = $request['email'];
        $obj->skype        = $request['skype'];
        $obj->vcard        = $request['vcard'];
        $obj->status       = $request['status'];
        $obj->order        = $request['order'];

        $obj->save();
    }

}
