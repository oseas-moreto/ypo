<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteFaq;


class ServiceFaq{

    public function create($request) {
        if($request){
            $obj1 = new SiteFaq();
            $request['id'] = $obj1->getNextUserID('site_faq', 'idfaq', $obj1);
            for ($i=0; $i<count($request['idlanguage']); $i++) {
                $obj = new SiteFaq();
                $request['response']     = [];
                $request['response'][$i] = $this->formatJson($request['titleResponse'], $request['orderResponse'], $request['textResponse'], $i);
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idfaq <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Faq <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteFaq();
                $request['response']     = [];
                $request['response'][$i] = $this->formatJson($request['titleResponse'], $request['orderResponse'], $request['textResponse'], $i);
                $obj = SiteFaq::where('idfaq', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idfaq <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Faq <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteFaq::where('idfaq', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idfaq      = $request['id'];
        $obj->idlanguage = $request['idlanguage'][$i];
        $obj->title      = $request['title'][$i];
        $obj->text       = $request['text'][$i];
        $obj->order      = $request['order'];
        $obj->response   = $request['response'][$i];
        $obj->status     = $request['status'];

        $obj->save();
    }

    public function formatJson($titleResponse = [], $orderResponse = [], $textResponse = [],  $indiceLinguagem){
        $json = [];
        $indiceLinguagem++;
        $json[$indiceLinguagem] = [];
        if(isset($titleResponse[$indiceLinguagem])){
            for($x=0;$x<count($titleResponse[$indiceLinguagem]);$x++){
                $json[$indiceLinguagem][] = [
                    'titleResponse' => $titleResponse[$indiceLinguagem][$x],
                    'orderResponse' => $orderResponse[$indiceLinguagem][$x],
                    'textResponse'  => $textResponse[$indiceLinguagem][$x]
                ];
            }  
        }
        
        return json_encode($json);
    }

}
