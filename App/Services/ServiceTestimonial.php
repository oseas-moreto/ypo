<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteTestimonial;

class ServiceTestimonial
{

    public function create($request)
    {
        if ($request) {
            $obj1 = new SiteTestimonial();
            $request['id'] = $obj1->getNextUserID('site_testimonial', 'idtestimonial', $obj1);
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteTestimonial();
                if (!isset($obj->idtestimonial)) {
                    $obj = new SiteTestimonial;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if ($obj->idtestimonial != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o depoimento <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteTestimonial();
                $obj = SiteTestimonial::where('idtestimonial', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);

            }

            $return = [];
            if ($obj->idtestimonial != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o depoimento <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = SiteTestimonial::where('idtestimonial', '=', $id)->get();
        $obj->status = 'd';
        $resp = $obj->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $i, $obj)
    {
        $obj->idtestimonial  = $request['id'];
        $obj->idlanguage     = $request['idlanguage'][$i];
        $obj->idcategory     = $request['idcategory'];
        $obj->name           = $request['name'][$i];
        $obj->cargo          = $request['cargo'][$i];
        $obj->data_string    = $request['data_string'][$i];
        $obj->mindescription = $request['mindescription'][$i];
        $obj->text           = $request['text'][$i];
        $obj->status         = $request['status'];
        $obj->image          = $request['image'];
        $obj->order          = $request['order'];
        $obj->home           = $request['home'];
        $obj->site           = $request['site'];
        $obj->link           = $request['link'];

        $obj->save();

    }

}
