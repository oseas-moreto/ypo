<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteInstitucional;


class ServiceInstitucional{

    public function create($request) {
        if($request){
            $obj1 = new SiteInstitucional();
            $request['id'] = $obj1->getNextUserID('site_institucional', 'idinstitucional', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteInstitucional();
                if(!isset($obj->idinstitucional)){
                    $obj = new SiteInstitucional;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idinstitucional <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Institucional <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteInstitucional();
                $obj = SiteInstitucional::where('idinstitucional', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idinstitucional <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Institucional <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteInstitucional::where('idinstitucional', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idinstitucional = $request['id'];
        $obj->idlanguage      = $request['idlanguage'][$i];
        $obj->title           = $request['title'][$i];
        $obj->subtitle        = $request['subtitle'][$i];
        $obj->hash            = $request['hash'][$i];
        $obj->text            = $request['text'][$i];
        $obj->status          = $request['status'];
        $obj->imagem          = $request['imagem'];
        $obj->keywords        = $request['keywords'][$i];
        $obj->description     = $request['description'][$i];

        $obj->save();
    }

}
