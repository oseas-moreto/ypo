<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use App\Models\Entities\SiteSolicitacaoDesconto;

class ServiceSolicitacao
{
    public function register($request){
        if ($request) {
            $obj              = new SiteSolicitacaoDesconto();
            $obj->iduser      = $request['iduser'];
            $obj->idempresa   = $request['idempresa'];
            $obj->mensagem    = $request['mensagem'];
            $obj->date_create = date('Y-m-d');
            $obj->dados       = json_encode($request);

            $obj->save();

            $return = [];
            if ($obj->idsolicitacao != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel enviar a solicitação";
                $return['data'] = $obj;
            }

            return $return;
        }

    }

    public function destroy($id){
        $obj = SiteSolicitacaoDesconto::find($id);
        $resp = $obj->delete();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }
}
