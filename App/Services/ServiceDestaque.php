<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteDestaque;


class ServiceDestaque{

    public function create($request) {
        if($request){
            $obj = new SiteDestaque();
            $this->save($request, $obj);

            $return = [];
            if($obj->iddestaque <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a Destaque <strong>{$obj->titulo}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteDestaque::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->iddestaque <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a Destaque <strong>{$obj->titulo}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteDestaque::find($id);
        $obj->ativo = 'd';
        $resp = $obj->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->titulo      = $request['titulo'];
        $obj->ordem       = $request['ordem'];
        $obj->link        = $request['link']; 
        $obj->imagem      = $request['imagem']; 
        $obj->data        = $request['data'];  
        $obj->ativo       = $request['ativo'];

        $obj->save();
    }
}
