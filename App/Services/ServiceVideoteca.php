<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteVideoteca;


class ServiceVideoteca{

    public function create($request) {
        if($request){
            $obj = new SiteVideoteca();
            $this->save($request, $obj);

            $return = [];
            if($obj->idvideoteca <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a Videoteca <strong>{$obj->titulo}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteVideoteca::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idvideoteca <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a Videoteca <strong>{$obj->titulo}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteVideoteca::find($id);
        $obj->ativo = 'd';
        $resp = $obj->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->titulo      = $request['titulo'];
        $obj->texto       = $request['texto'];
        $obj->site        = $request['site']; 
        $obj->data        = $request['data'];  
        $obj->webinar     = $request['webinar'];
        $obj->idcategoria = $request['idcategoria'];
        $obj->video       = $request['video'];
        $obj->imagem      = $request['imagem'];
        $obj->ativo       = $request['ativo'];

        $obj->save();
    }
}
