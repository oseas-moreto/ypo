<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\TeamOffice;


class ServiceOffice{

    public function create($request) {
        if($request){
            $obj1 = new TeamOffice();
            $request['id'] = $obj1->getNextUserID('site_team_office', 'idoffice', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new TeamOffice();
                if(!isset($obj->idoffice)){
                    $obj = new TeamOffice;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idoffice <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new TeamOffice();
                $obj = TeamOffice::where('idoffice', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idoffice <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = TeamOffice::where('idoffice', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idoffice = $request['id'];
        $obj->idlanguage = $request['idlanguage'][$i];
        $obj->title      = $request['title'][$i];
        $obj->status     = $request['status'];
        $obj->order      = $request['order'];
        $obj->type       = $request['type'];

        $obj->save();
    }

}
