<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteProductSegment;


class ServiceProductSegment{

    public function create($request) {
        if($request){
            $obj1 = new SiteProductSegment();
            $request['id'] = $obj1->getNextUserID('site_product_segment', 'idsegment', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteProductSegment();
                if(!isset($obj->idsegment)){
                    $obj = new SiteProductSegment;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idsegment <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteProductSegment();
                $obj = SiteProductSegment::where('idsegment', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idsegment <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteProductSegment::where('idsegment', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idsegment  = $request['id'];
        $obj->idlanguage = $request['idlanguage'][$i];
        $obj->title      = $request['title'][$i];
        $obj->subtitle   = $request['subtitle'][$i];
        $obj->text       = $request['text'][$i];
        $obj->status     = $request['status'];
        $obj->image      = $request['imagem'];
        $obj->order      = $request['order'];
        $obj->icon       = $request['icon'];
        $obj->type       = $request['type'];

        $obj->save();
    }

}
