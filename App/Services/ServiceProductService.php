<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteProductService;


class ServiceProductService{

    public function create($request) {
        if($request){
            $obj1 = new SiteProductService();
            $request['id'] = $obj1->getNextUserID('site_product_service', 'idservice', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteProductService();
                if(!isset($obj->idservice)){
                    $obj = new SiteProductService;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idservice <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteProductService();
                $obj = SiteProductService::where('idservice', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                if(!$obj instanceof SiteProductService) $obj = new SiteProductService();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idservice <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteProductService::where('idservice', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idservice   = $request['id'];
        $obj->idlanguage  = $request['idlanguage'][$i];
        $obj->idcategory  = $request['idcategory'];
        $obj->idsegment   = $request['idsegment'];
        $obj->title       = $request['title'][$i];
        $obj->mintext     = $request['mintext'][$i];
        $obj->text        = $request['text'][$i];
        $obj->description = $request['description'][$i];
        $obj->keywords    = $request['keywords'][$i];
        $obj->status      = $request['status'];
        $obj->image       = $request['image'];
        $obj->video       = $request['video'];
        $obj->price       = $request['price'];
        $obj->productcode = $request['productcode'];
        $obj->icon        = $request['icon'];
        $obj->doc         = $request['doc'];
        $obj->home        = $request['home'];
        $obj->photos      = isset($request['photos']) ? json_encode($request['photos']) : '';
        $obj->order       = $request['order'];

        $obj->save();
    }

}
