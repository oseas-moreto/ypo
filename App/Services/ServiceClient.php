<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteClient;

class ServiceClient
{

    public function create($request)
    {
        if ($request) {
            $obj1 = new SiteClient();
            $request['id'] = $obj1->getNextUserID('site_client', 'idclient', $obj1);
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteClient();
                if (!isset($obj->idclient)) {
                    $obj = new SiteClient;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if ($obj->idclient != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            for ($i = 0; $i < count($request['idlanguage']); $i++) {
                $obj = new SiteClient();
                $obj = SiteClient::where('idclient', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                if(!$obj instanceof SiteClient) $obj = new SiteClient();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);

            }

            $return = [];
            if ($obj->idclient != '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = SiteClient::where('idclient', '=', $id)->get();
        $obj->status = 'd';
        $resp = $obj->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $i, $obj)
    {
        $obj->idclient       = $request['id'];
        $obj->idlanguage     = $request['idlanguage'][$i];
        $obj->idcategory     = $request['idcategory'];
        $obj->name           = $request['name'][$i];
        $obj->cargo          = $request['cargo'][$i];
        $obj->data_string    = $request['data_string'][$i];
        $obj->mindescription = $request['mindescription'][$i];
        $obj->text           = $request['text'][$i];
        $obj->status         = $request['status'];
        $obj->image          = $request['image'];
        $obj->order          = $request['order'];
        $obj->home           = $request['home'];
        $obj->site           = $request['site'];
        $obj->link           = $request['link'];

        $obj->save();

    }

}
