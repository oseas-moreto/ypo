<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteCategory;


class ServiceCategory{

    public function create($request) {
        if($request){
            $obj1 = new SiteCategory();
            $request['id'] = $obj1->getNextUserID('site_category', 'idcategory', $obj1);
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteCategory();
                if(!isset($obj->idcategory)){
                    $obj = new SiteCategory;
                }
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idcategory <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteCategory();
                $obj = SiteCategory::where('idcategory', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idcategory <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteCategory::where('idcategory', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idcategory = $request['id'];
        $obj->idlanguage = $request['idlanguage'][$i];
        $obj->title      = $request['title'][$i];
        $obj->subtitle   = $request['subtitle'][$i];
        $obj->text       = $request['text'][$i];
        $obj->status     = $request['status'];
        $obj->image      = $request['imagem'];
        $obj->order      = $request['order'];
        $obj->nivel      = $request['nivel'];
        $obj->listagem   = $request['listagem'];
        $obj->icon       = $request['icon'];
        $obj->type       = $request['type'];

        $obj->save();
    }

}
