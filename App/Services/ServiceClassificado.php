<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteClassificado;


class ServiceClassificado{

    public function create($request) {
        if($request){
            $obj = new SiteClassificado();
            $this->save($request, $obj);

            $return = [];
            if($obj->idclassificado <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Classificado <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteClassificado::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idclassificado <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Classificado <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteClassificado::find($id);
        $obj->ativo = 'd';
        $resp = $obj->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        if(isset($request['imagem']) && count($request['imagem']) > 0){
            $request['imagem'] = $this->tratarImagens($request['imagem']);
        }
        $obj->iduser         = $request['iduser'];
        $obj->nome           = $request['nome'];
        $obj->logo           = $request['logo'];
        $obj->banner         = $request['banner'];
        $obj->tipo           = $request['tipo'];
        $obj->texto          = $request['texto'];
        $obj->video          = $request['video'];    
        $obj->valor          = $request['valor'];
        $obj->imagens        = (isset($request['imagem']) && count($request['imagem'])) ? json_encode($request['imagem']) : $request['imagens'];
        $obj->nomeContato    = $request['nomeContato'];
        $obj->emailContato   = $request['emailContato'];
        $obj->whatsapp       = $request['whatsapp'];
        $obj->botaoZap       = $request['botaoZap'];
        $obj->site           = $request['site'];
        $obj->moeda          = $request['moeda'];
        $obj->idcategoria    = $request['idcategoria'];
        $obj->idcategoria2   = $request['idcategoria2'];
        $obj->palavrasChaves = $request['palavrasChaves'];
        $obj->ativo          = $request['ativo'];

        $obj->save();
    }


    public function tratarImagens($imagem){
        $newImagem = [];

        foreach ($imagem as $key => $value) {
            if(strlen($value)) $newImagem[] = $value;
        }
        
        return $newImagem;
    }

}
