<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteModule;


class ServiceModule{

    public function create($request) {
        if($request){
            $obj1 = new SiteModule();
            $request['id'] = $obj1->getNextUserID('site_module', 'idmodule', $obj1);
            for ($i=0; $i<count($request['idlanguage']); $i++) {
                $obj = new SiteModule();
                $this->save($request, $i, $obj);
            }

            $return = [];
            if($obj->idmodule <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Module <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            for ($i=0;$i<count($request['idlanguage']);$i++) {
                $obj = new SiteModule();
                $obj = SiteModule::where('idmodule', '=', $request['id'])->where('idlanguage', '=', $request['idlanguage'][$i])->first();

                if(!isset($obj->idmodule)){
                    $obj = new SiteModule;
                }
                //echo $obj->title.'-'.$obj->idlanguage.'-'.$i.'-'.$request['id'].'-'.$request['title'][$i].'<br>';
                $this->save($request, $i, $obj);
                
            }

            $return = [];
            if($obj->idmodule <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Module <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteModule::where('idmodule', '=', $id)->get();
        foreach ($obj as $key => $value) {
            $value->status = 'd';
            $resp = $value->save();
    
            $return = [];
    
            if($resp){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }
        }

        return $return;
    }

    public function save($request, $i, $obj){
        $obj->idmodule = $request['id'];
        $obj->idlanguage = $request['idlanguage'][$i];
        $obj->json = $request['json'][$i];
        $obj->title = $request['title'][$i];
        $obj->status = $request['status'];
        $obj->hash = $request['hash'][$i];

        $obj->save();
    }

}
