<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteEmpresa;


class ServiceEmpresa{

    public function create($request) {
        if($request){
            $obj = new SiteEmpresa();
            $this->save($request, $obj);

            $return = [];
            if($obj->idempresa <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Empresa <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteEmpresa::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idempresa <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Empresa <strong>{$obj->nome}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteEmpresa::find($id);
        $obj->ativo = 'd';
        $resp = $obj->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        if(isset($request['redesocial']) && count($request['redesocial']) > 0){
            $request['redesocial'] = $this->tratarRedeSocial($request['redesocial']);
        }
        if(isset($request['imagem']) && count($request['imagem']) > 0){
            $request['imagem'] = $this->tratarImagens($request['imagem']);
        }
        $obj->nome           = $request['nome'];
        $obj->subtitulo      = $request['subtitulo'];
        $obj->nomeContato    = $request['nomeContato'];
        $obj->emailContato   = $request['emailContato'];
        $obj->botaoZap       = $request['botaoZap'];
        $obj->whatsapp       = $request['whatsapp'];
        $obj->site           = $request['site'];
        $obj->banner         = $request['banner'];
        $obj->endereco       = $request['endereco'];
        $obj->texto          = $request['texto'];
        $obj->descontos      = $request['descontos'];
        $obj->termos         = $request['termos'];
        $obj->palavrasChaves = $request['palavrasChaves'];
        $obj->logo           = $request['logo'];
        $obj->redesSociais   = (isset($request['redesocial']) && count($request['redesocial']) > 0) ? json_encode($request['redesocial']) : $request['redesSociais'];
        $obj->imagens        = (isset($request['imagem']) && count($request['imagem'])) ? json_encode($request['imagem']) : $request['imagens'];
        $obj->ativo          = $request['ativo'];
        $obj->especiaisYpo   = $request['especiaisYpo'];
        $obj->idcategoria    = $request['idcategoria'];
        $obj->idcategoria2   = $request['idcategoria2'];

        $obj->save();
    }


    public function tratarImagens($imagem){
        $newImagem = [];

        foreach ($imagem as $key => $value) {
            if(strlen($value)) $newImagem[] = $value;
        }
        
        return $newImagem;
    }

    public function tratarRedeSocial($redesocial){
        $newRedeSocial = [];
        foreach ($redesocial['icone'] as $key => $value) {
            if(strlen($value)){
                $newRedeSocial[] = [
                    'icone' => $value,
                    'link' => $redesocial['link'][$key]
                ];
            }
        }
        return $newRedeSocial;
    }

}
