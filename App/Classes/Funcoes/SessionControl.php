<?php

namespace App\Classes\Funcoes;

class SessionControl 
{
    public static function start_session(){
        if(empty(session_id())){
            session_start();
        }

        if(!isset($_SESSION['link'])) $_SESSION['link'] = '/login';
        if(!isset($_SESSION['class'])) $_SESSION['link'] = '';
        if(!isset($_SESSION['name'])) $_SESSION['link'] = 'Minha Conta';
    }

    public static function session_destroy(){
        self::start_session();
        session_destroy();

        if(!isset($_SESSION['link'])) $_SESSION['link'] = '/login';
        if(!isset($_SESSION['class'])) $_SESSION['link'] = '';
        if(!isset($_SESSION['name'])) $_SESSION['link'] = 'Minha Conta';
    }

    // Pega o SESSION_ID
    public static function get_sessao(){
        if(empty(session_id())){
            session_start();
        }
        return session_id();
    }

    /*CRIA UM COOKIE*/
    public static function set_cookie($name, $value){
        return setcookie($name, $value,time()+112);
    }

    public static function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function which_device(){
        $mobile = FALSE;

        $user_agents = array("iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","IsGeneric");

        foreach($user_agents as $user_agent){
            if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE) {
                $mobile = TRUE;
                $modelo = $user_agent;
                break;
            }
        }

        if($mobile){
            return $modelo;
        }else{
            return "Computador";
        }
    }
}
