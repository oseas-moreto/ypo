<?php

namespace App\Classes\Funcoes;


class DateManipulation{

    public static function daymonthlabel($t, $i){
      $array = [];
      $array['m'] = array(
          1 => 'Janeiro',
          'Fevereiro',
          'Março',
          'Abril',
          'Maio',
          'Junho',
          'Julho',
          'Agosto',
          'Setembro',
          'Outubro',
          'Novembro',
          'Dezembro'
      );
      $array['d'] = array(
          1 => 'Domingo',
          'Segunda-Feira',
          'Terça-Feira',
          'Quarta-Feira',
          'Quinta-Feira',
          'Sexta-Feira',
          'Sábado'
      );

      return $array[$t][$i];
    }

    public static function returnmonth($m){
        $array = array(
          1 => 'Janeiro',
          'Fevereiro',
          'Março',
          'Abril',
          'Maio',
          'Junho',
          'Julho',
          'Agosto',
          'Setembro',
          'Outubro',
          'Novembro',
          'Dezembro'
        );

        return $array[$m];

    }

    public static function returnmonthabr($m){
        $array = array(
          '01' => 'Jan',
          '02' => 'Fev',
          '03' => 'Mar',
          '04' => 'Abr',
          '05' => 'Mai',
          '06' => 'Jun',
          '07' => 'Jul',
          '08' => 'Ago',
          '09' => 'Set',
          '10' => 'Out',
          '11' => 'Nov',
          '12' => 'Dez'
        );

        return $array[$m];

    }

    public static function dateinterval($start, $end){
      $datatime1 = new \DateTime($start);
      $datatime2 = new \DateTime($end);

      $data1  = $datatime1->format('Y-m-d H:i:s');
      $data2  = $datatime2->format('Y-m-d H:i:s');

      $diff = $datatime1->diff($datatime2);
      $horas = $diff->h + ($diff->days * 24);

      return $horas;

    }

    public static function searchDay($d){
      $phrase  = $d;
      $healthy = [1, 2, 3, 4, 5, 6, 7, 8];
      $yummy   = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Fer'];

      $newPhrase = str_replace($healthy, $yummy, $phrase);

      return $newPhrase;
    }

    public static function getToday(){
      return date('Y-m-d H:i:s');
    }

    public static function idade($date){
      // Separa em dia, mês e ano
      list($ano, $mes, $dia) = explode('-', $date);

      // Descobre que dia é hoje e retorna a unix timestamp
      $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
      // Descobre a unix timestamp da data de nascimento do fulano
      $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

      // Depois apenas fazemos o cálculo já citado :)
      $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

      return $idade;
    }

    public static function yearold18(){
        $atual = date('d-m-Y');
        return date('Y-m-d', strtotime('-6570 days', strtotime($atual)));
    }

}
