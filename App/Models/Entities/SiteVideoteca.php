<?php

namespace App\Models\Entities;

use App\Core\Models;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteVideoteca extends Models{
    
    protected $table = 'videoteca';
    protected $primaryKey = 'idvideoteca';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];

    public static function getVideotecas($filters){
        $result = self::when(isset($filters['categoria']) && $filters['categoria'] > 0, function ($q)  use ($filters) {
            return $q->where('idcategoria', '=', $filters['categoria']);
        })->when((isset($filters['data']) && strlen($filters['data'])), function ($q)  use ($filters) {
            return $q->where('data', '=', $filters['data']);
        });

        return $result->where('ativo', '=', 's');
    }
}
