<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Language;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteMenu extends Models
{
    use HasCompositePrimaryKey;

    protected $table = 'site_menu';
    protected $primaryKey = array('idmenu', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language()
    {
        return $this->belongsTo(Language::class, 'idlanguage');
    }

}
