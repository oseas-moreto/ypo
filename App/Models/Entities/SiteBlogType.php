<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Language;
use App\Models\Entities\SiteBlog;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

use Awobaz\Compoships\Compoships;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteBlogType extends Models
{

    use HasCompositePrimaryKey;
    use Compoships;

    protected $table = 'site_blog_type';
    protected $primaryKey = array('idtype', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language()
    {
        return $this->belongsTo(Language::class, 'idlanguage');
    }

    public function blogs()
    {
        return $this->hasMany(SiteBlog::class, $this->primaryKey);
    }

}
