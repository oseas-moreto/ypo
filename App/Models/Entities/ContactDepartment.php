<?php

namespace App\Models\Entities;

use App\Models\Entities\Language;
use App\Core\Models;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;
use Awobaz\Compoships\Compoships;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class ContactDepartment extends Models {
    use HasCompositePrimaryKey;
    use Compoships;

    protected $table = 'contacts_department';
    protected $primaryKey = array('iddepartment', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language(){
        return $this->belongsTo(Language::class, 'idlanguage');
    }
}
