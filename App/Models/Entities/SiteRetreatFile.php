<?php

namespace App\Models\Entities;

use App\Core\Models;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteRetreatFile extends Models
{
    protected $table = 'retreats_files';
    protected $primaryKey = 'idretreat';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];
}
