<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\SiteHotel;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Newsletter extends Models {
    protected $table = 'site_newsletter';
    protected $primaryKey = 'idnewsletter';
    public $timestamps = false;
    protected $fillable = [ 'name', 'email', 'idhotel', 'date'];
    protected $guarded = [];

    public function hotel()
    {
        return $this->belongsTo(SiteHotel::class, 'idhotel');

    }
}
