<?php

namespace App\Models\Entities;

use App\Models\Entities\Language;
use App\Core\Models;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteInstitucional extends Models {
    use HasCompositePrimaryKey;

    protected $table = 'site_institucional';
    protected $primaryKey = array('idinstitucional', 'idlanguage');
    public $timestamps = false;
    protected $fillable = ['title', 'subtitle', 'text', 'status'];
    protected $guarded = [];

    public function language(){
        return $this->belongsTo(Language::class, 'idlanguage');
    }
}
