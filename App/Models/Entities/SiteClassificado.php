<?php

namespace App\Models\Entities;
use App\Models\Entities\SiteCategory;

use App\Core\Models;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteClassificado extends Models
{
    protected $table = 'classificado';
    protected $primaryKey = 'idclassificado';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];

    public static function getClassificados($filters){
        $result = self::when(isset($filters['categoria']) && $filters['categoria'] > 0, function ($q)  use ($filters) {
            return $q->whereRaw('( idcategoria = '.$filters['categoria'].' OR idcategoria2 = '.$filters['categoria'].' )');
        })->when((isset($filters['min']) && isset($filters['max'])), function ($q)  use ($filters) {
            return $q->whereBetween('valor', [$filters['min'], $filters['max']]);
        });

        if(isset($filters['city']) && is_array($filters['city'])){
            $result->join('access_user', 'classificado.iduser', '=', 'access_user.iduser')
            ->whereIn('cidade', $filters['cidade']);
        }

        if(isset($filters['tipo']) && is_array($filters['tipo'])){
            $result->whereIn('tipo', $filters['tipo']);
        }

        return $result->where('ativo', '=', 's');
    }

    public static function getCidades(){
        return self::join('access_user', 'classificado.iduser', '=', 'access_user.iduser')
        ->where('ativo', '=', 's')
        ->selectRaw('CONCAT(cidade," - ", estado) as city, cidade')
        ->groupBy('cidade')->get();
    }
}
