<?php

namespace App\Models\Entities;

use App\Core\Models;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;
use App\Models\Entities\Language;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteBanner extends Models {
    use HasCompositePrimaryKey;

    protected $table = 'site_banners';
    protected $primaryKey = array('idbanner', 'idlanguage');
    public $timestamps = false;
    protected $fillable = [ 'text', 'title', 'description', 'link', 'image', 'status', 'item1', 'item2', 'item3', 'item4'];
    protected $guarded = [];

     public function language(){
        return $this->belongsTo(Language::class, 'idlanguage');
    }

}
