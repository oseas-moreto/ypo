<?php

namespace App\Models\Entities;

use App\Models\Entities\Language;
use App\Models\Entities\SiteCategory;
use App\Models\Entities\SiteBlogType;
use App\Models\Entities\SiteBlogAuthor;
use App\Core\Models;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

use Awobaz\Compoships\Compoships;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteBlog extends Models {
    use HasCompositePrimaryKey;
    use Compoships;

    protected $table = 'site_blog';
    protected $primaryKey = array('idblog', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language(){
        return $this->belongsTo(Language::class, 'idlanguage');
    }

    public function category(){
        return $this->belongsTo(SiteCategory::class, array('idcategory', 'idlanguage'));
    }

    public function auyhor(){
        return $this->belongsTo(SiteBlogAuthor::class, array('idauthor', 'idlanguage'));
    }

    public function type(){
        return $this->belongsTo(SiteBlogType::class, array('idtype', 'idlanguage'));
    }
}
