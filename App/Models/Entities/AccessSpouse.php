<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\AccessUser;

/**
 * Description of AccessSpouse
 *
 * @author oseas
 */
class AccessSpouse extends Models{
    protected $table = 'spouse';
    protected $primaryKey = 'idspouse';
    public $timestamps = false;
    protected $guarded = [];

    public function usuario(){
        return $this->belongsTo(AccessUser::class, 'iduser');
    }
}
