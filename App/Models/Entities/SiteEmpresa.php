<?php

namespace App\Models\Entities;

use App\Core\Models;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteEmpresa extends Models
{
    protected $table = 'empresa';
    protected $primaryKey = 'idempresa';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];
}
