<?php

namespace App\Models\Entities;

use App\Core\Models;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteSeo extends Models {
    protected $table = 'site_seo';
    protected $primaryKey = 'idseo';
    public $timestamps = false;
    protected $fillable = [ 'keywords', 'description'];
    protected $guarded = [];

}
