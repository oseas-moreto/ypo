<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\SiteCategory;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteDica extends Models
{
    protected $table = 'dicas';
    protected $primaryKey = 'iddica';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];

    public function categoria($id){
        $categoria = SiteCategory::where('idlanguage', '=', 1)->where('idcategory', $id)->first();
        return isset($categoria->title) ? $categoria->title : '';
    }

    public function subcategoria(){
        return $this->belongsTo(SiteCategory::class, ['idcategoria2']);
    }
}
