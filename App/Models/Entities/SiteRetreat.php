<?php

namespace App\Models\Entities;

use App\Core\Models;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteRetreat extends Models
{
    protected $table = 'retreats';
    protected $primaryKey = 'idretreat';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];
}
