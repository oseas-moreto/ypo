<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\AccessMenu;
use App\Models\Entities\AccessUser;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class AccessGroup extends Models {
    protected $table = 'access_group';
    protected $primaryKey = 'idgroup';
    public $timestamps = false;
    protected $fillable = ['description', 'status'];
    protected $guarded = [];

    public function menus(){
        return $this->belongsToMany(AccessMenu::class, 'access_groupxmenu', $this->primaryKey, 'idmenu');
    }

    public function users(){
        return $this->hasMany(AccessUser::class, $this->primaryKey);
    }

    public function checkpage($menu, $id){
        $page = $this->join('access_groupxmenu', 'access_group.idgroup', '=', 'access_groupxmenu.idgroup')
        ->join('access_menu', 'access_groupxmenu.idmenu', '=', 'access_menu.idmenu')
        ->where('access_menu.urlmenu', '=', $menu)
        ->where('access_group.idgroup', '=', $id)
        ->select('access_menu.idmenu')->first();

        return $page;
    }

    public function pagelist($grupo, $level){
        $page = $this->join('access_groupxmenu', 'access_group.idgroup', '=', 'access_groupxmenu.idgroup')
            ->join('access_menu', 'access_groupxmenu.idmenu', '=', 'access_menu.idmenu')
            ->where('access_group.idgroup', '=', $grupo)
            ->where('access_menu.level', '=', $level)->where('access_menu.status', '=', 'a')->select('access_menu.*', 'access_group.idgroup')->orderBy('access_menu.order')->get();

        return $page;
    }


}
