<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\SiteEmpresa;
use App\Models\Entities\AccessUser;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteSolicitacaoDesconto extends Models
{
    protected $table      = 'solicitacoes_desconto';
    protected $primaryKey = 'idsolicitacao';
    public $timestamps    = false;
    protected $fillable   = [];
    protected $guarded    = [];

    public function empresa(){
        return $this->belongsTo(SiteEmpresa::class, 'idempresa');
    }

    public function usuario(){
        return $this->belongsTo(AccessUser::class, 'iduser');
    }
}
