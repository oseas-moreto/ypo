<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Language;
use App\Models\Entities\SiteCategory;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;
use Awobaz\Compoships\Compoships;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SiteClient
 *
 * @author oseas
 */
class SiteClient extends Models
{
    use HasCompositePrimaryKey;
    use Compoships;

    protected $table = 'site_client';
    protected $primaryKey = array('idclient', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language()
    {
        return $this->belongsTo(Language::class, 'idlanguage');
    }

    public function category(){
        return $this->belongsTo(SiteCategory::class, array('idcategory', 'idlanguage'));
    }
}
