<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\AccessUser;
use App\Models\Entities\AccessSpouse;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Description of AccessDiretoria
 *
 * @author oseas
 */
class AccessDiretoria extends Models{
    protected $table = 'diretoria';
    protected $primaryKey = 'iddiretoria';
    public $timestamps = false;
    protected $guarded = [];

    public function usuario(){
        return $this->belongsTo(AccessUser::class, 'iduser');
    }

    public function spouse(){
        return $this->belongsTo(AccessSpouse::class, 'idspouse');
    }

    public static function search($parameters = []){

        $diretores = self::leftJoin('access_user as b', function($join) {
            $join->on('diretoria.iduser', '=', 'b.iduser');
        })
        ->leftJoin('spouse as c', function($join) {
            $join->on('diretoria.iduser', '=', 'c.iduser');
        })
        ->whereRaw('(diretoria.iduser != 0 OR diretoria.idspouse != 0)');

        if(isset($parameters['nome'])){
            $diretores->whereRaw('('.$parameters['nome'].')');
        }

        if(isset($parameters['ramo'])){
            $diretores->whereRaw('('.$parameters['ramo'].')');
        }

        if(isset($parameters['capitulo'])){
            $diretores->where('diretoria.capitulo', $parameters['capitulo']);
        }

        $diretores->orderBy('ordemCapitulo')->orderBy('iddiretoria', 'ASC');

        return $diretores->select('diretoria.*')->groupBy('diretoria.iddiretoria')->get();
    }
}
