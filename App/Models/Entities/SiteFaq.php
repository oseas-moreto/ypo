<?php

namespace App\Models\Entities;

use App\Models\Entities\Language;
use App\Core\Models;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SiteFaq
 *
 * @author oseas
 */
class SiteFaq extends Models {
    use HasCompositePrimaryKey;

    protected $table = 'site_faq';
    protected $primaryKey = array('idfaq', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language(){
        return $this->belongsTo(Language::class, 'idlanguage');
    }
}
