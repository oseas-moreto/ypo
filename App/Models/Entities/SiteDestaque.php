<?php

namespace App\Models\Entities;

use App\Core\Models;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteDestaque extends Models{
    
    protected $table = 'destaque';
    protected $primaryKey = 'iddestaque';
    public $timestamps = false;
    protected $fillable = [];
    protected $guarded = [];
}
