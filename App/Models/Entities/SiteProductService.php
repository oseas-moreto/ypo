<?php

namespace App\Models\Entities;

use App\Models\Entities\Language;
use App\Models\Entities\SiteProductCategory;
use App\Models\Entities\SiteProductSegment;
use App\Core\Models;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;
use Awobaz\Compoships\Compoships;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteProductService extends Models {
    use HasCompositePrimaryKey;
    use Compoships;

    protected $table = 'site_product_service';
    protected $primaryKey = array('idservice', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language(){
        return $this->belongsTo(Language::class, 'idlanguage');
    }

    public function category(){
        return $this->belongsTo(SiteProductCategory::class, array('idcategory', 'idlanguage'));
    }

    public function segment(){
        return $this->belongsTo(SiteProductSegment::class, array('idsegment', 'idlanguage'));
    }
}
