<?php

namespace App\Models\Entities;

use App\Models\Entities\Language;
use App\Core\Models;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;
use Awobaz\Compoships\Compoships;
use App\Models\Entities\SiteClassificado;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteCategory extends Models {
    use HasCompositePrimaryKey;
    use Compoships;

    protected $table = 'site_category';
    protected $primaryKey = array('idcategory', 'idlanguage');
    public $timestamps = false;
    protected $guarded = [];

    public function language(){
        return $this->belongsTo(Language::class, 'idlanguage');
    }

    public function classificados(){
        return $this->hasMany(SiteClassificado::class, 'idcategoria');
    }

    public static function getTypes(){
        return [
            'parceiros',
            'classificados',
            'retreats',
            'dicas',
            'videoteca'
        ];
    }
}
