<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>

<main id="mainContent" class="">
  <div class="container-parcerias-detalhe">
    <div class="container box-parcerias-detalhe">
      <div class="box-parcerias-img">
        <img src="/ypobrasil/images/parcerias/img-0.jpg" alt="">
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box-parcerias-title">
            <img src="/ypobrasil/images/parcerias/img.jpg" alt="">
            <h2>Clara Resorts: vantagens  exclusivas para YPOers</h2>
          </div>
          <div class="col-md-12">
            <div class="box-parcerias-text">
              <p>Quem procura um lugar especial perto de São Paulo para se divertir com os filhos precisa conhecer um dos hotéis do Clara Resorts. O Santa Clara Eco Resort foi reconhecido por 3 anos consecutivos como o melhor hotel do Brasil e América do Sul e entre os melhores do mundo para famílias pelo site TripAdvisor.</p>
              <p>Os dois resorts possuem estrutura completa de lazer com piscinas climatizadas ou aquecidas, quadras, brinquedoteca, Pub com shows noturnos, espaços para eventos e SPA by L’Occitane. Além disso, há diversas atividades para todas as idades e recreações a partir de 3 anos, sempre acompanhadas pela equipe de monitores treinados.No Clara Ibiúna Resort, você ainda pode curtir uma linda represa onde são praticados vários esportes aquáticos.Ambos os hotéis oferecem pensão completa com café da manhã, almoço, café da tarde e jantar e atividades já inclusas na diária.</p>
            </div>
          </div>
        </div>
        <div class="divisor">
        </div>
        <div class="col-md-12">
          <div class="box-parcerias-list">
            <div class="list-title">
              <h2>Descontos e Benefícios</h2>
            </div>
            <ul>
              <li>10% de desconto em hospedagem, excetuando-se Natal, Reveillon, Carnaval e Páscoa.</li>
              <li>10% de desconto na hospedagem de eventos de empresas de membros do YPO.</li>
              <li>Pontos em dobro em nosso Programa Fidelidade.</li>
              <li>Cortesia em sala de reunião para eventos do YPO.</li>
              <li>Welcome drink.</li>
              <li>Presente especial para esposas, Kit de produtos L´Occitane em Provence</li>
            </ul>
          </div>
        </div>
        <div class="col-md-12">
          <div class="container-parcerias-img-small">
            <div class="row box-parcerias-img-small">
            <div class="col-md-3">
              <div class="box-parcerias-small-img">
                    <img src="/ypobrasil/images/parcerias/img-01.jpg" alt="">
                </div>
              </div>
              <div class="col-md-3">
                <div class="box-parcerias-small-img">
                  <img src="/ypobrasil/images/parcerias/img-02.jpg" alt="">
                </div>
              </div>
              <div class="col-md-3">
                <div class="box-parcerias-small-img">
                  <img src="/ypobrasil/images/parcerias/img-03.jpg" alt="">
                </div>
              </div>
              <div class="col-md-3">
                <div class="box-parcerias-small-img">
                  <img src="/ypobrasil/images/parcerias/img-03.jpg" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="divisor">
        </div>
        <div class="col-md-12 container-parcerias-info">
          <div class="row ">
            <div class="col-md-4 box-parcerias-info">
              <span class="info1">Tel/WhatsApp</span>
              <span class="info2">(11) 9999-0000</span>
            </div>
            <div class="col-md-4 box-parcerias-info">
              <span class="info1">E-mail</span>
              <span class="info2">johnsample@corporate.com.br</span>
            </div>
            <div class="col-md-4 box-parcerias-info">
              <span class="info1">Site</span>
              <span class="info2">corporate.com.br/span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<?php include('includes/footer.php') ?>
</body>
</html>