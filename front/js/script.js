// ACORDDION


    var acc = document.getElementsByClassName("accordion");
    var i;
    
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = 'unset';
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }

// INPUT 

function showInput(){
  var toggle = document.querySelector('.boxBusca');
  toggle.style.heigth = window.innerHeight - 60 + "px";
  if(toggle.style.top == "100px"){
      toggle.style.top = "-10000px";
  }else {
      toggle.style.top = "100px";
  }
}

  $(document).scroll(function () { 
    var posicaoScroll = $(document).scrollTop(); 
     if (posicaoScroll > 50) $('.boxBusca').animate({'top': "-10000px"}, 100);
  })

  function showMenu(){
    var toggle = document.querySelector('.menu-categoria');
    toggle.style.heigth = window.innerHeight - 60 + "px";
    if(toggle.style.right == "-10000px" && toggle.style.display == "none"){
      toggle.style.display = "block"
        toggle.style.right = "-4px";
    }else {
        toggle.style.display = "none"
        toggle.style.right = "-10000px";
    }
};

function showUl(){
  var toggle = document.querySelector('.box-down-ul-classificados');
  toggle.style.heigth = window.innerHeight - 60 + "px";
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};


function showUlRetreats(){
  var toggle = document.querySelector('.box-down-ul-retreats');
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};
function showUlDicas(){
  var toggle = document.querySelector('.box-down-ul-dicas');
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};
function showUlDados(){
  var toggle = document.querySelector('.box-down-ul-dados');
  if(toggle.style.display == "block"){
    toggle.style.display = "none"
  }else {
      toggle.style.display = "block"
  }
};

$( function() {
  $( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: 2000,
    values: [ 0, 2000 ],
    slide: function( event, ui ) {
      $("#amountMin" ).val( "R$ " + ui.values[ 0 ]);
      $("#amountMax").val("R$ " + ui.values[ 1 ] )
    }
  });
  $( "#amountMin" ).val( "R$ " + $( "#slider-range" ).slider( "values", 0 ) );
  $( "#amountMax" ).val( "R$ " + $( "#slider-range" ).slider( "values", 1 ) );
} );

