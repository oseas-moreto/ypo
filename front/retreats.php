<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>

<main id="mainContent" class="container-categoria container-retreats">
      <section class="retreats-banner">
				<div class="banner-topo">
					<div>
						<span class="shape-down"></span>
						<h2>Retreats</h2>
					</div>
				</div>
		</section>
  <div class="container">
    <div class="row">
        <div class="col-md-4">
          <?php include('includes/retreats-menu.php') ?>
        </div>
          <div class="col-md-8 container-retreats-itens">
            <div class="row ">
              <div class="col-md-12 retreats-item">
                  <div class="row">
                    <div class="col-md-5 boximg">
                        <img src="/ypobrasil/images/categoria/item-retreats.jpg" alt="">
                    </div>
                    <div class="col-md-7">
                        <div class="boxtexto">
                          <h2>Lorem ipsum dolor sit amet, consectetur</h2>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-12 retreats-item">
                  <div class="row">
                    <div class="col-md-5 boximg">
                        <img src="/ypobrasil/images/categoria/item-retreats.jpg" alt="">
                    </div>
                    <div class="col-md-7">
                        <div class="boxtexto">
                          <h2>Lorem ipsum dolor sit amet, consectetur</h2>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        
    </div>
</main>
<?php include('includes/footer.php') ?>
</body>
</html>