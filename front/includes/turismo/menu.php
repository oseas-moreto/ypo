<div class="section--bg-wrapper-05">
		<div class="container-fluid">
			<div class="row  no-gutters ">
				<div class="col-md-2 side-col d-flex align-items-center text-nowrap">
					<!-- start navigation-toggle -->
						<a href="#" id="top-bar__navigation-toggler" class="hide-object-desktop">
							<span></span>
						</a>
						<!-- end navigation-toggle -->
						<!-- start logo -->
					<a href="index.php" class="top-bar__logo">
						<img src="images/logo.png" alt="">
					</a>
					<!-- end logo -->
				</div>
				<div class="col-auto">
					<!-- start desktop menu -->
					<nav id="top-bar__navigation">
					<ul>
						<li>
							<a href="#"><span>turismo</span></a>
							<ul>
								<li><a href="#.">Home Page 1</a></li>
								<li><a href="#.">Home Page 2</a></li>
								<li><a href="#.">Landing Page</a></li>
							</ul>
						</li>
						<li>
							<a href="#"><span>gastronomia</span></a>
						
						</li>
						<li><a href="#"><span>bancos e seguros</span></a></li>
						<li><a href="#"><span>educação e cultura</span></a></li>
						<li>
							<a href="#"><span>automotivos</span></a>
						</li>
						<li><a href="#"><span>lojas</span></a></li>
						
						<li><a href="#"><span>locações</span></a></li>
						
						<li><a href="#"><span>variados</span></a></li>
					</ul>
				</nav>
					<!-- end desktop menu -->
				</div>
				
			</div>
		</div>
	</div>