<div onclick="showMenu()" class="openMenuMobile">
    <button>
      Menu
      <i class="fas fa-bars"></i>
    </button>
</div>
<div class="menu-categoria categoria-principal">
  <div class="box-menu-categoria">
    <div class="menuCategoria">
      <h2 class="menu-categoria-t1">
        Categorias
      </h2>
      <ul>
        <li>Temas</li>
        <li>Destinos </li>
        <li>Hotéis</li>
        <li>Propriedades</li>
        <li>Atividades e exercícios</li>
        <li>Fornecedores</li>
      </ul>
    </div>
</div>  
</div>    