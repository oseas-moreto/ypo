<div id="nav-aside">
	<a href="#" class="nav-aside__close">
		<i class="icon">
			<svg>
				<use xlink:href="#remove"></use>
			</svg>
		</i>
        <img src="images/Logo.jpg" alt="">
    </a>
    
	<div class="nav-aside__content">
        <div id="nav-aside_menu"></div>


		<address>
			&copy; 2020, YPO. TODOS OS DIREITOS RESERVADOS.   <br>  POLÍTICA DE PRIVACIDADE DA YPO   <br>   TERMOS DE USO
		</address>
		<address>
			<span>CRIAÇÃO/DESIGN  <span class="color-azul">AGÊNCIA ME</span>   |  DESENVOLVIMENTO  <span class="color-azul">DEEP OCEAN</span></span>
		</address>
	</div>
</div>