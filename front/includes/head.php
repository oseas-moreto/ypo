<meta charset="utf-8">
    <title>YPO BRASIL</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="YPO BRASIL">
    <meta name="author" content="Quadron">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" href="/ypobrasil//css/style.css">
    <link rel="stylesheet" href="/ypobrasil//css/estilo.css">
    <link rel="stylesheet" href="/ypobrasil//css/categoria.css">
    <link rel="stylesheet" href="/ypobrasil//css/videoTeca.css">
    <!-- Load google font
    ================================================== -->
    <script>
    WebFontConfig = {
        google: { families: [ 'Roboto:300,400,400i,500,700', 'Montserrat:700'] }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>