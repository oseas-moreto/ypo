<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>

<main id="mainContent" class="container-categoria-cadastro">
  <div class="container">
    <div class="row">
        <div class="col-md-4">
          <?php include('includes/menu-categoria.php') ?>
        </div>
        <div class="col-md-8">
          <div class="form">
              <h2 class="form-t1">
                Criar Anúncio
              </h2>
              <div class="box-form">
                <form action="" class="">
                  <div class="box-form-flex">
                    <div class="box-form-select-flex">
                      <div class="form-group">
                        <div class="wrapper-select-for-title">
                          <label class="placeholder-label">Categoria</label>
                          <select name="industry" class="js-init-select select-custom-02">
                            <option>selecione uma opção</option>
                            <option>#</option>
                            <option>#</option>
                            <option>#</option>
                            <option>#</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="wrapper-select-for-title">
                          <label class="placeholder-label">Tipo</label>
                          <select name="industry" class="js-init-select select-custom-02">
                            <option>selecione uma opção</option>
                            <option>#</option>
                            <option>#</option>
                            <option>#</option>
                            <option>#</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="box-form-input-flex">
                      <input placeholder="Nome contato" type="text">
                      <input placeholder="E-mail" type="text">
                    </div>
                    <div class="box-form-input-flex">
                      <input placeholder="Telefone contato" type="tel">
                      <input placeholder="Whatsapp" type="text">
                    </div>
                  </div>   
                  <div class="box-form-input-full">
                    <input placeholder="Título do anúncio (Até 90 caracteres)*" type="text"> 
                  </div>  
                  <textarea placeholder="Descrição" name="" id="" cols="87" rows="8"></textarea>   
                  <div class="box-form-input-flex">
                    <input placeholder="Valor" type="text">
                    <input placeholder="Condições de pagamento" type="text">
                  </div>
                  <div class="uploadIMG">
                    <div class="row">
                      <div class="col-md-12">
                        <span>Adicionar fotos*</span>
                      </div>
                      <div class="col-md-5 img-large text-center">
                        <img src="https://via.placeholder.com/245" alt="">
                        <span class="prin">
                          foto principal
                        </span>
                      </div>
                      <div class="col-md-7 img-small">
                        <div class="row">
                          <div class="col-md-4">
                            <img src="https://via.placeholder.com/120" alt="">
                          </div>
                          <div class="col-md-4">
                          <img src="https://via.placeholder.com/120" alt="">
                          </div>
                            <div class="col-md-4">
                            <img src="https://via.placeholder.com/120" alt="">
                          </div>
                          <div class="col-md-4">
                            <img src="https://via.placeholder.com/120" alt="">
                          </div>
                          <div class="col-md-4">
                            <img src="https://via.placeholder.com/120" alt="">
                          </div>
                          <div class="col-md-4">
                            <img src="https://via.placeholder.com/120" alt="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-form-fim">
                    <div class="form-buttuns">
                        <label for="arquivo" class="input-file">
                            <div>
                              <span>Adicionar arquivo (Se houver)</span>
                              <span><i class="far fa-file-alt"></i></span>
                            </div>
                        </label>
                        <input type="file" id="arquivo" style="display: none">
                        <button type="submit" class="btn btn-submit ct-btn--perspective btn-primary btn-lg text-uppercase pull-right">Publicar
                        </button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
    </div>
  </div>


</main>
<?php include('includes/footer.php') ?>
</body>
</html>