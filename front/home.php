<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>
<!-- end Header -->
<main id="mainContent">
	<!-- start main slider -->
	<section class="container-indent section--bg-vertical-line">
		<div class="mainSlider-layout">
			<div class="loading-content"></div>
			<div class="mainSlider mainSlider-size-01 slick-dots-01" data-arrow="false" data-dots="true">
				<div class="slide main-slider-01-wrapper">
					<div class="main-slider-02-wrapper">
						<div class="img--holder" data-bg="images/banner-01.jpg"></div>
					</div>
				</div>
				<div class="slide">
					<div class="img--holder" data-bg="images/banner-02.jpg"></div>
				</div>
				<div class="slide">
					<div class="img--holder" data-bg="images/banner-03.jpg"></div>
				</div>
				<div class="slide">
					<div class="img--holder" data-bg="images/banner-04.jpg"></div>
				</div>
				<div class="slide">
					<div class="img--holder" data-bg="images/banner-03.jpg"></div>
				</div>
				<div class="slide">
					<div class="img--holder" data-bg="images/banner-06.jpg"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- end main slider -->
	<!-- start section -->
	<section class="section--bg-01 section--bg-wrapper-01 section--pr" id="scroll-down-anchor">
		<div class="section__indent-01 section--bg-vertical-line">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-md-12">
						<div class="section-heading">
							<span class="triangle-right"></span>
							<h3 class="title">Parcerias</h3>
						</div>
					</div>
					<div class="col-md-12 box-itens-meio">
						<div class="row">
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-01.png" alt="Turismo">
								<span>Turismo</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-02.png" alt="Gastronomia">
								<span>Gastronomia</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-03.png" alt="Serviços Financeiros">
								<span>Serviços Financeiros</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-04.png" alt="Educação e Cultura">
								<span>Educação e Cultura</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-05.png" alt="Locadoras de Veículos">
								<span>Locadoras de Veículos</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-06.png" alt="Lojas">
								<span>Lojas</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-07.png" alt="Locação de Espaços">
								<span>Locação de Espaços</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-08.png" alt="Saúde e Estética">
								<span>Saúde e Estética</span>
							</div>
							<div class="col-6 col-lg-4 itens">
								<img src="/images/itens-icon-09.png" alt="Variados">
								<span>Variados</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end section -->
	<!-- start section -->
	<section class="section section__indent-02-custom section--bg-vertical-line section--bg-00 section-blue">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-md-12">
					<div class="section-heading">
						<span class="triangle-right"></span>
						<h3 class="title white">Classificados</h3>
					</div>
				<div class="slick-arrow-extraright show-mobile">
					<div class="slick-arrow slick-prev icon-next">
						<i class="fas fa-chevron-left"></i>
					</div>
					<div class="slick-arrow slick-next icon-next">
						<i class="fas fa-chevron-right"></i>
					</div>
				</div>
				</div>
				<div class="js-carusel-news-class col-md-12 box-itens-classificados">
						<a href="" class="promobox03 block-once itens item veiculos">
							<div class="promobox03__img">
								<img src="../images/veiculos.jpg" alt="">
							</div>
							<div class="promobox03__description">
								<div class="promobox03__number">01</div>
								<div class="promobox03__layout">
									<h4 class="promobox03__title box-title-veiculos">Veículos</h4>
									<div class="promobox03__show">
										<p>
											Compre, venda ou alugue  um Veículos com mais segurança 
										</p>
										<span class="promobox03__link">ir para a página</span>
									</div>
								</div>
							</div>
						</a>
						<a href="" class="promobox03 block-once itens item Imoveis">
							<div class="promobox03__img">
								<img src="../images/imoveis.jpg" alt="">
							</div>
							<div class="promobox03__description">
								<div class="promobox03__number">02</div>
								<div class="promobox03__layout">
									<h4 class="promobox03__title box-title-imoveis">Imóveis</h4>
									<div class="promobox03__show">
										<p>
											Compre, venda ou alugue  um imóvel com mais segurança 
										</p>
										<span class="promobox03__link">ir para a página</span>
									</div>
								</div>
							</div>
						</a>
						<a href="" class="promobox03 block-once itens item variados">
							<div class="promobox03__img">
								<img src="../images/variados.jpg" alt="">
							</div>
							<div class="promobox03__description">
								<div class="promobox03__number">03</div>
								<div class="promobox03__layout">
									<h4 class="promobox03__title box-title-variados">Variados</h4>
									<div class="promobox03__show">
										<p>
											Compre, venda ou alugue  um Variados com mais segurança 
										</p>
										<span class="promobox03__link">ir para a página</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>	
				<div class="box-verTodos">
					<span class="circle"><i class="fas fa-chevron-right"></i></span>
					<span>
						Ver todos 
					</span>
				</div>
		</div>
	</section>
	<!-- end section -->
	<!-- start section -->
	<section class="section--bg-01 section--bg-wrapper-01 section--pr" id="scroll-down-anchor">
		<div class="section__indent-01 section--bg-vertical-line">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-md-12">
						<div class="section-heading section-retreats">
							<span class="triangle-right"></span>
							<h3 class="title">Retreats</h3>
						</div>
					</div>
					<div class="col-md-12 box-itens-meio">
						<div class="row">
							<div class="col-6 col-lg-4  itens">
								<img src="/images/itens-icon-10.png" alt="Temas">
								<span>Temas</span>
							</div>
							<div class="col-6 col-lg-4  itens">
								<img src="/images/itens-icon-11.png" alt="Destinos">
								<span>Destinos</span>
							</div>
							<div class="col-6 col-lg-4  itens">
								<img src="/images/itens-icon-12.png" alt="Hotéis">
								<span>Hotéis</span>
							</div>
							<div class="col-6 col-lg-4  itens">
								<img src="/images/itens-icon-13.png" alt="Educação e Cultura">
								<span>Propriedades</span>
							</div>
							<div class="col-6 col-lg-4  itens">
								<img src="/images/itens-icon-14.png" alt="Locadoras de Veículos">
								<span>Atividades e Exercícios</span>
							</div>
							<div class="col-6 col-lg-4  itens">
								<img src="/images/itens-icon-15.png" alt="Lojas">
								<span>Fornecedores</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- start section -->
	<!-- end section -->
	<section class="section section--bg-vertical-line section-dicas">
		<div class="container">
			<div class="">
				<div class="section-heading">
					<span class="triangle-right"></span>
					<h3 class="title">Dicas</h3>
				</div>
			</div>
			<div class="section-heading box-dicas">
				<h4 class="title">Encontre aqui as melhores <br> indicações dos YPOers</h4>
			</div>
			<form class="contact-form form-default" id="contactform" method="post" novalidate="novalidate" action="#">
				<div class="box-dicas">	
					<div class="form-group">
						<div class="wrapper-select-for-title">
							<label class="placeholder-label">SEGMENTO</label>
							<select name="industry" class="js-init-select select-custom-02">
								<option>selecione uma opção</option>
								<option>Agriculture</option>
								<option>Delivery</option>
								<option>MasMedia</option>
								<option>Entertaiment</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="wrapper-select-for-title">
							<label class="placeholder-label">CIDADE</label>
							<select name="industry" class="js-init-select select-custom-02">
								<option>selecione uma opção</option>
								<option>Agriculture</option>
								<option>Delivery</option>
								<option>MasMedia</option>
								<option>Entertaiment</option>
							</select>
						</div>
					</div>		
				</div>
			</form>
		</div>
	</section>
	<!-- end section -->
	<!-- start section -->
	<section class="section section__indent-03 section--bg-vertical-line box-section-videoteca">
		<div class="container section--pr">
			<div class="">
				<div class="section-heading section-videoteca">
					<span class="triangle-right"></span>
					<h3 class="title">videoteca</h3>
				</div>
			</div>
			<div class="slick-arrow-extraright">
				<div class="slick-arrow slick-prev icon-next">
					<i class="fas fa-chevron-left"></i>
				</div>
				<div class="slick-arrow slick-next icon-next">
					<i class="fas fa-chevron-right"></i>
				</div>
			</div>
			<div class="js-carusel-news promobox02__slider">
				<div class="item">
					<a href="#" class="promobox02">
						<figure>
							<img src="images/blog/blog-01.jpg" alt="">
							<figcaption>
								<div class="promobox02__time">25 Jan 2021</div>
								<h4 class="promobox02__title">
									Lorem ipsum dolor sit amet, consectetur
								</h4>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="item">
					<a href="#" class="promobox02">
						<figure>
							<img src="images/blog/blog-02.jpg" alt="">
							<figcaption>
								<div class="promobox02__time">21 Jan 2021</div>
								<h4 class="promobox02__title">
									Lorem ipsum dolor sit amet, consectetur
								</h4>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="item">
					<a href="#" class="promobox02">
						<figure>
							<img src="images/blog/blog-03.jpg" alt="">
							<figcaption>
								<div class="promobox02__time">05 Jan’ 19</div>
								<h4 class="promobox02__title">
									New Davic 2 Pro/Zoom firmware released
								</h4>
							</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="btn-row btn-top text-center">
				<a class="btn-link-icon" href="#">
					<i class="btn__icon">
						<i class="fas fa-chevron-right"></i>
					</i>
					<span class="btn__text">VER TODOS</span>
				</a>
			</div>
		</div>
	</section>
	<!-- end section -->


</main>
<?php include('includes/footer.php') ?>
</body>
</html>