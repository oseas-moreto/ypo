<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php include('includes/head.php');?>

</head>
<body>
<!-- start Header -->
<header id="top-bar">
	<?php include('includes/menu.php');?>
</header>

<main id="mainContent" class="container-categoria container-dica-indicacao">
      <section class="dica-indicacao-banner">
				<div class="banner-topo">
					<div>
						<span class="shape-down"></span>
						<h2>Dicas e Indicações</h2>
					</div>
				</div>
		</section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="p-dica">
          Abaixo, você vai encontrar as melhores indicações em diversos segmentos. <br>
          Dicas seguras, testadas e aprovadas por YPOers para YPOers. <br>
          Deixe também as suas. Boas indicações são sempre bem-vindas!
        </p>
      </div>
        <div class="col-md-4">
          <?php include('includes/dica-menu.php') ?>
        </div>
          <div class="col-md-8 container-dica-indicacao-itens">
            <div class="row ">
              <div class="col-md-12 dica-indicacao-item">
                <h2 class="t1-dica">
                  Lorem ipsum dolor sit
                </h2>
                <p class="p-texto-dica">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae ex dolorem ut? At rerum,
                </p>
                <div class="dica-contato">
                  <div class="tel-dica">
                    <span class="t2-dica">Tel/WhatsApp</span>
                    <span class="info-dica">(11) 99999-0000</span>
                  </div>
                  <div class="email-dica">
                    <span class="t2-dica">E-mail</span>
                    <span class="info-dica">joansample@corporate.com.br</span>
                  </div>
                  <div class="site-dica">
                    <span class="t2-dica">Site</span>
                    <span class="info-dica">corporate.com.br</span>
                  </div>
                </div>
              </div>
              <div class="col-md-12 dica-indicacao-item">
                <h2 class="t1-dica">
                  Lorem ipsum dolor sit
                </h2>
                <p class="p-texto-dica">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae ex dolorem ut? At rerum,
                </p>
                <div class="dica-contato">
                  <div class="tel-dica">
                    <span class="t2-dica">Tel/WhatsApp</span>
                    <span class="info-dica">(11) 99999-0000</span>
                  </div>
                  <div class="email-dica">
                    <span class="t2-dica">E-mail</span>
                    <span class="info-dica">joansample@corporate.com.br</span>
                  </div>
                  <div class="site-dica">
                    <span class="t2-dica">Site</span>
                    <span class="info-dica">corporate.com.br</span>
                  </div>
                </div>
              </div>
              <div class="col-md-12 dica-indicacao-item">
                <h2 class="t1-dica">
                  Lorem ipsum dolor sit
                </h2>
                <p class="p-texto-dica">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae ex dolorem ut? At rerum,
                </p>
                <div class="dica-contato">
                  <div class="tel-dica">
                    <span class="t2-dica">Tel/WhatsApp</span>
                    <span class="info-dica">(11) 99999-0000</span>
                  </div>
                  <div class="email-dica">
                    <span class="t2-dica">E-mail</span>
                    <span class="info-dica">joansample@corporate.com.br</span>
                  </div>
                  <div class="site-dica">
                    <span class="t2-dica">Site</span>
                    <span class="info-dica">corporate.com.br</span>
                  </div>
                </div>
              </div>
              <div class="col-md-12 dica-indicacao-item">
                <h2 class="t1-dica">
                  Lorem ipsum dolor sit
                </h2>
                <p class="p-texto-dica">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae ex dolorem ut? At rerum,
                </p>
                <div class="dica-contato">
                  <div class="tel-dica">
                    <span class="t2-dica">Tel/WhatsApp</span>
                    <span class="info-dica">(11) 99999-0000</span>
                  </div>
                  <div class="email-dica">
                    <span class="t2-dica">E-mail</span>
                    <span class="info-dica">joansample@corporate.com.br</span>
                  </div>
                  <div class="site-dica">
                    <span class="t2-dica">Site</span>
                    <span class="info-dica">corporate.com.br</span>
                  </div>
                </div>
              </div>
              <div class="col-md-12 dica-indicacao-item">
                <h2 class="t1-dica">
                  Lorem ipsum dolor sit
                </h2>
                <p class="p-texto-dica">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae ex dolorem ut? At rerum,
                </p>
                <div class="dica-contato">
                  <div class="tel-dica">
                    <span class="t2-dica">Tel/WhatsApp</span>
                    <span class="info-dica">(11) 99999-0000</span>
                  </div>
                  <div class="email-dica">
                    <span class="t2-dica">E-mail</span>
                    <span class="info-dica">joansample@corporate.com.br</span>
                  </div>
                  <div class="site-dica">
                    <span class="t2-dica">Site</span>
                    <span class="info-dica">corporate.com.br</span>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="page-nav">
                  <a href="#" class="page-nav__btn page-nav__left">
                    <i class="btn__icon">
                      <i class="fas fa-chevron-left"></i>
                    </i>
                    <span class="btn__text">ANT</span>
                  </a>
                  <ul class="page-nav__list">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                  </ul>
                  <a href="#" class="page-nav__btn page-nav__right">
                    <span class="btn__text">PROX</span>
                    <i class="btn__icon">
                      <i class="fas fa-chevron-right"></i>
                    </i>
                  </a>
                </div>
              </div>
            </div>
          </div>

        </div>
    </div>
    <section class="container-dica-form">
        <div class="row box-form-dica">
          <div class="col-md-12 box-form-dica-t1">
            <h2 class="t1-form-dica">Deixa sua dica ou indicação</h2>
          </div>
            <div class="col-md-12 ">
            <form class="contact-form form-default form-dica" id="contactform" method="post" novalidate="novalidate" action="#">
                <div class="box-dicas">	
                  <div class="form-group">
                    <div class="wrapper-select-for-title">
                      <label class="placeholder-label">SEGMENTO</label>
                      <select name="industry" class="js-init-select select-custom-02">
                        <option>selecione uma opção</option>
                        <option>Agriculture</option>
                        <option>Delivery</option>
                        <option>MasMedia</option>
                        <option>Entertaiment</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="wrapper-select-for-title">
                      <label class="placeholder-label">CIDADE</label>
                      <select name="industry" class="js-init-select select-custom-02">
                        <option>selecione uma opção</option>
                        <option>Agriculture</option>
                        <option>Delivery</option>
                        <option>MasMedia</option>
                        <option>Entertaiment</option>
                      </select>
                    </div>
                  </div>		
                </div>
                <div class="input-flex">
                  <input type="text" placeholder="Nome/Título">
                  <input type="email" placeholder="E-mail">
                </div>
                <div class="input-flex">
                  <div>
                    <input type="tel" placeholder="Tel/Whatapp">
                    <input type="text" placeholder="Site(Se houver)">
                  </div>
                  <textarea name="" id="" cols="4" rows="3" placeholder="Descrição"></textarea>
                </div>
                <div class="box-form-dica-btn-submit">
                  <button class="form-dica-btn-submit" type="submit">enviar</button>
                </div>
              </form>            
            </div>
        </div>
    </section>
</main>
<?php include('includes/footer.php') ?>
</body>
</html>